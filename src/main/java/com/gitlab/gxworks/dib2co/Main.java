// Copyright (C) 2019,2020  Roland Horsch <g x w orks @ma il .de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package com.gitlab.gxworks.dib2co;

import com.gitlab.dibdib.common.TcvCodecAes;
import com.gitlab.dibdib.console_ui.UiRunner;
import java.io.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.TsvCodecIf;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_ui.CalcPres;
import net.sf.dibdib.thread_wk.CcmRunner;

public class Main extends UiRunner {

/**
 * @param args Command line arguments
 * @throws Exception
 */
public static void main( String[] args) throws Exception {
//=====

	INSTANCE = new Main();

	Dib2Config.init( '0', "co", path, INSTANCE, new TsvCodecIf[] { TcvCodecAes.instance });

//	io.github.gxworks.dibdib.Test.INSTANCE.run( new String[0]);
//	if (5 >= args.length)
//		return;

	System.out.println( "Dib2Co version " + Dib2Constants.VERSION_STRING);
	System.out.println( Dib2Constants.NO_WARRANTY[0] + ' ' + Dib2Constants.NO_WARRANTY[1]);
	System.out.println( "Requires proper security settings for Java ! (.../jre/lib/security)\n");
	System.out.println( "Really! :-)\nTry it out, but do not be surprised ...");
	System.out.println( "If you get some kind of decoding error after the next steps, it could very well");
	System.out.println( "be your Java settings.\n");
	if (0 < args.length) {
		path = (1 < args[0].length()) ? args[0] : path;
		if ((1 < args.length) && StringFunc.toUpperCase( args[1]).contains( "DEBUG")) {
			DEBUG = true;
		}
	}
	INSTANCE.run();
}

@Override
public void run() {
	CcmRunner.INSTANCE.init( CalcPres.INSTANCE.getQPlace()); //true, true );
	CalcPres.INSTANCE.init( CcmRunner.INSTANCE.wxGateIn); //CcmRunner.INSTANCE.qGateOut, CcmRunner.INSTANCE.qGateIn );
	CalcPres.INSTANCE.start();

	try {
		System.out.println( "Access code (for file '" + path + "')?");
		in = new BufferedReader( new InputStreamReader( System.in, "UTF8"));
		String accessCode = in.readLine().trim(); //.replaceAll( "[^ -~]", "" );
		TcvCodec.instance.setAccessCode( StringFunc.bytesUtf8( accessCode));
		System.out.println( "Password? (matching tha data/ account, e.g. e-mail password)");
		String phrase = in.readLine().trim();
		TcvCodec.instance.setHexPhrase( StringFunc.hexUtf8( phrase, false));
		for (int i0 = phrase.length() - 1; i0 >= 0; --i0) {
			if (' ' > phrase.charAt( i0)) {
				phrase = phrase.substring( 0, i0) + phrase.substring( i0 + 1);
			}
		}
		System.out.println( "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n...\n");
		System.out.println( "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n...\n");
		if (new File( path).isFile()) {
			if (!getData( phrase, accessCode)) {
				return;
			}
		}
		runLoop();
	} catch (Exception e) {
		System.out.println( "Exception: " + e);
		e.printStackTrace();
	}
	System.out.println( "Good-by.");
}

//=====
}
