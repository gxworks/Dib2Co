// Copyright (C) 2016,2017,2018,2019  Roland Horsch <g x w orks @ma il.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package com.gitlab.dibdib.console_ui;

import io.github.gxworks.dibdib.Test;
import java.io.*;
import java.util.*;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_any.QMapSto.QVal;
import net.sf.dibdib.thread_ui.*;
import net.sf.dibdib.thread_wk.CcmRunner;

/** For Dib2Co etc.
 */
public abstract class UiRunner implements PlatformFuncIf, UiFuncIf, Runnable {
//=====

public static UiRunner INSTANCE;

///// Threaded (in/ out/ trigger)

//protected CommandBaton qyCommand;

/////

public static boolean DEBUG = false;

protected static Thread[] qThreads = null;
protected static String path = "dibdib.dm";
// protected static String key;
protected static HashSet<String> entries;
protected static BufferedReader in;

private static String zEntryLiteral = null;

private static void printHelp() {
	System.out.println( "Type ';' (plus ENTER) to end the program, '?' to get some info/ help,");
	System.out.println( "'?!' for the license, ',' for a listing.");
	System.out.println( "(Sample usage: type '3', press ENTER, '4', ENTER, '+', ENTER)");
	System.out.println( "(OR: type '3', press ENTER, '4', ENTER, ':ADD' (or ',add'), ENTER)");
	System.out.println( "(Sample 2: type '\\' + name of new file, press ENTER, ':EXPORT', ENTER)\n");
}

private static HashSet<String> toEntrySet( String dat) {
	HashSet<String> out = new HashSet<String>();
	int i1 = 0;
	for (int i0 = 0; 0 <= (i1 = dat.indexOf( '\n', i0)); i0 = i1 + 1) {
		String[] els = dat.substring( i0, i1).split( "\t");
		if ((3 >= els.length) || (16 >= els[3].length())) {
			continue;
		}
		if (7 < els.length) {
			if (("PREF".equals( els[2])) || ("VAR".equals( els[2]))) {
				out.add( els[1] + els[2] + "...." + els[7].trim().replaceAll( "[^0-9A-F]", ""));
			} else {
				out.add( els[1] + els[2] + els[3].substring( 0, 16) + els[7].trim());
			}
		} else if (3 < els.length) {
			out.add( els[1] + els[2] + els[3].substring( 0, 16));
		}
	}
	return out;
}

private void sleep( int time) {
	try {
		Thread.sleep( time);
	} catch (InterruptedException e) {
	}
}

private static void cmpData( String marker, HashSet<String> d0, HashSet<String> d1) {
	for (String entry : d1) {
		if (!d0.contains( entry)) {
			System.out.println( "DIFF" + marker + "\t" + entry);
			for (String xcmp : d0) {
				if ((9 < xcmp.length()) && entry.startsWith( xcmp.substring( 0, 9))) {
					System.out.println( "\n////?\t" + xcmp);
				}
			}
		}
	}
}

protected static boolean getData( String phrase, String accessCode) {
	System.out.println( "Trying to decode data from " + new File( path).getAbsolutePath());
	System.out.println( "\n...\n");

	byte[] header = new byte[8];
	byte[] dat = TcvCodec.instance.readPacked( path, header, phrase, accessCode);

	if (1 == (header[0] & 0xff)) {
		entries = new HashSet<String>();
	} else if (Dib2Constants.MAGIC_BYTES[0] != (header[0])) {
		System.out.println( "Cannot decode file: " + path);
		return false;
	} else {
		String datx = new String( dat, StringFunc.STRX16U8);
		System.out.print( datx);
		entries = toEntrySet( datx);
		System.out.println( "\n\n=====");
		int version = header[2] & 0xff;
		int cnt = CcmSto.instance.importCsv( dat, true, (4 >= version) ? 4 : 0);
		System.out.println( "Records: " + cnt);
		System.out.println( "=====\n");
		if (DEBUG) {
			HashSet<String> cmp = toEntrySet( new String( CcmSto.instance.toCsv( null, 0, ~0, ~0), StringFunc.STRX16U8));
			cmpData( "1", cmp, entries);
			cmpData( "2", entries, cmp);
			String sTmp = "DEBUG - Feeling temporary";
			QVal uTmp = QMapSto.qval4AtomicLiteral( new QResult(), sTmp);
			System.out.print( "\n** " + sTmp + " =? '" + QMapSto.string4QVal( uTmp)
				+ "': " + QMapSto.equalValues( QVal.asQVal( uTmp), sTmp)
				+ " " + !QMapSto.equalValues( QVal.asQVal( uTmp), sTmp + '!'));
			QMapSto.idle();
			System.out.print( "--> " + " !=? " + QMapSto.string4QVal( uTmp)
				+ ": " + !QMapSto.equalValues( QVal.asQVal( uTmp), sTmp));
			cmp = toEntrySet( new String( CcmSto.instance.toCsv( null, 0, ~0, ~0), StringFunc.STRX16U8));
			cmpData( "3", cmp, entries);
			cmpData( "4", entries, cmp);
			System.out.println( "\n\nEntries: " + entries.size() + "/ " + cmp.size());
			System.out.println( "=====\n");
			String keyInfo = Dib2Config.codecs[0].getKeyInfo( entries);
			System.out.println( keyInfo);
			System.out.println( "=====\n");
		} else if (1 < cnt) {
			System.out.println( "=====");
			System.out.println( "NOTE: The main purpose of this program is to just read your data,");
			System.out.println( "as done. You may now quit this program by hitting ENTER.");
			System.out.println( "If you want to experiment with your data and if you have a proper.");
			System.out.println( "backup, you can choose to continue by typing 'YES'.");
			System.out.println( "=====\n");
			try {
				String x0 = in.readLine();
				if (!"YES".equals( x0.trim().toUpperCase( Locale.ROOT))) {
					return false;
				}
			} catch (IOException e) {
				return false;
			}
		}
	}
	return true;
}

private void printLines( String[] lines, int len, String pre, boolean cut) {
	for (int i0 = 0; i0 < len; ++i0) {
		String line = "";
		if ((lines.length > i0) && (null != lines[i0])) {
			line = lines[i0].replace( "\n", " // ");
			if ((null != line) && cut && (75 <= line.length())) {
				line = line.substring( 0, 70) + "...";
			}
			line = StringFunc.makePrintable( line);
		}
		System.out.println( (null == line) ? "" : pre + line);
	}
}

private void paint( String[] xLines) {
	int len;
	String[] lines = xLines;

	if (null == lines) {
		CalcPres.INSTANCE.prepareTextLines( true);
		CalcPres.INSTANCE.consolidateTextLines();
		lines = UiDataSto.qPageLinesStable.lines;
		len = UiDataSto.qPageLinesStable.count;
	} else {
		//		Dib2Ui.qSwitches[ Dib2Ui.SWI_VIEW ] = Dib2Ui.SW_VAL_VIEW_Help;
		UiDataSto.qPageLinesStable.lines = lines;
		len = lines.length;
		UiDataSto.qPageLinesStable.count = len;
	}
	System.out.println( "\n\n\n_____");
	String pre = " ";
	System.out.println( "");
	printLines( lines, len, pre, false);
	pre = "...";
	System.out.println( "\n_____\n");
	{
		lines = new String[UiDataSto.MIN_TEXT_LINES];
		len = CcmSto.instance.stackRead( 3, lines, 0, true);
	}
	printLines( lines, UiDataSto.MIN_TEXT_LINES, pre, (null == xLines));
	//	Mapping.Cats[] cats = Mapping.Cats.list4Flags( UiDataSto.qSwitches[ UiDataSto.SWI_MAPPING_CATS ] );
	System.out.println( "[ " // + ((0 >= cats.length) ? Mapping.Cats.DEFAULT : cats[ 0 ]).name()
		+ QMapSto.string4QVals( Mapping.Cats.list4Flags( UiDataSto.qSwitches[UiDataSto.SWI_MAPPING_CATS] & ((1L << 32) - 1L)),
			" ")
		+ " ]");
	System.out.println( "_____");
	System.out.println( "';': end the program, '?': info/help, '?!': license, ',': list.");
	System.out.println( (DEBUG ? "DEBUG -- " : "") + "Data/cmd? ");
}

private boolean pushCmd( String command, String... parameter) {
	CommandBaton cmd = null;
	if (null != command) {
		cmd = new CommandBaton();
		cmd.operator = QCalc.getOperator( command);
		cmd.uiParameter = ((null != parameter) && (0 < parameter.length)) ? parameter[0] : null;
		//		qyCommand = cmd;
	}
	if (null != cmd) {
		boolean out = true;
		cmd = CalcPres.INSTANCE.handleUiEvent( cmd);
		if (null != cmd) {
			//			out = 
			CcmRunner.INSTANCE.wxGateIn.push( cmd);
		}
		//		qyCommand = null;
		return out;
	}
	return false;
}

/** UI thread */
private boolean OnEntry( String line) {
	if (line.equals( "//") && (null == zEntryLiteral)) { //.startsWith( "{_{" ) || line.startsWith( "{{" ) || line.startsWith( "[[" )) {
		zEntryLiteral = ""; //line.trim();
		return true;
	}
	boolean operator = true;
	if (null != zEntryLiteral) {
		operator = false;
		if (line.equals( "//")) { // .trim().startsWith( "]" ) || line.trim().startsWith( "}" )) {
			line = zEntryLiteral; // + line).trim();
			zEntryLiteral = null;
		} else {
			zEntryLiteral += '\n' + line; // (('{' == zEntryLiteral.charAt( 0 )) ? '\n' : ' ') + line;
			return true;
		}
	}
	line = line.trim();
	String x0 = StringFunc.string4Mnemonics( line);
	//  ... list processing ...
	x0 = StringFunc.makePrintable( x0);
	if (!line.startsWith( "_{{") && !line.startsWith( "{{")) {
		x0 = x0.trim();
	}
	operator = operator && line.equals( x0);
	if ((0 < x0.length()) && (' ' > x0.charAt( 0))) {
		x0 = "\\" + x0;
	}
	if (990 >= UiDataSto.qSwitches[UiDataSto.SWI_STEP_OR_ACCESS_MINUTE]) {
		printHelp();
		System.out.println( "Data/cmd? ");
		UiDataSto.qSwitches[UiDataSto.SWI_STEP_OR_ACCESS_MINUTE] = 1000;
		return true;
	}
	UiDataSto.qSwitches[UiDataSto.SWI_VIEW] = UiDataSto.SW_VAL_VIEW_Standard;
	boolean done = false;
	while (!done) {
		done = true;
		if (0 >= x0.length()) {
			continue;
		}
		final char c1 = (1 < x0.length()) ? x0.charAt( 1) : ' ';
		final char c0 = (('-' != x0.charAt( 0)) || ('0' > c1) || ('9' < c1)) ? x0.charAt( 0) : c1;
		if (x0.startsWith( "@!")) {
			DEBUG = !"@!.".equals( x0);
			if (!DEBUG) {
				continue;
			}
			try {
				Test.INSTANCE.run( new String[0]);
				Mapping[] mpgs = CcmSto.instance.toList( 0, 999);
				for (Mapping mpg : mpgs) {
					System.out.println( mpg.toCsvLine( ""));
				}
			} catch (Exception e) {
				System.out.println( "Exception: " + e);
				e.printStackTrace();
			}
			continue;
		} else if (",".equals( x0) || (StringFunc.toUpperCase( x0).contains( "DUMP") && ((':' == c0) || (',' == c0)))) {
			paint( new String( CcmSto.instance.toCsv( null, 0, ~0, ~3), StringFunc.STRX16U8).split( "\n"));
			printHelp();
			System.out.println( "");
			continue;
		}
		String parameter = null;
		if (('_' == c0) || ('\\' == c0) || ('°' == c0)) {
			///// data
			if ((1 == x0.length()) && ('_' != c0)) {
				x0 = "DUP";
			} else {
				parameter = StringFunc.string4Mnemonics( x0.substring( (('_' != c0) ? 1 : 0)));
				x0 = "NOP";
			}
		} else if (0 <= ".,:;^])`?!".indexOf( c0)) {
			switch (c0) {
			case '.': // click/move
				continue; // ...
			case ',': // command
			case ':': // command
				x0 = x0.substring( 1);
				break;
			case ';': // control
			case '^': // control
				if (1 < x0.length()) {
					continue; // ...
				}
				x0 = "EXIT";
				break;
			case ']': // meta
			case ')': // meta
				continue; // ...
			case '`': // meta2
			case '?': // meta2
				if ("?!".equals( x0)) {
					x0 = "ABOUT";
				} else if (1 < x0.length()) {
					continue; // ...
				} else {
					x0 = "HELP";
				}
				break;
			case '!': // ...
				if (1 >= x0.length()) {
					x0 = "CLEAR";
				} else if (2 == x0.length()) {
					int repeat = (x0.charAt( 1) & 0xf) - 1;
					x0 = "CLEAR";
					if ('*' == x0.charAt( 1)) {
						x0 = "CLRALL";
					} else {
						// x0 = "CLR" + (x0.charAt( 1 ) & 0xf);
						for (; repeat > 0; --repeat) {
							pushCmd( "CLEAR");
						}
					}
				} else {
					continue; // ...
				}
				break;
			default:
				continue;
			}
		} else //if (' ' <= c0) {
		if (!operator || (('A' <= c0) && (c0 <= 'z')) || (0x80 <= c0) || (('0' <= c0) && (c0 <= '9'))) {
			parameter = x0;
			x0 = "NOP";
		}
		if (pushCmd( ((0 >= x0.length()) ? "EXEC" : x0), parameter)) {
			sleep( 100);
			return false;
		}
	}
	return true;
}

protected void runLoop() {
	qThreads = new Thread[1];
	boolean active = false;
	OnEntry( "");
	while (!CalcPres.wxExitRequest) {
		String x0;
		try {
			// TODO: timeout/alarm
			x0 = in.readLine();
		} catch (IOException e) {
			e.printStackTrace();
			break;
		}
		if (CalcPres.wxExitRequest // (-99 >= Dib2UiW.INSTANCE.qStatus)
			|| (";".equals( x0))) {
			break;
		}
		if (!OnEntry( x0)) {
			if (!CalcPres.wxExitRequest) { //(0 >= Dib2UiW.INSTANCE.qStatus) {
				if (null != CcmRunner.qActive) { // (0 <= Dib2UiW.INSTANCE.qStatus) {
					Thread.yield();
				}
				qThreads[0] = new Thread( CcmRunner.INSTANCE);
				qThreads[0].start();
			} else {
				sleep( 200);
			}
		}
		if (!active && !DEBUG) {
			boolean ok = new File( path).renameTo( new File( path + ".old"));
			if (!ok) {
				CcmSto.instance.write( path + ".tmp.bak", true, false, true);
			}
		}
		active = true;
		if (pushCmd( null)) {
			sleep( 200);
		}
	}
	CalcPres.wxExitRequest = true;
	sleep( 100);
	for (int i0 = 0; i0 < qThreads.length; ++i0) {
		try {
			qThreads[i0].interrupt();
		} catch (Exception e) {
		}
	}
	sleep( 100);
	if (active && !DEBUG) {
		CcmSto.instance.write( path, true, false, false); //, Dib2Constants.FILE_STRUC_VERSION_MIN );
	}
}

/////

@Override
public File getFilesDir( String... parameters) {
	if ((0 < parameters.length) && (parameters[0].contains( "safe"))) {
		return null;
	}
	return new File( ".");
}

@Override
public void log( String... aMsg) {
	if (DEBUG) {
		System.out.println( Arrays.toString( aMsg));
	}
}

@Override
public void toast( String msg) {
	System.out.println( msg);
}

@Override
public void invalidate() {
	///// In this case, just use the worker thread instead of an extra UI thread
	if (null != CcmRunner.qActive) {
		System.out.print( "..");
	} else {
		//		UiCalcServer.INSTANCE.fetchTextLines();
		//		UiCalcServer.INSTANCE.consolidateTextLines();
		paint( null);
	}
}

@Override
public int getTextRow( int xYReal, int xRange) {
	final int LINE_SPACING = 1;
	int nY = (xYReal << UiDataSto.qShiftScale) / LINE_SPACING;
	return (0 > nY) ? 0 : ((nY >= xRange) ? (xRange - 1) : nY);
}

@Override
public int getTextColumn( int xXReal, String xText) {
	if ((null == xText) || (0 > xXReal)) {
		return -1;
	}
	return (xXReal < xText.length()) ? xXReal : xText.length();
}

@Override
public void setTouched4TextPos() {
	// n.a.
}

private String[] getLicense_lines;

@Override
public String[] getLicense( String pre) {
	if (null == getLicense_lines) {
		getLicense_lines = MiscFunc.getLicense( null, Dib2Constants.LICENSE_LIST).split( "\n");
	}
	return getLicense_lines;
}

@Override
public boolean pushClipboard( String label, String text) {
	return false;
}

@Override
public String getClipboard() {
	return null;
}

//=====
}
