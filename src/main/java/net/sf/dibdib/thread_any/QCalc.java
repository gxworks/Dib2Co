// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_any;

import java.util.*;
import java.util.regex.*;
import net.sf.dibdib.generic.QResult;
import net.sf.dibdib.thread_any.QMapSto.QVal;

/** Operators as functions (on Strings, Doubles, bitlists, lists, ...).
 * First 4 char's must be significant. First string of optional symbols is empty if
 * operator symbol is the default, rather than the name.
 * Boolean value = lowest bit of bitlist: TRUE = -1 or odd number, FALSE = 0 or even number.
 * Bitlist functions operate bitwise and are thus also used for booleans.
 */
public enum QCalc {
//=====

//TODO Consider number of return values for QVal.IN_PROCESS
// var L lastX, T term, Q quickMem

NOP( 0, 0, "no operation"),

///// Math (atomic)

ABS( 1, "POSV", "absolute value: [X0 X1 ...] => [ABS(X0) ABS(X1) ...] "),
ACOS( 1, "acos"),
ADD( 2, "\u2214", "sum of 2 values each [Y0 Y1 ...] [X0 X1 ...] => "),
AND( 2, "", "&", "&&", "\u2227", "binary AND"),
ASIN( 1, "asin"),
ATAN( 1, "atan"),
CEIL( 1, "integer above"),
COMP( 2, "comparison: Y X -> -1/0/1"),
COS( 1, "cos"),
COSH( 1, "cosh"),
DEG( 1, "radians to degrees"),
DIV( 2, "\u00f7", "\u2215", "division for 2 values (each)"),
E( 0, "Euler"),
FACT( 1, "", "!!", "FACTORIAL", "factorial"),
FALSE( 0, "0", "return 0 as even value (= boolean FALSE)"),
FLOOR( 1, "integer below"),
FRAC( 1, "fractional part"),
GRAD( 1, "radians to gradiens"),
I,
IDIV( 2, "", "//", "integer division"),
IMPLIES,
INF( 0, "infinity"),
INT( 1, "non-fractional part"),
LOG( 1, "base 10 logarithm"),
LN( 1, "natural logarithm"),
MAX2( 2, "maximum of 2 values"),
MIN2( 2, "minimum of 2 values"),
MOD( 2, "+%", "modulo (positive remainder)"),
MUL( 2, "\u00d7", "\u2217", "multiply: product of 2 values (each)"),
NADD( 1, "", "!+", "\u2213", "\u00b1", "CHS", "additive inverse (unary minus), change sign"),
NAN( 0, "error value NaN (not a number)"),
NAND( 2, "!&", "\u22bc", "\u2206", "binary NAND"),
NEGV, //( 1, "", "\u00af", "negative value [ABS !+]" ),
NMULT( 1, "", "!*", "\u215f", "\u00b9", "INV", "multiplicative inverse, reciprocal"),
NOR( 2, "!|", "binary NOR"),
NOT( 1, "!", "\u00ac", "negated bits, binary NOT"),
OR( 2, "", "|", "||", "\u2228", "binary OR"),
PERCENT( 1, "", "%", "percentage value (/100)"),
PI( 0, "\u03c0", "pi"),
POWER( 2, "", "**", "power"), // Do not use '^': => variables/ mappings.
PRED( 1, "predecessor"),
RADD( 1, "RAD", "radians, from degrees"),
RADG( 1, "radians, from gradiens"),
RND1( 0, "random number"),
REM, // ( 2, "-%", "remainder, returning the sign of the first value" ),
ROOT,
ROUND( 1, "rounded value"),
SIGN( 1, "value -> -1/0/1"),
SIN( 1, "sin"),
SINH( 1, "sinh"),
SHL( 2, "", "<<", "\u226a", "\u00ab", "shift left"),
SHRA( 2, "", ">>", "\u226b", "\u00bb", "arithmetic shift right"),
SHRL( 2, ">>>", "logical shift right"),
SQRT( 1, "square root"),
SQUARE( 1, "squared value"),
SUB( 2, "\u2212", "subtraction with 2 values (each)"),
SUCC( 1, "successor"),
TAN( 1, "tan"),
TANH( 1, "tanh"),
TAU( 0, "\u03c4", "tau (= 2 * pi)"),
TRUE( 0, "-1", "return -1 as odd value (= boolean TRUE)"),
TRUNC( 2, "truncate"),
XOR( 2, "|%", "\u22bb", "\u2207", "binary XOR"),

///// Any atomic type

CHOICE( 3, "OPT", "conditional value: Z Y X => X ? Y : Z"),
DATI( 1, "date, from integer representation"),
DATSL( 1, "convert 'slot second' to date"),
DATT( 1, "convert ticks in seconds to date"),
EQ( 2, "", "=", "==", "is equal"),
GE( 2, "", ">=", "\u2265", "is greater than or equal"),
GT( 2, "", ">", "is greater than"),
IDAT( 1, "integer representation of date"),
ISTYPE, //  ( 2, "is same type (compare type of 2 values)" ),
LE( 2, "", "<=", "\u2264", "is less than or equal"),
LENGTH( 1, "number of characters"),
LT( 2, "", "<", "is less than"),
MINUS( 2, "", "-", "subtract value or char's"),
NE( 2, "", "!=", "<>", "\u2260", "is not equal"),
PART( 2, "", "/", "divide or cut off"),
PLUS( 2, "", "+", "add value or char's"),
RX( 2, "", "~", "index of match for regular expression"),
RXG( 3, "", "~/", "n-th group of match for regular expression"),
RXN( 3, "", "~+", "index of n-th match for regular expression"),
RXT( 3, "", "~&", "n-th match for regular expression"),
SECT, //MSEC,
SLSD, // date to slot second
SPLITAT( 2, 2, "SUB1", "split atom at index"),
TIMES( 2, "", "*", "multiply or repeat, by value"),
UTF16,
UTF32,
UTF8,
YDAT,
YNUM,

///// Sequence/ vector/ matrix

LIMIT,

ALL, // ( -2, "return -1 if all members match P: [L] P -> Xbool" ),
AT, // (-2, "", ".", "INDX"  [L] I => L[I]
ATKEY, // ".:" associative
CONC, // ( -2, "", ",", "CONS", "concatenate" ),
FOLD, // L/R
DUP( -1, 2, "duplicate: X => X X"),
EDUPTO, // ( -2, "DUPI", "duplicate to: ... X I -> X ... X" ),
EMAP, // ( -2, "operator X (with additional args) as scalar on each member: ... X [L] -> [L']" ),
EMPTY,
EPICK, // dup item n to top ... ->
EROLL, // move item n to top ... ->
EXPAND,
HAS, // ( -2, "has as member: X [L] -> Ybool" ),
INTRS, // INTERSECTION,
IN, // ( -2, "ELEMENT", "is member of: [L] X -> Ybool" ),
//INDEX, => AT
MDROP, // ( -2, "drop first/last n members in list: [L] n -> [L']" ),
MCROSS, // cross product
//MDIA, matrix with matching diagonal
//MDIAG, // diagonal of matrix
MDIV, //( -2, "matrix division"),
MDOT, // scalar product
MEAN,
MEDIAN,
MEIG, // eigenvalues
MEYE, // identity matrix
//MFLIP,
MINV, //( -2, "matrix inversion"),
//MINP pseudo-inverse
MMUL, //( -2, "matrix multiplication"),
MODE, // modal
MONE, // matrix with n ones
MROT, // ( 1, "rotate matrix" ),
MSTD, // standard deviation
MTRP, // .' ( 1, "transpose matrix" ),
//MTRX  ..'
MZERO, // matrix with n zeros
NIP, // drop second
OF, // I [L] => L[I]
ONEOF, // ( -2, "return -1 if exactly one member matches P: [L] P -> Xbool" ),
OVER, // dup second to top
PRODUCT( -1, "\u220f", "product of sequence of values"),
RANDOM( -1, "sequence of random numbers"),
RANGE( -1, "sequence of numbers"), // ".." (1, "end/ start end/ start end step
RXSEL, // ( -2, "filter list with pattern P: [L] P -> [L']" ),
RHO,
RLDOWN, // ( -3, "roll down: X Y Z -> Y Z X" ),
RLUP, // ( -3, "roll up: X Y Z -> Z X Y" ),
ROT, // ( -3, "ROTATE", "rotate: X Y Z -> Z Y X" ),
SELECT, // ( 2, "compress list by list of booleans" ),
SEQ, // ( 2, "create running sequence" ),
SIZE, // COUNT
SIZS, // DIMS
SLICE,
SMALL, // ( -1, "is empty list or single value" ),
SOME, // ( -2, "return -1 if some members match P: [L] P -> Xbool" ),
SORT,
SUBSET,
SUPSET,
SUM( -1, "\u2211", "sum of sequence of values"),
SWAP( -2, "EXCH", "swap top 2 values: Y X -> X Y"),
TAKE, // ( -2, "take first n members: [L] n -> [L']" ),
TO, // ".="
TOKEY, // ".!" associative
TUCK, // dup below, top to third
UNION,
Z, //(1, "`", "quote: turn sequence into atomic string (literal item)"),
ZLENGTH, // length of full literal
ZRX( -2, "", "`~", "index of match for regular expression"),
ZRXG( -3, "", "`/", "n-th group of match for regular expression"),
ZRXN( -3, "", "`+", "index of n-th match for regular expression"),
ZRXSPLIT( -2, "RXS", "split literal on regex"),
ZRXT( -3, "", "`&", "n-th match for regular expression"),
ZSPLIT( -2, "SUBSTR", "split literal by range"),

///// Combinator (consumes the whole stack, resulting stack as list)

BRANCH, // [P1] [P0] V => $[V?P0:P1]
COND, // // [P2] [P1] [P0] => $[P0] ? P1 : P2
IF, // [P2] [P1] [P0] => $[P0] ? $[P1] : $[P2]
FILTER, // WHERE COMPR
// LOOP,
// RECURSE, RCLIN, RCTAIL, ...
REDUCE, // FOLD
RUN, // EXEC? (PROG)
UNLESS, // [P1] [P0] => $[P0] ? $[] : $[P1]
WHEN, // [P1] [P0] => $[P0] ? $[P1] : $[]
XDIP, // ( 2, "execute and reverse: X [P] -> R X" ),
XDUP, // ( 1, "execute and save: [P] -> [P] R" ),
XMAP, // ( 2, "execute on each member: ... [L] [P] -> [L']" ),
WHILE, // ( 2, "execute D while C: ... [D] [C] -> R" ),

///// External (not pure) 0: generic (... ==> SETQ)

ABOUT( 0, 0, "show license"),
CLEAR( -1, 0, "CLR", "CLX", "DROP", "POP", "\u0008", "\u00a2", "drop top value"),
CLR1( -1, 0, "drop top value"),
CLR2( -1, 0, "drop top 2 values"),
CLR3( -1, 0, "drop top 3 values"),
CLRALL( 0, 0, "clear all 'volatile' data (stack + memory)"),
CLOSE,
DATE( 0, "current date"),
EXISTS,
EXIT( 0, 0, "QUIT", "end program"),
FDECR( -1, "Decrypt to file, from .dib"),
FENCR( -1, "Encrypt file as .dib"),
FORALL,
HELP( 0, 0, "show help page"),
LOAD( -1, "@", "RCM", "load value from memory/ URI: name -> val"),
MC( -1, 0, "MMC", "CLRM", "clear memory value"),
MMAT, // get variable's N-th subvalue
MMCA, // ( 0, "clear memory (all values)" ),
MMKL, // get value for key within variable
MMKS, // put value for key into variable
MMLD( 1, "", "$", "MR", "load value from memory"),
MMSTO( 2, "", ":", "MS", "store value in memory"),
MMTO, // put variable as variable's N-th subvalue
OPEN,
RCQ, // "RCL", recall/load quick
RCX, // "LASTX", recall previous stack value
READ,
SEED( -1, "use as seed for random generator"),
SEEK,
SETQ, //( -2, "Set option/ configuration value"),
STEP( -1, -1, "GO", "pop operator and apply: ... [OP] -> R"),
STORE( -2, "DEF", "store/ post value in memory/ to URI: val name -> val/NaN"),
STQ, // store quick
TICK( 0, "CLOCK", "current time in sec"),
UICOD( -1, 0, "Set UI offset for Unicode characters"), // ==> SETQ
VIEW, //( 1, "set view (filter/ category)" ),
WRITE,

///// External 1: mappings

ARCHIVE, //( 0, "archive old entries" ),
DUMP( 0, /* "LSALL",*/"display all"),
EXPALL( -1, "export all data (incl. keys!) as plain CSV/TSV (careful!)."),
EXPORT( -1, "export data as plain CSV/TSV to file etc."),
IMPORT( -1, "import data from file etc."),
PW( -1, "set overall password"),
PWAC( -1, "set app's access code"),
QAT, // "^."
QATKEY, // "^:"
QCAT( -2, "change mapping's categories: OID cats ->"),
QDEL( -1, "QCLR", "delete mapping for given OID"),
QFILTER( 0, 0, "switch category for filtering"),
QLOAD( -1, "get data for given OID"),
QOID( -1, "get first OID for label and current cat: name -> OID"),
QNEXT( -1, "get next OID for same label"),
QQL( -1, "@^", "get data for mapping's label and current cat"),
QSTORE( -3, "store new mapping: data cats label ->"),
QS( -2, ":^", "store new mapping for current cats: data label ->"),
QTO,
QTOKEY,
QUP( -2, "update/ replace data of mapping: OID data ->"),
SAVTO( -1, "save all data as encoded copy to named file"),
//VWCAT( -1, "set cat for viewing data") ==> ... CAT SETQ
// VWFILTER,

/////,
;

public final int cArgs;
public final boolean zipped;
public final int cReturnValues;
public final String[] optionals;
public final String description;

private static char[] functSymbols;
private static QCalc[] functEnums;
private static Random zRandom = new Random( 42);

/////

static {
	TreeMap<Integer, QCalc> map = new TreeMap<Integer, QCalc>();
	for (QCalc funct : QCalc.values()) {
		for (String opt : funct.optionals) {
			if (1 == opt.length()) {
				map.put( opt.charAt( 0) & 0xffff, funct);
			}
		}
	}
	functSymbols = new char[map.size()];
	functEnums = new QCalc[functSymbols.length];
	int cnt = 0;
	for (Integer ch : map.keySet()) {
		functSymbols[cnt] = (char) (int) ch;
		functEnums[cnt++] = map.get( ch);
	}
}

private QCalc() {
	cArgs = -1;
	zipped = true;
	cReturnValues = 0;
	optionals = new String[0];
	description = null;
}

private QCalc( int xcArgs, int xcReturnValues, String... xmOptionals) {
	cArgs = (0 <= xcArgs) ? xcArgs : -xcArgs;
	zipped = (0 < xcArgs);
	cReturnValues = xcReturnValues;
	optionals = ((null != xmOptionals) && (1 <= xmOptionals.length)) ? Arrays.copyOf( xmOptionals, xmOptionals.length - 1)
		: xmOptionals;
	description = ((null != xmOptionals) && (1 <= xmOptionals.length)) ? (xmOptionals[xmOptionals.length - 1]) : null;
}

private QCalc( int xcArgs, String... xmOptionals) {
	cArgs = (0 <= xcArgs) ? xcArgs : -xcArgs;
	zipped = (0 < xcArgs);
	cReturnValues = 1;
	optionals = ((null != xmOptionals) && (1 <= xmOptionals.length)) ? Arrays.copyOf( xmOptionals, xmOptionals.length - 1)
		: xmOptionals;
	description = ((null != xmOptionals) && (1 <= xmOptionals.length)) ? (xmOptionals[xmOptionals.length - 1]) : null;
}

public static QCalc getOperator( String cmd) {
	if ((null == cmd) || (0 >= cmd.length())) {
		return null;
	}
	QCalc funct = null;
	try {
		funct = QCalc.valueOf( StringFunc.toUpperCase( cmd));
	} catch (Exception e) {
		funct = null;
	}
	if ((null == funct) && (1 == cmd.length())) {
		final char c0 = cmd.charAt( 0);
		int iSym = Arrays.binarySearch( functSymbols, c0);
		if (0 <= iSym) {
			funct = functEnums[iSym];
		}
	}
	if (null == funct) {
		for (QCalc fx : QCalc.values()) {
			for (int i0 = fx.optionals.length - 1; i0 >= 0; --i0) {
				if (fx.optionals[i0].equals( cmd)) {
					funct = fx;
					break;
				}
			}
		}
	}
	if (null == funct) {
		final String cmdU = StringFunc.toUpperCase( cmd);
		for (QCalc fx : QCalc.values()) {
			final String nam = fx.name();
			if (((3 <= nam.length()) && cmdU.startsWith( nam))
				|| ((4 <= cmdU.length()) && nam.startsWith( cmdU))) {
				funct = fx;
				break;
			}
		}
	}
	return funct;
}

/////

public String getOperator() {
	return ((1 < optionals.length) && (0 >= optionals[0].length())) ? optionals[1] : null;
}

public String getOperatorOrName() {
	return ((1 < optionals.length) && (0 >= optionals[0].length())) ? optionals[1] : name();
}

public String getDescription() {
	StringBuilder out = new StringBuilder( 100);
	if (null == description) {
		out.append( '.');
	}
	out.append( name() + " (" + cArgs + ")  \t");
	for (String opt : optionals) {
		if (0 < opt.length()) {
			out.append( " " + opt + ' ');
		}
	}
	if (null != description) {
		out.append( "\t" + description);
	}
	return out.toString();
}

public static double getNextRandom() {
	return zRandom.nextDouble();
}

private static QVal convertDat( QResult pooled, double numD4, QCalc conv) {
	QVal out0 = QMapSto.NaN;
	if (!Double.isNaN( numD4)) {
		long vx = (long) numD4;
		final long xmint = vx / 100 % 100;
		final long xsec = vx % 100;
		vx /= 10000;
		String str = "";
		switch (conv) {
		case DATI:
			str = String.format( "%d-%02d-%02dT%02d:%02d", (int) vx / 10000, ((int) vx / 100) % 100,
				(int) vx % 100, xmint, xsec);
			break;
		case DATT:
			str = MiscFunc.dateLocal4Millis( true, ((long) numD4) / 10);
			break;
		case DATSL:
			str = MiscFunc.date4SlotSecondApprox( numD4);
			break;
		default:
			;
		}
		out0 = QMapSto.qval4AtomicValue( pooled, str);
	}
	return out0;
}

public static QVal doRegEx( QResult pooled, String str, String pattern, double inxD4, QCalc kind) {
	QVal out0 = QMapSto.NaN;
	try {
		int inx = (int) (inxD4 / 1.0e4);
		final Pattern pat = Pattern.compile( pattern);
		final Matcher mat = pat.matcher( str);
		if ((kind == QCalc.RXG) || (kind == QCalc.ZRXG)) {
			if (mat.find()) {
				out0 = QMapSto.qval4String( pooled, mat.group( inx));
			}
			return out0;
		}
		int count = (0 <= inx) ? inx : 99999;
		int i0 = -1;
		while (mat.find()) {
			++i0;
			if (i0 >= count) {
				break;
			}
		}
		if (0 > inx) {
			count = i0 + inx + 1;
			i0 = count - 1;
			if (0 <= count) {
				mat.reset();
				i0 = -1;
				while (mat.find()) {
					++i0;
					if (i0 >= count) {
						break;
					}
				}
			}
		}
		if (i0 < count) {
			out0 = QMapSto.NaN;
		} else if ((kind == RXT) || (kind == ZRXT)) {
			out0 = QMapSto.qval4String( pooled, mat.group());
		} else {
			out0 = QMapSto.qval4DoubleD4( mat.start() * 1.0e4);
		}
	} catch (Exception e) {
		out0 = QMapSto.NaN;
	}
	return out0;
}

/** Handle numeric cases. Return null if n.a. */
public double[] calc( double[] argsD4) {
	if (cArgs != argsD4.length) {
		return null;
	}
	double out0 = Double.NaN;
	double out1 = Double.NaN;
	int cOut = 1;
	try {
		long vx = 0;
		long vy = 0;
		switch (this) {
		case ABS:
			out0 = (0 > argsD4[0]) ? (-argsD4[0]) : argsD4[0];
			break;
		case ACOS:
			out0 = Math.acos( argsD4[0] / 1.0e4) * 1.0e4;
			break;
		case ADD:
			out0 = argsD4[0] + ((1 == cArgs) ? 1.0e4 : argsD4[1]);
			break;
		// case ALL:
		case AND:
			// case '\u2227':
			vx = (long) (argsD4[0] / 1.0e4) & (long) (argsD4[1] / 1.0e4);
			out0 = vx * 1.0e4;
			break;
		case ASIN:
			out0 = Math.asin( argsD4[0] / 1.0e4) * 1.0e4;
			break;
		// case AT:
		case ATAN:
			out0 = Math.atan( argsD4[0] / 1.0e4) * 1.0e4;
			break;
		case CEIL:
			out0 = Math.ceil( argsD4[0] / 1.0e4) * 1.0e4;
			break;
		//			case CLEAR:
		//				// case '\u00a2':
		//				return new double[ 0 ];
		case COMP:
			out0 = MiscFunc.equalRounded( argsD4[0], argsD4[1]) ? 0.0 : ((argsD4[0] > argsD4[1]) ? 1.0e4 : -1.0e4);
			break;
		// case CONC:
		// case COND:
		case COS:
			out0 = Math.cos( argsD4[0] / 1.0e4) * 1.0e4;
			break;
		case COSH:
			out0 = Math.cosh( argsD4[0] / 1.0e4) * 1.0e4;
			break;
		// case CROSS:
		// case CUT:
		// case DATE:
		case DEG:
			out0 = argsD4[0] * 180.0 / Math.PI;
			break;
		case DIV:
			// case '\u00f7':
			out0 = (argsD4[0] * 1.0e4 / argsD4[1]);
			break;
		// case DROP:
		case DUP:
			out0 = argsD4[0];
			out1 = out0;
			cOut = 2;
			break;
		case E:
			out0 = Math.E * 1.0e4;
			break;
		// case ELEMENT:
		// case EMPTY:
		case EQ:
			out0 = MiscFunc.equalRounded( argsD4[0], argsD4[1]) ? -1.0e4 : 0.0;
			break;
		case XDIP:
		case XDUP:
		case STEP:
		case XMAP:
		case BRANCH:
			return null; // external
		//			case EXISTS:
		//			case EXPAND:
		case WHILE:
			return null; // external
		case FACT:
			vx = (long) (argsD4[0] / 1.0e4);
			if (200 >= vx) {
				out0 = vx;
				for (int i0 = (int) vx - 1; i0 >= 2; --i0) {
					out0 *= i0;
				}
				out0 *= 1.0e4;
			} else {
				out0 = Double.NaN;
			}
			break;
		case FALSE:
			out0 = 0.0;
			break;
		// case FILTER:
		case FLOOR:
			out0 = Math.floor( argsD4[0] / 1.0e4) * 1.0e4;
			break;
		// case FORALL:
		case FRAC:
			vx = (((long) (argsD4[0])) / 10000) * 10000;
			out0 = argsD4[0] - vx;
			break;
		case GE:
			out0 = (MiscFunc.equalRounded( argsD4[0], argsD4[1]) || (argsD4[0] >= argsD4[1])) ? -1.0 : 0.0;
			break;
		// case GET:
		case GRAD:
			out0 = argsD4[0] * 200.0 / Math.PI;
			break;
		case GT:
			out0 = (!MiscFunc.equalRounded( argsD4[0], argsD4[1]) && (argsD4[0] > argsD4[1])) ? -1.0 : 0.0;
			break;
		// case HELP:
		case IDIV:
			vx = (long) MiscFunc.roundForIntUsage( argsD4[0]) / (long) MiscFunc.roundForIntUsage( argsD4[1]);
			out0 = vx * 1.0e4;
			break;
		// case IS:
		// case I:
		// case IMPLIES:
		// case IN:
		case INF:
			out0 = Double.POSITIVE_INFINITY;
			break;
		// case INDEX:
		case INT:
			vx = (((long) (argsD4[0])) / 10000) * 10000;
			out0 = vx;
			// case INTERSECTION:
			// case ISNT:
		case LE:
			out0 = (MiscFunc.equalRounded( argsD4[0], argsD4[1]) || (argsD4[0] <= argsD4[1])) ? -1.0 : 0.0;
			break;
		// case LIMIT:
		case LOG:
			out0 = (Math.log10( argsD4[0]) - 4.0) * 1.0e4;
			break;
		case LN:
			out0 = Math.log( argsD4[0] / 1.0e4) * 1.0e4;
			break;
		case LT:
			out0 = (!MiscFunc.equalRounded( argsD4[0], argsD4[1]) && (argsD4[0] < argsD4[1])) ? -1.0 : 0.0;
			break;
		// case MAP:
		case MAX2:
			out0 = (argsD4[0] >= argsD4[1]) ? argsD4[0] : argsD4[1];
			break;
		// case MDIV:
		// case MINV:
		// case MMUL:
		case MIN2:
			out0 = (argsD4[0] <= argsD4[1]) ? argsD4[0] : argsD4[1];
			break;
		case MOD:
			vy = (0 <= argsD4[1]) ? (long) argsD4[1] : -(long) argsD4[1];
			out0 = ((long) (argsD4[0] / 1.0e4)) % (vy / 10000);
			out0 *= 1.0e4;
			out0 = (0 <= out0) ? out0 : (out0 + vy);
			break;
		// case MROT:
		// case MSEC:
		// case MTRP:
		case MUL:
			out0 = argsD4[0] * argsD4[1] / 1.0e4;
			break;
		case NADD:
			// case '_':
			// case '\u2213':
			// case '\u00b1':
			out0 = -argsD4[0];
			break;
		case NAN:
			out0 = Double.NaN;
			break;
		case NAND:
			// case '\u22bc':
			// case '\u2206':
			vx = ~((long) (argsD4[0] / 1.0e4) & (long) (argsD4[1] / 1.0e4));
			out0 = vx * 1.0e4;
			break;
		case NE:
			out0 = !MiscFunc.equalRounded( argsD4[0], argsD4[1]) ? -1.0 : 0.0;
			break;
		case NMULT:
			// case ';':
			// case '\u215f':
			// case '\u00b9':
			out0 = 1.0e8 / argsD4[0];
			break;
		case NOP:
			return new double[0];
		case NOR:
			vx = ~((long) (argsD4[0] / 1.0e4) & ~(long) (argsD4[1] / 1.0e4));
			out0 = vx * 1.0e4;
			break;
		case NOT:
			vx = ~(long) (argsD4[0] / 1.0e4);
			out0 = vx * 1.0e4;
			break;
		// case OF:
		// case ONEOF:
		// case OPT:
		case OR:
			// case '\u2228':
			vx = (long) (argsD4[0] / 1.0e4) | (long) (argsD4[1] / 1.0e4);
			out0 = vx * 1.0e4;
			break;
		case PERCENT:
			out0 = argsD4[0] / 100.0;
			break;
		case PI:
			out0 = Math.PI * 1.0e4;
			break;
		case POWER:
			out0 = Math.pow( argsD4[0] / 1.0e4, argsD4[1] / 1.0e4) * 1.0e4;
			break;
		case PRED:
			vx = (long) (argsD4[0] / 1.0e4);
			out0 = (vx - 1) * 1.0e4;
			break;
		// case PRODUCT:
		// case PUT:
		case RADD:
			out0 = argsD4[0] * Math.PI / 180.0;
			break;
		case RADG:
			out0 = argsD4[0] * Math.PI / 200.0;
			break;
		// case RANDOM:
		// case REDUCE:
		// case RHO:
		// case RLDOWN:
		// case RLUP:
		// case ROOT:
		// case ROTATE:
		case RND1:
			out0 = getNextRandom() * 1.0e4;
			break;
		case ROUND:
			out0 = Math.round( argsD4[0] / 1.0e4) * 1.0e4;
			break;
		// case SELECT:
		// case SEQ:
		case SIGN:
			out0 = MiscFunc.equalRounded( argsD4[0], 0.0) ? 0.0 : ((0.0 < argsD4[0]) ? 1.0e4 : -1.0e4);
			break;
		case SIN:
			out0 = Math.sin( argsD4[0] / 1.0e4) * 1.0e4;
			break;
		case SINH:
			out0 = Math.sinh( argsD4[0] / 1.0e4) * 1.0e4;
			break;
		// case SIZE:
		case SHL:
			if (0.0 <= argsD4[1]) {
				vx = (long) (argsD4[0] / 1.0e4) << (long) (argsD4[1] / 1.0e4);
			} else {
				vx = (long) (argsD4[0] / 1.0e4) >>> -(long) (argsD4[1] / 1.0e4);
			}
			out0 = vx * 1.0e4;
			break;
		case SHRA:
			vx = (long) (argsD4[0] / 1.0e4) >> (long) (argsD4[1] / 1.0e4);
			out0 = vx * 1.0e4;
			break;
		case SHRL:
			vx = (long) (argsD4[0] / 1.0e4) >>> (long) (argsD4[1] / 1.0e4);
			out0 = vx * 1.0e4;
			break;
		// case SMALL:
		// case SOME:
		//case SORT:
		// case SPLIT:
		case SQRT:
			out0 = Math.sqrt( argsD4[0]) * (1.0e4 / 1.0e2);
			break;
		case SQUARE:
			out0 = argsD4[0] * argsD4[0] / 1.0e4;
			break;
		case SUB:
			out0 = argsD4[0] - ((1 == cArgs) ? 1 : argsD4[1]);
			break;
		// case SUBSET:
		case SUCC:
			vx = (long) (argsD4[0] / 1.0e4);
			out0 = (vx + 1) * 1.0e4;
			break;
		// case SUM:
		// case SUPERSET:
//		case SWAP:
//			out0 = arguments[1];
//			out1 = arguments[0];
//			cOut = 2;
//			break;
		// case TAIL:
		// case TAKE:
		case TAN:
			out0 = Math.tan( argsD4[0] / 1.0e4) * 1.0e4;
			break;
		case TANH:
			out0 = Math.tanh( argsD4[0] / 1.0e4) * 1.0e4;
			break;
		case TAU:
			out0 = Math.PI * 2 * 1.0e4;
			break;
		// case TICK:
		case TRUE:
			out0 = -1.0 * 1.0e4;
			break;
		case TRUNC:
			out0 = ((0.0 <= argsD4[0]) ? Math.floor( argsD4[0] / 1.0e4) : Math.ceil( argsD4[0] / 1.0e4)) * 1.0e4;
			break;
		// case UNION:
		case XOR:
			// case '\u22bb':
			// case '\u2207':
			//				long vx = (((long) vals[ 0 ]) & ((1L << 48) - 1)) ^ (((long) vals[ 1 ]) & ((1L << 48) - 1));
			vx = (long) (argsD4[0] / 1.0e4) ^ (long) (argsD4[1] / 1.0e4);
			out0 = vx * 1.0e4;
			break;
		// case WRITE:

		default:
//			out0 = Double.NaN;
			return null;
		}
	} catch (Exception e) {
		out0 = Double.NaN;
	}
	out0 = MiscFunc.roundForIntUsage( out0);
	if (2 <= cOut) {
		return new double[] { out0, MiscFunc.roundForIntUsage( out1) };
	}
	return new double[] { out0 };
}

/** Handle scalars and simple cases. */
public QVal[] calc( QVal[] arguments) {
	if ((null == arguments) || (cArgs != arguments.length)) {
		return null;
	}
	final QResult pooled = QResult.get8Pool();
	switch (this) {
	case MINUS:
	case PLUS:
	case PART:
	case TIMES:
		if (QMapSto.isNumeric( arguments[0]) && QMapSto.isNumeric( arguments[1])) {
			switch (this) {
			case MINUS:
				return SUB.calc( arguments);
			case PLUS:
				return ADD.calc( arguments);
			case PART:
				return DIV.calc( arguments);
			case TIMES:
				return MUL.calc( arguments);
			default:
				;
			}
		}
	default:
		;
	}
	QVal out0 = null;
	QVal out1 = null;
	int cOut = 1;
	boolean todo = false;
	String str = "";
	double num;
	long nLong;
//	int inx = 0;
	//	byte[] dat;
	//	long[] handles;
	try {
		switch (this) {
//		case ALL:
//			todo = true;
//			break;
//		case AT:
//			todo = true;
//			break;
		case CHOICE:
			num = QMapSto.doubleD4oQVal( arguments[2]);
			out0 = arguments[0];
			if (!Double.isNaN( num) && (0 != (1 & (long) (num / 1.0e4)))) {
				out0 = arguments[1];
			}
			break;
		case CONC:
			todo = true;
			break;
		case DATI:
		case DATT:
		case DATSL:
			num = QMapSto.doubleD4oQVal( arguments[0]);
			out0 = convertDat( pooled, num, this);
			break;
//		case MDROP:
//			todo = true;
//			break;
		case DUP:
			out0 = arguments[0];
			out1 = out0;
			cOut = 2;
			break;
//		case EDUPTO:
//			todo = true;
//			break;
//		case EMPTY:
//			todo = true;
//			break;
		case EQ:
			todo = true;
			break;
//		case EXISTS:
//			todo = true;
//			break;
//		case EXPAND:
//			todo = true;
//			break;
//		case RXSEL:
//			todo = true;
//			break;
		case GE:
			todo = true;
			break;
		case GT:
			todo = true;
			break;
//		case INTRS:
//			todo = true;
//			break;
		case ISTYPE:
			todo = true;
			break;
		case IDAT:
			str = QMapSto.string4QVal( arguments[0]).replaceAll( "[^0-9T]", "").replace( 'T', '.');
			//				long v0 = MiscFunc.leapMinute256ForDate( date );..
			out0 = QMapSto.qval4AtomicValue( pooled, str);
			break;
//		case IN:
//			todo = true;
//			break;
		case LE:
			todo = true;
			break;
		case LENGTH:
			str = QMapSto.string4QVal( arguments[0]);
			out0 = QMapSto.qval4DoubleD4( str.length() * 1.0e4);
			break;
		case LT:
			todo = true;
			break;
//		case EMAP:
//			todo = true;
//			break;
//		case MSEC:
//			todo = true;
//			break;
		case NE:
			todo = true;
			break;
		case NOP:
			todo = false;
			break;
//		case OF:
//			todo = true;
//			break;
//		case ONEOF:
//			todo = true;
//			break;
		case PLUS:
			str = QMapSto.string4QVal( arguments[0]) + QMapSto.string4QVal( arguments[1]);
			out0 = QMapSto.qval4AtomicLiteral( pooled, str);
			break;
//		case RLDOWN:
//			todo = true;
//			break;
//		case RLUP:
//			todo = true;
//			break;
//		case ROT:
//			todo = true;
//			break;
		case RXG:
		case RXN:
		case RXT:
			out0 = doRegEx( pooled, QMapSto.string4QVal( arguments[0]), QMapSto.string4QVal( arguments[1]),
				QMapSto.doubleD4oQVal( arguments[2]), this);
			break;
		case RX:
			out0 = doRegEx( pooled, QMapSto.string4QVal( arguments[0]), QMapSto.string4QVal( arguments[1]),
				0.0, this);
			break;
		case SEED:
			nLong = (long) (Integer.MAX_VALUE * QMapSto.doubleD4oQVal( arguments[0]) / 1.0e4);
			zRandom = new Random( nLong);
			out0 = QMapSto.qval4DoubleD4( nLong * 1.0e4);
			break;
//		case SELECT:
//			todo = true;
//			break;
//		case SIZE:
//			todo = true;
//			break;
//		case SLICE:
//			todo = true;
//			break;
//		case SMALL:
//			todo = true;
//			break;
//		case SOME:
//			todo = true;
//			break;
//		case SORT:
//			todo = true;
//			break;
		case SPLITAT:
			num = QMapSto.doubleD4oQVal( arguments[1]) / 1.0e4;
			str = QMapSto.string4QVal( arguments[0]);
			out0 = QMapSto.qval4AtomicLiteral( pooled, str.substring( 0, (int) num));
			out1 = QMapSto.qval4AtomicLiteral( pooled, str.substring( (int) num));
			cOut = 2;
			break;
//		case SUBSET:
//			todo = true;
//			break;
//		case SUPSET:
//			todo = true;
//			break;
//		case TAKE:
//			todo = true;
//			break;
		case TIMES:
			str = QMapSto.string4QVal( arguments[0]);
			double factor = QMapSto.doubleD4oQVal( arguments[1]) / 1.0e4;
			if ((2 <= factor) && (factor <= 1000)) {
				int i0 = (int) factor;
				StringBuilder out = new StringBuilder( i0 * str.length() + 2);
				for (; i0 > 0; --i0) {
					out.append( str);
				}
				str = out.toString();
				out0 = QMapSto.qval4AtomicLiteral( pooled, str);
			}
			break;
//		case UNION:
//			todo = true;
//			break;
		default: {
			double[] doubles = new double[arguments.length];
			int cnt = 0;
			for (QVal arg : arguments) {
				doubles[cnt++] = QMapSto.doubleD4oQVal( arg);
			}
			doubles = calc( doubles);
			if (null != doubles) {
				cOut = doubles.length;
				if (2 <= cOut) {
					out1 = QMapSto.qval4DoubleD4( doubles[1]);
				}
				if (1 <= cOut) {
					out0 = QMapSto.qval4DoubleD4( doubles[0]);
				}
			}
		}
		}
		if (todo) {
			out0 = QMapSto.qval4AtomicLiteral( pooled, "(function not implemented)");
		}
	} catch (Exception e) {
		return null;
	}
	if (2 <= cOut) {
		return new QVal[] { out0, out1 };
	} else if (null == out0) {
		return null;
	}
	return new QVal[] { out0 };
}
//=====
}
