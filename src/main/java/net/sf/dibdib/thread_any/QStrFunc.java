// Copyright (C) 2016,2017,2018,2019,2020  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_any;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.regex.*;
import net.sf.dibdib.config.Dib2Constants;

/** 'Quick' string utils: text segmentation and sorting (cmp UtilString and QMap.QVal!),
 *  with 'shash' as sortable hash value,
 *  to be used in QMap/QVal: 'shash' + original string as required.
 */
public final class QStrFunc {
//=====

/*
String handling, definitions/ data types:

QSTR is a String that represents SEQuences of ATOMs: ATOM is the primitive item,
CATenation the primitive operation, and SEQs are the corresponding primitive terms.
Main data types (implemented via QMap.QVal/long [] = QSEQ/LIST, QVAL = instances):
-- empty SEQuence (= NIL for "", also used as special delimiter)
-- single ATOM (= sequence with 1 item): QDATE = calendric time value,
   QNUM = numeric value, QWORD = atomic literal
-- (primitive) supporting (no 'sememe'): QERROR = error reference, QSLIT = structural literal, 
   QFLIT = delimiting/ functional literal (NIL as special delimiter)
-- aggregations (list, bag, map, association)

CHAR0 = \0,..\7f
CHAR1 = \1,..\7f
CHAR(X) = \0,..\7fffffff 
CTL0 = \0,..\1f
CTL1 = \80,..\9f
SP = \20 (' ')
SHCHAR = \e000,..\f7ff
VCHAR(X) = CHAR except SP, CTL0, CTL1, SHCHAR, surrogate, \2000,..\200f, \2029,..\202f, \2060,..

QWORD (atomic literal) = sequence of letters or literal digits, possibly mixed with others
QSLIT (structural literal) = sequence of CTL0 char's, possibly mixed with CHARX
QFLIT (functional literal: delimiting, punctuation etc.) = sequence of other char's
Non-literals: QDATE, QNUM (with/ without units), QERROR
(Note: A QWORD may have the same String representation as a QNUM etc., but is considered
as different <= the value 12345.0 is different from '12345' as telephone number or ID)

LIST = packaged sequence = list of LIST_ITEMs/ array of QVALs
QSEQ = array, to be used as sequence of QVALs
QTERM/QPAR = (extended) literal = sequence of primitives, with SP as default delimiter
TERM = sequence, possibly containing packaged aggregations 
QBAG = array, to be used as bag
QSET = array, to be used as set
QMAP = array of pairs: LITERAL_ITEM + LIST_ITEM
QASSOC/QMAP3 = array of triples: LITERAL_ITEM + LITERAL_ITEM + LIST_ITEM

(Q)STRING = (any) String of CHARs, usually excluding CTL0 (and avoiding CTL1 etc.)
            for representing primitives or QSEQ

=> Using handles for TOKENs, LITERALs and STRUCTs 

Parser:
1. string4Mnemonics: '_' + ... + '_' > ...,
   quotes: '_{{' > STX, '}}_' > ETX,
   lists:  '_[[' > SLS, ']]_' > ELS,
   templ.: '_<'  > SO,   '>_' > SI,
2. atoms4String:
   balance quotes, transform into sequence of ATOMs
3. lists4Atoms:
   balance lists, remove list SEPs
   remove outer STX/ ETX pairs from ATOMs 

...
token: word (string of digits, ...) / float (date, ...)
sememe: predicate/ operator/ restrictor/ selector/ descriptor
expression: '(' operator [' ' term]* ')'
predication: '{' predicate [' ' term]+ '}'
substitution: '{' atom '}'
term: atom/ float/ group/ selector
group: descriptor [ '(' restrictor ')' ]
atom: word/ '<' word [' ' word]* '>'/ group '.' selector
word: [symbol/ alpha/ digit]* symbol/alpha/num [symbol/ alpha/ digit]*
restrictor/ descriptor/ ...: word
float: [d]+/ '0x' [d]+/ '0.' [d]+ ['e' ['+'/ '-'] d] ['~` d] ['_' [word '*' '/']+]/ ...
marker: (see below)

Float: normalized:
start with '0.' followed by '1'-'9' and more digits, plus exponent plus precision info plus unit
e.g.: 0.123e3~2_kg.m/s/s = (with last 2 digits plus binary rounding not reliable:) [123 - 64, 123 + 64] kg*m/sec^2.
Units as ASCII: 'm', 's', 'kg', ... with '$', pound -> '#', euro -> ',', yen -> '%', and ':' for date

Shash (sortable hash):
with collation key as bitlist for words ('a' is sorted like 'A' etc.: normalization form NFKC, strength PRIMARY)
plus special float encoding for numbers,
use Unicode private area (0xE100..0xF7FF):
0000 0000 000.....  control chars
0000 0000 00000001  - QQ
0000 0000 00000010  - QX
0000 0000 00000011  - QM
1110 0000 1.......? ?  (tag)
1110 0001 00000000  NaN as QERROR
1110 0001 00......  ?QERROR reference
1110 0001 1.......  ? (tag)
1110 0001 10......  ? format left (template)
1110 0001 11......  ? format right
1110 0001 11111111  TMP: numeric with original string
1110 0010 000000..  QFLIT delimiter/ separator (punctuation etc.)
1110 0010 000001..  QSLIT structurual literal as hidden separator ...
1110 0010 000010..  RFU (struc +)
1110 0010 000011..  RFU (struc X)
1110 0010 00110xxx  Bitlist (? ..due to surr. area only 8 per char)/ byte[] with x trailing 0 bits
1110 0010 0010xxxx  Bitlist (? 12 bits per char | 0x4000) with x trailing 0 bits
1110 0010 00111111  Bitlist with temporary format and preceding marker ('X', 'A', 'B', 'I', 'P')
1110 0011 0.......  TODO QNUM numeric value with units (6 bit coll key + 1 bit mul/div-marker)
                    (':' for imaginary part of complex numbers)
1110 0011 10......  QDATE as seconds in yearly slots with flipped sign bit << 7 + TZ info: 40+1+7+9 bits
1110 0011 1111111.  RFU (negative float with extended range)
1110 01.. ........  negative float (>= double precision), pos. exponent: IEEE bits toggled
1110 10.. ........  negative float >= -2 (bits toggled (+1) for sorting!)
1110 1100           NaN, sorted ahead of 0 (plus value for optional types)
1110 1100 00000000  0
1110 11.. ........  positive float with neg. or 0 exponent: IEEE bits
1111 00.. ........  positive float with pos. exponent
DEPR 1111 0100 0.......  positive float with extended range (RFU)
1111 0011 1111111.  RFU (positive float with extended range)
DEPR 1111 0100 1.......  float with units
DEPR 1111 0101 ........  collation key (depends on Java implementation)
1111 01.. ........  collation key (depends on Java implementation)
DEPR 1111 0101 ........  indicator for (condensed) collation key (depends on Java implementation)
DEPR 1111 0101 0.......  - 7-bit encoded chars from 16 collation bits (first byte == 0x00)
DEPR 1111 0101 10000101  - bytes from collation bits (first byte < 0x80, excluding 0x00)
DEPR 1111 0101 10000110  - 16-bit uints (excl. 0) from 16 collation bits (first byte == 0x80)
DEPR 1111 0101 10000111  - bytes from collation bits (first byte > 0x80, excluding 0x00)
DEPR 1111 011. ........  9 significant bits of original char (e.g. control chars)  
0x0 as filler, subsequent chars have to be >= 0x20 !
(used chars must not overlap with control chars!)
01.. .... ........  for subsequent 12-/14-bit pieces
DEPR 0....... 1........  for subsequent 7-/14-bit pieces
DEPR 10...... .........  for subsequent 14-bit pieces (avoids surrogate area)
........ .........  (excluding 0x00.., 0x..00, 0xd...) for subsequent byte pieces

floatD4=10000*float: as a compromise for the precision of long int's and decimal floats 
floatD4 with at least 2 bytes, value pieces encoded as IEEE floats with special toggling:
111. ..             (encoded bits for sign bit with 1 bit of exponent)
       .. ........  (remaining 10 bits of exponent for double precision, possibly toggled)
+ 10..............* (14 bits of value (significand), in several chunks as needed)
1111 0100 1.......  float with first 7 bits of unit
+0....... 1.......* (7 bit ASCII pieces from unit name or '.' for factor or '/')
+111..... ........  (exponent as above, followed by significand as above)
+01...... 0.......  (RFU)

e.g.:
E    type   exp  exp   val  val
1111 00 00 0000 0000 1 100 0000 0000 0000 = 1.1(2) * 2^1 = 11(2) = 3
1111 00 00 0000 0000 1 000 0000 0000 0000 = 1.0(2) * 2^1 = 2
1110 11 11 1111 1111 1 100 0000 0000 0000 = 1.1(2) * 2^0 = 1.1(2) = 1.5
1110 11 11 1111 1111 1 000 0000 0000 0001 = 1.000000000000001(2) * 2^0
1110 11 11 1111 1111 1 000 0000 0000 0000 1000 ... 0001 ~ 1.000...2
1110 11 11 1111 1111 1 000 0000 0000 0000 = 1.0(2) * 2^0 = 1
1110 11 00 0000 0001 1 000 0000 0000 0000 = 1.0(2) * 2^-1022
1110 11 00 0000 0000 1 000 0000 0000 0000 = 0
1110 10 00 0000 0001 1 000 0000 0000 0000 = -1
1110 10 00 0000 0000 1 000 0000 0000 0000 = -2

 */

/*
Cmp. icu-project.org:
-- Primary weights can be 1 to 4 bytes in length. (If a character has a 3-byte
CE primary weight, we'll call it a 3-byter, for example)
-- They have the restriction that no weight can be a proper initial sequence
of another. Eg, if X has weight 85 42, then no three or four byte weight
can start with 85 42.
-- It is important that the most frequent characters have short weights.
So, for example, in U5.2 we have {space, A-Z} being 1-byters.
-- The first bytes are important, and are allocated to special ranges.
[Java uses double bytes/ ...: Special case: 0x80..., trailing 0x00! for concatenation] 
 */

private static final String REGEX_SPACE_NL = "[\\s\\p{Z}]";
static final Pattern PATTERN_SPACE_NL = Pattern.compile( REGEX_SPACE_NL);
static final Pattern PATTERN_SPACE_NL_SEQ = Pattern.compile( REGEX_SPACE_NL + "+");
static final Pattern PATTERN_SPACE_BEGIN = Pattern.compile( "^\\s+");
static final Pattern PATTERN_SPACE_END = Pattern.compile( "\\s+$");
public static final String REGEX_LINE_BREAK = "\\r?[\\n\\u0085\\u2028\\u2029]"; // "\\r?[\\n\\v\\u0085\\u2028\\u2029]";
public static final Pattern PATTERN_LINE_BREAK_TAB = Pattern.compile( "\\r?[\\n\u0085\u2028\u2029\\t]"); // "\\r?[\\n\\v\\u0085\\u2028\\u2029\\t]"
public static final Pattern PATTERN_WORD_CONNECTOR = Pattern
	.compile( "[\\p{L}\\p{M}\\p{N}\\p{Pc}[\\p{InEnclosedAlphanumerics}&&\\p{So}]]+");
public static final Pattern PATTERN_WORD_BASIC = Pattern.compile( "[\\p{L}\\p{M}\\p{N}]+");
public static final Pattern PATTERN_WORD_SYMBOL = Pattern.compile( "[\\p{L}\\p{M}\\p{N}\\p{S}]+");
public static final Pattern PATTERN_SYMBOLS = Pattern.compile( "\\p{S}+");
//public static final Pattern PATTERN_LETTERS_CASED = Pattern.compile( "\\p{L&}+" );
public static final Pattern PATTERN_PUNCTUATION = Pattern.compile( "\\p{P}+");
public static final Pattern PATTERN_CONTROLS_UNI = Pattern.compile( "\\p{Cc}+");
public static final Pattern PATTERN_CONTROLS_ANSI = Pattern.compile( "\\p{Cntrl}+");
public static final Pattern PATTERN_DIGITS = Pattern.compile( "\\p{Nd}+");
public static final Pattern PATTERN_DIGITS_BASIC = Pattern.compile( "\\p{Digit}+");
public static final Pattern PATTERN_HEXS = Pattern.compile( "\\p{XDigit}+");
public static final Pattern PATTERN_NUMERICS = Pattern.compile( "\\p{N}+");

//private static final String REGEX_CURRENCY = "\\p{Sc}";
private static final String REGEX_NUMBER = "((([\\+\\-])|([0-9]\\.)|([1-9]))[0-9_\u00B7\\'\\.\\,]*[0-9])|([0-9])";
private static final String REGEX_NUMBER_SEP = "[_\u00B7\\']";
private static final Pattern PATTERN_NUMBER_SEP = Pattern.compile( REGEX_NUMBER_SEP);
private static final String REGEX_UNIT_SUFFIX = "_[\\./A-z0-9\\p{Sc}]+";
private static final Pattern PATTERN_UNIT = Pattern.compile( REGEX_UNIT_SUFFIX);
private static final String REGEX_DIGITS_TEL_ETC = "[\\+\\#0][0-9\\-\\*]+\\#?";
private static final Pattern PATTERN_DIGITS_TEL_ETC = Pattern.compile( REGEX_DIGITS_TEL_ETC);
private static final String REGEX_OID_DIGITS_ETC = "\\%[0-9A-Za-z_\\^\\~]+";
private static final Pattern PATTERN_OID_DIGITS_ETC = Pattern.compile( REGEX_OID_DIGITS_ETC);
//private static final String REGEX_NUMBER_UNIT = REGEX_NUMBER + REGEX_UNIT_SUFFIX;
private static final Pattern PATTERN_NUMBER = Pattern.compile( REGEX_NUMBER);
private static final String REGEX_FLOAT_DEC = "[\\+\\-]?[0-9_\u00B7\\'\\.\\,]*[0-9][eE][\\+\\-]?[0-9]+[\\~0-9]*" //,
; //+ REGEX_UNIT_SUFFIX;
private static final Pattern PATTERN_FLOAT_DEC = Pattern.compile( REGEX_FLOAT_DEC);
private static final String REGEX_FLOAT_HEX =
	"[\\+\\-]?0[xX][0-9A-Fa-f_\u00B7\\'\\.\\,]*[0-9A-Fa-f]([pP][\\+\\-]?[0-9]+[\\~0-9]*)?"; //+ REGEX_UNIT_SUFFIX;
private static final Pattern PATTERN_FLOAT_HEX = Pattern.compile( REGEX_FLOAT_HEX);
private static final String REGEX_DATE = "\\-?[0-9]+\\-[0-1][0-9]\\-[0-9][0-9]T?[\\.0-9\\:\\+\\-]*";
private static final Pattern PATTERN_DATE = Pattern.compile( REGEX_DATE);
public static final Pattern PATTERN_DATE_D = Pattern.compile( "[0-9][0-9]\\.[0-1][0-9]\\.[12]?[0-9]?[0-9][0-9]");
public static final Pattern PATTERN_TIME = Pattern.compile( "[0-9]+\\:[0-9][0-9](\\:[0-9][0-9])?");
/** Include '_' for IDs in programming and '^'/'~' for OIDs/base64 */
private static final String REGEX_WORD = "[_\\^\\~\\p{L}\\p{S}\\p{N}\\p{M}]*[\\p{L}\\p{S}][_\\^\\~\\p{L}\\p{S}\\p{N}\\p{M}]*";
//private static final String REGEX_WORD = "[_\\^\\~\\w\\p{L}\\p{S}\\p{N}\\p{M}]*[\\p{L}\\p{S}][_\\^\\~\\w\\p{L}\\p{S}\\p{N}\\p{M}]*";
private static final Pattern PATTERN_WORD = Pattern.compile( REGEX_WORD);
// Literal 'with meaning' or as ID etc., base64 x-chars explicit:
//private static final String REGEX_SEMEME = "[\\w\\p{L}\\p{S}\\p{N}\\_\\^\\~\\p{Po}][\\- \\w\\p{L}\\p{S}\\p{N}_\\^\\~\\p{Po}]*";
//private static final Pattern PATTERN_SEMEME = Pattern.compile( REGEX_SEMEME );
private static final String CHARS_NUM_FIRST = "+-0123456789#";
private static final String CHARS_STRUC = "()[]{}<>"; // (cmp. Unicode 'Ps'/'Pe' open/close)
//private static final String CHARS_STRUC_LINE = "./=;|-*:";
private static final String CHARS_QUOTE_LEFT = "'\"\u00ab\u2018\u201b\u201c\u201f\u2039"; // (cmp. Unicode 'Pi' initial)
//private static final String CHARS_QUOTE_RIGHT = "'\"\u00bb\u2019\u201d\u203a"; // (cmp. Unicode 'Pf' final)
private static final String CHARS_STRUC_MOD = "?%!&@"; // '$' moved to currency
private static final String CHARS_FORMAT = "*_+^~"; // markdown, '#' moved to numeric
private static final String CHARS_MARKER = CHARS_FORMAT + CHARS_STRUC + CHARS_STRUC_MOD;
private static final String CHARS_MARKER_PLUS_LEFT = ".:" + CHARS_MARKER;
static final String CHARS_MARKER_DOLLAR = "$#" + CHARS_MARKER;

private static boolean isCurrency( char ch0) {
	return ('$' == ch0) || ((0xa2 <= ch0) && (ch0 <= 0xa5)) || ((0x20a0 <= ch0) && (ch0 < 0x20cf));
}

// Old
public static final char SHASH_QQ = (char) 1;
public static final char SHASH_QX = (char) 2;
public static final char SHASH_QM = (char) 3;

public static final char SHASH_OFFS = (char) 0xE100;
public static final String SHASH_NAN = "" + (char) 0xE100; // 0xEC00;
public static final char SHASH_ERR_MAX = (char) 0xE17F;
public static final char SHASH_FORMAT_LEFT = (char) 0xE180; // ?
public static final char SHASH_FORMAT_RIGHT = (char) 0xE1A0; // 0xE180; // ?
public static final char SHASH_TEMP_NUM_STRING = (char) 0xE1ff; // TODO
public static final char SHASH_FLITERAL = (char) 0xE200;
public static final char SHASH_SLITERAL = (char) 0xE204;
public static final char SHASH_TEMP_BITLIST = (char) 0xE23f; // TODO
public static final char SHASH_NUM = (char) 0xE300; // with units/ date/ without units
public static final char SHASH_DATE = (char) 0xE380; // leaving 6 bits
public static final long SHASH_DATE_0 = (0x1L << (48 + 3 + 6 - 1));
public static final char SHASH_NEG = (char) 0xE400;
public static final char SHASH_POS = (char) 0xEC00;
//public static final char SHASH_NUM_UNIT = (char) 0xF480; // old
public static final char SHASH_LITERAL = (char) 0xF400;

public static final long FLAG_VALUE_CAPS__4 = 4;
public static final long FLAG_VALUE_CAPS_ALL__6 = 6;
public static final long FLAG_VALUE_CAPS_FIRST__4 = 4;
/** Make shashBits directly usable for QMap ... */
public static final int FLAG_INDEXED__1 = 1;

private static char[] zColl64Char4Key = null;
private static char[] zColl64CharUpper4Key;
private static byte[] zColl64Key4Char;

private static void getCollArrays() {
	if (null == zColl64Char4Key) {
		Object[] aa = StringFunc.getCollArrays();
		zColl64Char4Key = (char[]) aa[0];
		zColl64CharUpper4Key = (char[]) aa[1];
		zColl64Key4Char = (byte[]) aa[2];
	}
}

/** For simple strings. Last bit is ignored, bit 1/2 is used as case marker.
 * @param bits
 * @return
 */
public static String string4ShashBitsLiteral( long bits) {
	getCollArrays();
	if (0 == bits) {
		return "";
	}
	long out = bits;
	int len = 10;
	out >>>= FLAG_INDEXED__1;
	long markerCase8End = FLAG_VALUE_CAPS_ALL__6 >>> FLAG_INDEXED__1;
	for (; len > 0; --len) {
		if (markerCase8End < (out & 0x3fL)) {
			break;
		}
		markerCase8End = 0;
		out >>>= 6;
	}
	char[] aOut = new char[len];
	for (--len; len >= 0; --len) {
		aOut[len] = zColl64Char4Key[(int) (out & 0x3f)];
		out >>>= 6;
	}
	if ((0 == (bits & FLAG_VALUE_CAPS__4)) || (10 <= aOut.length)
		|| (0 >= aOut.length)) { // || QMap.isVoid( handle )) {
		return new String( aOut);
	}
	// First or all char's uppercase:
	final int cUpper = (FLAG_VALUE_CAPS_ALL__6 == (bits & FLAG_VALUE_CAPS_ALL__6))
		? aOut.length
		: 1;
	for (int i0 = 0; i0 < cUpper; ++i0) {
		aOut[i0] = zColl64CharUpper4Key[zColl64Key4Char[aOut[i0]]];
	}
	return new String( aOut);
}

public static double doubleD4oShashBits( long shashBits) {
	long bits = 0xe000 | (shashBits >>> (48 + 3));
	if (QStrFunc.SHASH_NEG > bits) {
		return Double.NaN;
	}
	boolean pos = (QStrFunc.SHASH_POS <= bits);
	bits -= pos ? QStrFunc.SHASH_POS : QStrFunc.SHASH_NEG;
	// Exponent
	bits <<= 52;
	// Significand
	bits |= (shashBits << 1) & 0x000fffffffffffffL;
	return MiscFunc.roundForIntUsage( Double.longBitsToDouble( pos ? bits : -bits));
}

public static double doubleD4oShashNum( String shash) {
	if (0 >= shash.length()) {
		return 0;
	}
	if (SHASH_NEG > shash.charAt( 0)) {
		return Double.NaN;
	}
	//TODO
	// else if SHASH_NUM_UNIT
	// Simplified:
	long bits = shash.charAt( 0);
	boolean pos = (SHASH_POS <= shash.charAt( 0));
	bits -= pos ? SHASH_POS : SHASH_NEG;
	bits <<= 52;
	switch ((shash.length() > 5) ? 5 : shash.length()) {
	case 5:
		bits |= (shash.charAt( 4) & 0x3fffL) >>> 4; // Fall through.
	case 4:
		bits |= (shash.charAt( 3) & 0x3fffL) << 10;
	case 3:
		bits |= (shash.charAt( 2) & 0x3fffL) << 24;
	case 2:
		bits |= (shash.charAt( 1) & 0x3fffL) << 38;
	case 1:
		break;
	default:
		return Double.NaN;
	}
	return MiscFunc.roundForIntUsage( Double.longBitsToDouble( pos ? bits : -bits));
}

public static long shashBits4shash( String shash, String optString) {
	long bits = shash.charAt( 0);
	bits <<= (48 + 3);
	switch ((shash.length() > 5) ? 5 : shash.length()) {
	case 5:
		//			bits |= (shash.charAt( 4 ) & 0x7f00L) >>> 6;
		//			bits |= (shash.charAt( 4 ) & 0x007fL) >>> 5; // Fall through.
		bits |= (shash.charAt( 4) & 0x3fffL) >>> 5; // Fall through.
	case 4:
		bits |= (shash.charAt( 3) & 0x3fffL) << 9;
	case 3:
		bits |= (shash.charAt( 2) & 0x3fffL) << 23;
	case 2:
		bits |= (shash.charAt( 1) & 0x3fffL) << 37;
	case 1:
		break;
	default:
		return 0;
	}
	return bits;
}

/** ? Caller: ? remove case markers */
public static String shash4ShashBits( long bits) {
	if (0 == bits) {
		return "";
	}
	long out = bits & ~FLAG_INDEXED__1;
	int len = 4 + 1;
	long cmp = out;
	for (; len > 0; --len) {
		if (0 /*markerCase8End*/ < (cmp & 0x3fff)) {
			break;
		}
		cmp >>>= 14; //6;
	}
	char[] aOut = new char[len]; //(3 + 4 + len * 6 + 13) / 14 ];
	int val = (int) ((out >>> (48 + 3)) | 0xe000);
	int shift = 48 + 3;
	len = 0;
	for (int i0 = 0; i0 < aOut.length; ++i0) {
		aOut[i0] = (char) val;
		len = (0x4000 != val) ? i0 : len;
		shift -= 14;
		val = 0x4000 | (int) (0x3fff & ((0 <= shift) ? (out >>> shift) : (out << (-shift))));
	}
	++len;
	return new String( aOut, 0, len);
}

/** Calculate sortable hash (shash) for value.
 * @param repr String representation, to be parsed (or null).
 * @param valueD4 Value or additional (fractional) value.
 * @return shash.
 */
public static long shashBits4DoubleD4( String repr, double valueD4) {
	// German decimal point etc.:
	if ((null != repr) && (0 <= repr.indexOf( ','))) {
		if (repr.indexOf( '.') > repr.lastIndexOf( ',')) {
			repr = repr.replace( ",", "");
		} else {
			repr = repr.replace( ".", "").replace( ",", ".");
		}
	}
	long out = (long) SHASH_NAN.charAt( 0) << (48 + 3);
	try {
		double val = (null == repr) ? valueD4 : (StringFunc.doubleD4oString( repr, Double.NaN) + valueD4);
		if (Double.isNaN( val)) {
			return out;
		}
		if (!Double.isInfinite( val)) {
			val = Math.nextAfter( val, (0 <= val) ? Double.POSITIVE_INFINITY : Double.NEGATIVE_INFINITY);
		}
		long bits = Double.doubleToRawLongBits( val);
		if (0 > val) {
			bits = -bits;
		}
		final int exp = (int) ((bits & 0x7ff0000000000000L) >>> 52);
		final long significand = (bits & 0x000fffffffffffffL);
		return ((long) (((0 > val) ? SHASH_NEG : SHASH_POS) + exp)) << (48 + 3)
			| (significand >>> 1);
	} catch (Exception e) {
	}
	return out;
}

/** Calculate sortable hash (shash) string for value.
 * @param repr String representation, to be parsed (or null).
 * @param value Value or additional (fractional) value.
 * @return shash.
 */
//protected static String OLD_shash4Double( String repr, double value) {
//	String shash = null;
//	try {
//		double val = (null == repr) ? value : (StringFunc.doubleD4oString( repr, 0.0) + value);
//		long bits = Double.doubleToRawLongBits( val);
//		if (0 > val) {
//			bits = -bits;
//		}
//		int exp = (int) ((bits & 0x7ff0000000000000L) >>> 52);
//		long significand = ((bits & 0x000fffffffffffffL) << (4 * 14 - 52));
//		///// Stay away from surrogate area.
//		char[] bits14 = new char[5];
//		bits14[0] = (char) (((0 > val) ? SHASH_NEG : SHASH_POS) + exp);
//		bits14[4] = (char) (0x8000 | (significand & 0x3fff));
//		significand >>>= 14;
//		bits14[3] = (char) (0x8000 | (significand & 0x3fff));
//		significand >>>= 14;
//		bits14[2] = (char) (0x8000 | (significand & 0x3fff));
//		significand >>>= 14;
//		bits14[1] = (char) (0x8000 | (significand & 0x3fff));
//		shash = new String( bits14);
//		int count = 4;
//		for (; count >= 2; --count) {
//			if (bits14[count] != 0x8000) {
//				break;
//			}
//		}
//		shash = shash.substring( 0, count + 1);
//	} catch (Exception e) {
//	}
//	return shash;
//}

/** Calculate sortable hash (shash) string for value.
 * @param repr String representation, to be parsed (or null).
 * @param valueD4 Value or additional (fractional) value.
 * @return shash.
 */
public static String shash4DoubleD4( String repr, double valueD4) {
	long out = shashBits4DoubleD4( repr, valueD4);
	return shash4ShashBits( out);
}

/** TODO: Caller may have to clear the last bit. */
public static long shashBits4slotSecond16( long slotSecond16) {
	long bitsx16 = (slotSecond16 + SHASH_DATE_0) & ((SHASH_DATE_0 << 1) - 1);
	return ((long) SHASH_DATE << (48 + 3)) | bitsx16; // & ((1L << (48 + 6)) - 1)); // & ~1;
}

public static String shash4slotSecond16( long slotSecond16) {
	return shash4ShashBits( shashBits4slotSecond16( slotSecond16));
}

public static String shash4Date( String date) {
	return shash4slotSecond16( MiscFunc.slotSecond16oDateApprox( date));
}

public static long slotSecond16oShashDate( String shash) {
	long bitsx16 = shashBits4shash( shash, null);
	return (bitsx16 & ((SHASH_DATE_0 << 1) - 1)) - SHASH_DATE_0;
}

public static String date4ShashDate( String shash) {
	return MiscFunc.date4SlotSecond16Approx( slotSecond16oShashDate( shash));
}

public static long shashBits4PunctFS( String piece) {
	getCollArrays();
	final int len = (7 < piece.length()) ? 7 : piece.length();
	long out = (long) SHASH_FLITERAL << (48 + 3);
	for (int i0 = 0; i0 < len; ++i0) {
		final char ch0 = piece.charAt( i0);
		if (0 < StringFunc.XSTRUCS.indexOf( ch0)) {
			out = (long) SHASH_SLITERAL << (48 + 3);
		}
	}
	if (0 >= len) {
		return out;
	}
	final char ch0 = piece.charAt( 0);
	final byte key = (zColl64Key4Char.length > ch0) ? zColl64Key4Char[ch0] : (byte) 4;
	out |= (long) ((key - 1) & 0x3) << 48 + 3;
	for (int i0 = 0, shift = 48 + 3 - 7; i0 < len; ++i0, shift -= 7) {
		if (0 <= shift) {
			out |= (piece.charAt( i0) & 0x7fL) << shift;
			continue;
		}
		out |= (piece.charAt( i0) & 0x7f) >>> (-shift);
	}
	return out;
}

public static String shash4PunctFS( String piece) {
	return shash4ShashBits( shashBits4PunctFS( piece));
}

/** For quick shash calculation: does not work for special char's such as 'sharp S' etc.
 * @param strAnsi Is expected to contain basic ASCII/ ANSI/ IPA char's.
 * @return Shash bits.
 */
public static long shashBits4Ansi( String strAnsi, boolean forceLiteral) {
	getCollArrays();
	if (0 == strAnsi.length()) {
		return 0;
	} else if (!forceLiteral && (1 == strAnsi.length() && (0x2b0 > strAnsi.charAt( 0)))) {
		final char ch0 = strAnsi.charAt( 0);
		if (('0' <= ch0) && (ch0 <= '9')) {
			return shashBits4DoubleD4( null, (ch0 - '0') * 1.0e4);
		}
		if ((zColl64Key4Char.length > ch0) && (zColl64Key4Char['0'] <= zColl64Key4Char[ch0])
			&& (zColl64Key4Char[ch0] < 63)) {
			long flag = 0;
			if (zColl64Char4Key[zColl64Key4Char[ch0]] != ch0) {
				if (zColl64CharUpper4Key[zColl64Key4Char[ch0]] == ch0) {
					flag = FLAG_VALUE_CAPS_FIRST__4;
				}
			}
			return (long) SHASH_LITERAL << (48 + 3) | ((long) zColl64Key4Char[ch0]) << (64 - 3 - 6)
				| flag;
		}
	}
	long out = (long) SHASH_LITERAL << (48 + 3);
	int shift = (64 - 3 - 6);
	int len = (10 > strAnsi.length()) ? strAnsi.length() : 10;
	long flag = 0;
	boolean isWord = false;
	boolean isStruc = false;
	for (int i0 = 0; i0 < len; ++i0, shift -= 6) {
		final char ch0 = strAnsi.charAt( i0);
		if (isStruc || (0 < StringFunc.XSTRUCS.indexOf( ch0))) {
			isStruc = true;
		} else if (!isWord && (0x2b0 > ch0)) {
			if ((('a' <= ch0) && (ch0 <= 'z')) || (('A' <= ch0) && (ch0 <= 'Z'))) {
				isWord = true;
			} else if (('0' <= ch0) && (ch0 <= '9')) {
				isWord = true;
			} else if (0xc0 <= ch0) {
				isWord = true;
			}
		}
		if (zColl64Key4Char.length > ch0) {
			if (zColl64Char4Key[zColl64Key4Char[ch0]] != ch0) {
				if (zColl64CharUpper4Key[zColl64Key4Char[ch0]] == ch0) {
					if ((0 != flag) || (0 == i0)) {
						flag = (flag != 0) ? FLAG_VALUE_CAPS_ALL__6 : FLAG_VALUE_CAPS_FIRST__4;
					}
				}
			}
			out |= (long) zColl64Key4Char[ch0] << shift;
		} else {
			out |= 63L << shift;
		}
	}
	if (!isStruc && !isWord) {
		isWord = PATTERN_WORD.matcher( strAnsi).find();
	}
	if (isStruc || !isWord) {
		return shashBits4PunctFS( strAnsi);
	}
	return out | flag;
}

/** For quick shash calculation: does not work for special char's such as 'sharp S' etc.
 * @param strAnsi Is expected to contain basic ASCII/ ANSI/ IPA char's.
 * @return Shash value.
 */
public static String shash4Ansi( String strAnsi, boolean forceString) {
	long out = shashBits4Ansi( strAnsi, forceString);
	return shash4ShashBits( out);
}

private static boolean shashCheckAnsi( String piece) {
	getCollArrays();
	int len = piece.length();
	len = (10 < len) ? 10 : len;
	int i0 = 0;
	for (; i0 < len; ++i0) {
		final char ch0 = piece.charAt( i0);
		if ((zColl64Key4Char.length > ch0) && (0 < zColl64Key4Char[ch0]) && (zColl64Key4Char[ch0] < 63)) {
			continue;
		}
		break;
	}
	return (len <= i0);
}

private static String shashSpecial( boolean classify, String piece) {
	if ((null == piece) || (0 >= piece.length())) {
		return piece;
	}
	boolean num0 = //!forceString && 
		(0 <= CHARS_NUM_FIRST.indexOf( piece.charAt( 0)));
//	double frac = 0;
	String shash = "";
	boolean punct = false;
	// Units:
	String units = null;
	if (num0) {
		int iUnit = piece.lastIndexOf( '_');
		if ((0 < iUnit) && (iUnit < (piece.length() - 1)) && (' ' < piece.charAt( iUnit + 1))) {
			if (PATTERN_UNIT.matcher( piece.substring( iUnit)).matches()
				&& !PATTERN_NUMBER.matcher( piece.substring( iUnit)).matches()) {
				units = piece.substring( iUnit + 1);
				piece = piece.substring( 0, iUnit);
			}
		}
	}

	if (num0 && PATTERN_FLOAT_HEX.matcher( piece).matches()) {
		if ((0 > piece.indexOf( 'p')) && (0 > piece.indexOf( 'P'))) {
			piece += "p0";
		}
	} else if (num0 && (PATTERN_DATE.matcher( piece).matches()) || PATTERN_DATE_D.matcher( piece).matches()) {
		if (classify) {
			return "" + SHASH_DATE;
		}
		shash = shash4Date( piece);
	} else if (num0 && PATTERN_DIGITS_TEL_ETC.matcher( piece).matches()) {
		num0 = false;
	} else if (num0 && PATTERN_OID_DIGITS_ETC.matcher( piece).matches()) {
		num0 = false;
	} else if (num0 && PATTERN_FLOAT_DEC.matcher( piece).matches()) {
	} else if (num0 && PATTERN_NUMBER.matcher( piece).matches()) {
	} else if (PATTERN_WORD.matcher( piece).find()) { // floatError ||
		num0 = false;
	} else {
		num0 = false;
		punct = true;
	}
	if (num0) {
		if (0 >= shash.length()) {
			// German decimal point etc.:
			if (0 <= piece.indexOf( ',')) {
				if (piece.indexOf( '.') > piece.lastIndexOf( ',')) {
					piece = piece.replace( ",", "");
				} else {
					piece = piece.replace( ".", "").replace( ",", ".");
				}
			}
			piece = PATTERN_NUMBER_SEP.matcher( piece).replaceAll( "");
			//TODO: Extra long floats.
			String shashTmp = shash4DoubleD4( piece, 0); //frac*1e4);
			shashTmp = (SHASH_NAN.equals( shashTmp)) ? null : shashTmp;
			shash = (null == shashTmp) ? shash : shashTmp;
			if (null == shashTmp) {
				if (classify) {
					return "" + SHASH_LITERAL;
				}
				//			floatError = true;
				num0 = false;
				units = null;
			}
			//TODO
			// if (null != units) ...
			if (classify) {
				return "" + SHASH_TEMP_NUM_STRING;
			}
		}
	}
	if (!num0) {
		if (punct) {
			if (classify) {
				return "" + SHASH_FLITERAL;
			}
			shash = shash4PunctFS( piece);
		} else {
			if (null != units) {
				piece += "_" + units;
				//TODO
			}
			return null;
		}
	}
	return classify ? ("" + shash.charAt( 0)) : shash;
}

public static String shash4Literal( String piece, long... b0ForcexSha1) {
	//	final boolean forceX = (0 >= b0ForcexSha1.length) ? false : (0 != (1 & b0ForcexSha1[ 0 ]));
	final boolean bSha1 = (0 >= b0ForcexSha1.length) ? false : (0 != (2 & b0ForcexSha1[0]));
	String shash;
	if ((null == piece) || (0 >= piece.length())) {
		return "";
	} else if (" ".equals( piece)) {
		return "" + (SHASH_FLITERAL) + (char) (0x4000 | (' ' << 7));
	}
	boolean cut = (Dib2Constants.SHASH_MAX * 2) < piece.length();
	String px = cut ? piece.substring( 0, Dib2Constants.SHASH_MAX * 2) : piece;
	byte[] shashBytes = StringFunc.coll64xBytes( px); //coll.getCollationKey( px ).toByteArray();
	int len = shashBytes.length;
	//	///// Compress:
	if (0 >= len) {// || //(!forceString && 
		//		(zColl64Key4Char[ '0' ] > shashBytes[ 0 ])) {
		//		if (!PATTERN_WORD.matcher( piece ).find()) {
		return shash4PunctFS( px);
		//		}
	}
	boolean isWord = false;
	boolean isStruc = false;
	for (int i0 = 0; i0 < piece.length(); ++i0) {
		final char ch0 = piece.charAt( i0);
		if (isStruc || (0 < StringFunc.XSTRUCS.indexOf( ch0))) {
			isStruc = true;
		} else if (!isWord && (0x2b0 > ch0)) {
			if ((('a' <= ch0) && (ch0 <= 'z')) || (('A' <= ch0) && (ch0 <= 'Z'))) {
				isWord = true;
			} else if (('0' <= ch0) && (ch0 <= '9')) {
				isWord = true;
			} else if (0xc0 <= ch0) {
				isWord = true;
			}
		}
	}
	if (!isWord) {
		isWord = PATTERN_WORD.matcher( piece).find();
	}

	char[] shashChars = new char[2 + len / 2];
	shashChars[0] = isWord ? SHASH_LITERAL
		: (isStruc ? SHASH_SLITERAL : SHASH_FLITERAL); // | ((shashBytes[ 0 ]&0xff) << 4)); //1 ]);
	int shift = 4;
	int len2 = 1;
	{
		int i0 = 0;
		for (; i0 < shashBytes.length; ++i0, shift -= 6) {
			if (0 > shift) {
				shashChars[len2 - 1] |= (shashBytes[i0] & 0x3f) >>> (-shift);
				shift += 14;
				shashChars[len2++] = (char) (0x4000 | (0x3fff & (shashBytes[i0] << shift)));
			} else {
				shashChars[len2 - 1] |= 0x3fff & (shashBytes[i0] << shift);
			}
		}
	}
	shash = new String( Arrays.copyOf( shashChars, len2));
	if (cut || (Dib2Constants.SHASH_MAX <= len2)) {
		if (bSha1) {
			shashChars = Arrays.copyOf( shashChars, Dib2Constants.SHASH_MAX);
			byte[] sha1;
			try {
				sha1 = MessageDigest.getInstance( "SHA-1").digest( piece.getBytes( "UTF-8"));
			} catch (Exception e) {
				sha1 = new byte[20];
			}
			shashChars[Dib2Constants.SHASH_MAX - 11] = 0xffff;
			for (int i0 = 18; i0 >= 0; i0 -= 2) {
				shashChars[Dib2Constants.SHASH_MAX - 10 + (i0 / 2)] //-
					= (char) (((sha1[i0] & 0xff) << 8) + (sha1[i0 + 1] & 0xff));
			}
		} else {
			shashChars = Arrays.copyOf( shashChars, Dib2Constants.SHASH_MAX - 1);
		}
		shash = new String( shashChars);
	}
	return shash;
}

public static long shashBits4Literal( String piece, boolean forceLiteral) {
	//TODO speed up
	if (forceLiteral && shashCheckAnsi( piece)) {
		return shashBits4Ansi( piece, forceLiteral);
	}
	final String shash = shash4Literal( piece, forceLiteral ? 1 : 0);
	return shashBits4shash( shash, piece);
}

public static String string4ShashBits( long shash) {
	if (0 == shash) {
		return "";
	}
	final char ch0 = (char) (0xe000 | (int) (shash >>> 48 + 3));
	if (SHASH_LITERAL > ch0) {
		if (SHASH_NEG > ch0) {
			//			if (1 >= len) {
			//				return "";
			//			}
			if (SHASH_NUM > ch0) {
				if (SHASH_ERR_MAX > ch0) {
					return (SHASH_OFFS == ch0) ? "NaN" : "ERROR";
				}
				// For STRUC, PUNCT, ...:
				char[] out = new char[7];
				int len = 0;
				for (int shift = 48 + 3 - 7; shift >= 0; shift -= 7, ++len) {
					out[len] = (char) ((shash >>> shift) & 0x7f);
					if (' ' > out[len]) {
						break;
					}
				}
				return new String( out, 0, len);
			}
			//TODO Handle units
			return MiscFunc.date4SlotSecond16Approx( (shash & ((SHASH_DATE_0 << 1) - 1)) - SHASH_DATE_0);
		}
		double val = doubleD4oShashBits( shash);
		return StringFunc.string4DoubleD4( val);
	}
	return string4ShashBitsLiteral( shash);
}

public static String string4Shash( String shash) {
	getCollArrays();
	int len = shash.length();
	if (0 >= len) {
		return "";
	} else if (SHASH_LITERAL > shash.charAt( 0)) {
		final char ch0 = shash.charAt( 0);
		if (SHASH_NEG > ch0) {
			if (1 >= len) {
				return "";
			}
			if (SHASH_NUM > ch0) {
				if (SHASH_ERR_MAX > ch0) {
					return (SHASH_OFFS == ch0) ? "NaN" : "ERROR";
				}
				// For STRUC, PUNCT, ...:
				char[] out = new char[2 * (len - 1) - (0 == (shash.charAt( len - 1) & 0x7f) ? 1 : 0)];
				out[out.length - 1] = (char) (0x7f & (shash.charAt( shash.length() - 1) >>> 7));
				for (int i0 = 1; i0 < out.length; i0 += 2) {
					out[i0 - 1] = (char) (0x7f & (shash.charAt( 1 + i0 / 2) >>> 7));
					out[i0] = (char) (shash.charAt( 1 + i0 / 2) & 0x7f);
				}
				return new String( out);
			}
			// TODO Handle units
			return date4ShashDate( shash);
		}
		double val = doubleD4oShashNum( shash);
		return StringFunc.string4DoubleD4( val);
	}

	char[] out = new char[1 + shash.length() * 3];
	int len2 = 0;
	int shift = 4;
	for (int i0 = 0; i0 < len;) {
		out[len2++] |= (char) (((shash.charAt( i0) & 0x3fff) >>> shift) & 0x3f);
		shift -= 6;
		if (0 > shift) {
			out[len2] = (char) ((shash.charAt( i0) << (-shift)) & 0x3f);
			shift += 14;
			++i0;
		}
	}
	for (; len2 > 0; --len2) {
		if (0 != out[len2 - 1]) {
			break;
		}
	}
	for (int i0 = 0; i0 < len2; ++i0) {
		out[i0] = zColl64Char4Key[out[i0]];
	}
	return new String( out, 0, len2);
}

/** Calculate sortable hash (shash) values.
 * @param xSha1 for making it unique by appending SHA-1 value in case of long string
 * @param xaElements text or float or marker etc.
 * @return per element: up to (SHASH_MAX-2) chars or truncated (SHASH_MAX-1) chars or (SHASH_MAX-11) chars + X + SHA1
 */
public static String[] shash( boolean xSha1, String... xaElements) {
	String[] out = new String[xaElements.length];
	int inx = 0;
	for (String piece : xaElements) {
		final String tmp = shashSpecial( false, piece);
		out[inx++] = (null != tmp) ? tmp : shash4Literal( piece, (xSha1 ? 3 : 1));
	}
	return out;
}

public static String shash( String... xStr) {
	if (1 == xStr.length) {
		return shash( false, xStr)[0];
	}
	String[] out = shash( false, xStr);
	StringBuilder shash = new StringBuilder( Dib2Constants.SHASH_MAX * 2);
	String sep = "";
	for (String piece : out) {
		shash.append( sep);
		sep = "" + (char) 1;
		shash.append( piece);
		if (Dib2Constants.SHASH_MAX <= shash.length()) {
			break;
		}
	}
	return (Dib2Constants.SHASH_MAX <= shash.length()) ? shash.substring( 0, Dib2Constants.SHASH_MAX - 1)
		: shash.toString();
}

private static int outWithStructMarker( String[] out, int cOut, String struct) {
	if (2 == struct.length()
		&& (' ' > struct.charAt( 1))
		&& (0 <= ("" + StringFunc.QUOTE_START + StringFunc.QUOTE_END
			+ StringFunc.LIST_START + StringFunc.LIST_END).indexOf(
				struct.charAt( 1)))) {
		out[cOut++] = SHASH_SLITERAL + struct.substring( 1);
		return cOut;
	}
	boolean struc0 = 0 <= (CHARS_STRUC + CHARS_STRUC_MOD).indexOf( struct.charAt( 1));
	for (int i0 = 2; i0 < struct.length(); ++i0) {
		boolean struc = 0 <= (CHARS_STRUC + CHARS_STRUC_MOD).indexOf( struct.charAt( i0));
		if (struc != struc0) {
			if (struc) {
				final String tmp = struct.substring( 1, i0);
				out[cOut++] = //(PATTERN_WORD.matcher( tmp ).matches() ? SHASH_LITERAL : SHASH_PUNCT) + tmp;
					SHASH_FLITERAL + tmp;
				out[cOut++] = SHASH_SLITERAL + struct.substring( i0);
				return cOut;
			}
			out[cOut++] = SHASH_SLITERAL + struct.substring( 1, i0);
			final String tmp = struct.substring( i0);
			out[cOut++] = "" + //(PATTERN_WORD.matcher( tmp ).matches() ? SHASH_LITERAL : SHASH_PUNCT) + tmp;
				SHASH_FLITERAL + tmp;
			return cOut;
		}
	}
	if (struc0) {
		out[cOut++] = SHASH_SLITERAL + struct.substring( 1);
		return cOut;
	}
	out[cOut++] = struct;
	return cOut;
}

/** Split text into 'atoms' with simplified markers for numbers, bit lists, punctuation,
 * assuming '[[' and ']]' ...
 * LIST_START, QUOTE_START, etc. as markers for lists.
 * Markers: SHASH_STRUC, SHASH_PUNCT, SHASH_DATE, SHASH_TEMP_BITLIST, SHASH_TEMP_NUM_STRING, (SHASH_LITERAL)
 * @param xText
 * @return
 */
private static String[] splitTextAppend( String[] out, int cOut, String xText) {
	String[] in = xText.split( " ", -1);
	boolean first = true;
	for (String sliceFull : in) {
		if (!first && (0 < cOut)) {
			if ((null != out[cOut - 1]) && (SHASH_FLITERAL == out[cOut - 1].charAt( 0))) {
				out[cOut - 1] += ' ';
			} else {
				out[cOut++] = "" + SHASH_FLITERAL + ' ';
			}
		}
		first = false;
		String slice = sliceFull;
		while (0 < slice.length()) {
			if ((cOut + 4) >= out.length) {
				out = Arrays.copyOf( out, 2 * out.length);
			}
			int cut = 0;
			char ch0 = slice.charAt( 0);
			for (; cut < slice.length(); ++cut) {
				if (false //.
					|| (('a' <= ch0) && (ch0 <= 'z')) //.
					|| (('0' <= ch0) && (ch0 <= '9')) //.
					|| (('A' <= ch0) && (ch0 <= 'Z')) //.
					|| (' ' > ch0) //.
					|| (true //.
						&& !isCurrency( ch0) //.
						&& (0 > (CHARS_MARKER_PLUS_LEFT + CHARS_QUOTE_LEFT + '.')
							.indexOf( slice.charAt( cut))))) {
					break;
				}
			}
			if (cut >= slice.length()) {
				cOut = outWithStructMarker( out, cOut, SHASH_FLITERAL + slice);
				slice = "";
				continue;
			}
			String piece = slice;
			slice = "";
			String part = piece;
			String pre = "";
			boolean num = (0 < CHARS_NUM_FIRST.indexOf( ch0));
			if (0 < cut) {
				if ((1 == cut) && (1 < piece.length())
					&& (('+' == piece.charAt( 0)) || ('-' == piece.charAt( 0)))
					&& ('0' <= piece.charAt( 1)) && ('9' >= piece.charAt( 1))) {
					cut = 0;
					num = true;
				} else {
					// Potential further splitting later on:
					pre = SHASH_FLITERAL + piece.substring( 0, cut);
					part = piece.substring( cut);
					ch0 = part.charAt( 0);
				}
			}
			cut = -1;
			boolean date = false;
			if (num) {
				num = false;
				Matcher mx;
				if ((mx = PATTERN_FLOAT_HEX.matcher( part)).find() && (0 == mx.start())) {
					cut = mx.end();
					num = true;
				} else if ((mx = PATTERN_DATE.matcher( part)).find() && (0 == mx.start())) {
					cut = mx.end();
					date = true;
				} else if ((mx = PATTERN_FLOAT_DEC.matcher( part)).find() && (0 == mx.start())) {
					cut = mx.end();
					num = true;
				} else if ((mx = PATTERN_NUMBER.matcher( part)).find() && (0 == mx.start())) {
					cut = mx.end();
					int cut2 = 0;
					if ((mx = PATTERN_TIME.matcher( part)).find() && (0 == mx.start())) {
						cut2 = mx.end();
					} else if ((mx = PATTERN_DIGITS_TEL_ETC.matcher( part)).find() && (0 == mx.start())) {
						cut2 = mx.end();
					} else if ((mx = PATTERN_OID_DIGITS_ETC.matcher( part)).find() && (0 == mx.start())) {
						cut2 = mx.end();
					}
					num = cut2 < cut;
					cut = num ? cut : cut2;
				} else if ((mx = PATTERN_DIGITS_TEL_ETC.matcher( part)).find() && (0 == mx.start())) {
					cut = mx.end();
				} else if ((mx = PATTERN_OID_DIGITS_ETC.matcher( part)).find() && (0 == mx.start())) {
					cut = mx.end();
				}
				if (num && (mx = PATTERN_UNIT.matcher( part.substring( cut))).find() && (0 == mx.start())) {
					cut += mx.end();
				}
			} else if ((1 < part.length() && ('\'' == part.charAt( 1)))) {
				switch (ch0) { // TODO
				case 'P': // packed base64x
				case 'I': // oid/ indirect
				case 'X': // octets
					cut = part.lastIndexOf( '\'');
					if (1 >= cut) {
						cut = -1;
					} else {
						part = SHASH_TEMP_BITLIST + part;
						cut += 2;
					}
				default:
					;
				}
			}
			if (0 > cut) {
				Matcher mx = null;
				cut = (mx = PATTERN_WORD.matcher( part)).find() ? mx.start() : part.length();
				int cutx = (mx = PATTERN_NUMERICS.matcher( part)).find() ? mx.start() : cut;
				if (cut <= cutx) {
					cutx = (mx = PATTERN_DIGITS.matcher( part)).find() ? mx.start() : cut;
					if (cut <= cutx) {
						cutx = (mx = PATTERN_WORD.matcher( part)).find() ? mx.start() : -1;
					}
				}
				cut = cutx;
				if (0 > cut) {
					pre = (0 < pre.length()) ? pre : ("" + SHASH_FLITERAL);
					pre += part;
					cOut = outWithStructMarker( out, cOut, pre);
					continue;
				}
				if (0 < cut) {
					pre = (0 < pre.length()) ? pre : ("" + SHASH_FLITERAL);
					pre += part.substring( 0, cut);
					part = part.substring( cut);
				}
				cut = mx.end() - cut;
				if ((SHASH_OFFS & 0xff00) <= ch0) {
					part = SHASH_LITERAL + part;
					++cut;
				}
			} else if (date) {
				num = false;
				part = SHASH_DATE + part;
				++cut;
			} else if (num && (0 < pre.length()) && (pre.endsWith( "+") || pre.endsWith( "-"))) {
				part = pre.charAt( pre.length() - 1) + part;
				++cut;
				pre = pre.substring( 0, pre.length() - 1);
			}
			if (num) {
				if ((0 < pre.length()) && isCurrency( pre.charAt( pre.length() - 1))) {
					part = pre.charAt( pre.length() - 1) + part;
					++cut;
					pre = pre.substring( 0, pre.length() - 1);
				}
				part = SHASH_TEMP_NUM_STRING + part;
				++cut;
			}
			if (1 < pre.length()) {
				cOut = outWithStructMarker( out, cOut, pre);
				if (0 > cut) {
					cut = 0;
				}
			}
			if ((cut >= part.length()) || (0 >= cut)) {
				out[cOut++] = part;
				continue;
			}
			slice = part.substring( cut);
			if (0xf800 <= part.charAt( 0)) {
				out[cOut++] = SHASH_LITERAL + part.substring( 0, cut);
			} else {
				out[cOut++] = part.substring( 0, cut);
			}
		}
	}
	if (cOut < out.length) {
		out[out.length - 1] = "\ue000" + (char) cOut;
	}
	for (int i0 = 0; i0 < cOut; ++i0) {
		char ch0;
		if ((0 >= out[i0].length()
			|| ((ch0 = out[i0].charAt( 0)) >= 0xf800))
			|| (SHASH_SLITERAL == ch0)) {
			continue;
		}
		///// Due to additional cutting, the pieces might end up having a different type:
		final String outx = (SHASH_OFFS <= ch0) ? (out[i0].substring( 1)) : out[i0];
		if (0 >= outx.length()) {
			out[i0] = "";
			continue;
		}
		final String x0 = shashSpecial( true, outx);
		if (ch0 != ((null == x0) ? outx.charAt( 0) : x0.charAt( 0))) {
			out[i0] = (null == x0) ? outx : ("" + x0.charAt( 0) + outx);
		}
	}
	return out;
}

/** Transform text string (with quote markers STX/ETX etc.) into marked text atoms.
 * Markers as first char: SHASH_STRUC, SHASH_PUNCT, SHASH_DATE, SHASH_TEMP_BITLIST, SHASH_TEMP_NUM_STRING,
 * also SHASH_LITERAL if needed:
 * Missing marker = SHASH_LITERAL => simple word.
 */
public static String[] markedAtoms4String( String xStr) {
	String str = xStr;
	String[] out = new String[10 + xStr.length() / 4];
	int cOut = 0;
	int level = -1;
	String combine = "";
	String[] chunks = str.split( "" + StringFunc.QUOTE_START, -1);
	for (String chunk : chunks) {
		if ((cOut + 5) >= out.length) {
			out = Arrays.copyOf( out, 2 * out.length);
		}
		++level;
		if (0 < level) {
			combine += StringFunc.QUOTE_START;
			int i0 = -1;
			while (0 < level) {
				i0 = chunk.indexOf( StringFunc.QUOTE_END, i0 + 1);
				if (0 > i0) {
					combine += chunk;
					chunk = "";
					break;
				}
				--level;
			}
			if (0 < level) {
				continue;
			}
			if (0 <= i0) {
				combine += chunk.substring( 0, i0 + 1);
				chunk = chunk.substring( i0 + 1);
			}
			out[cOut++] = combine;
			combine = "";
		}
		if (0 < chunk.length()) {
			out = splitTextAppend( out, cOut, chunk.replace( StringFunc.QUOTE_END, ' '));
			cOut = (out[out.length - 1].startsWith( "\ue000"))
				? out[out.length - 1].charAt( 1)
				: out.length;
		}
	}
	if (0 < combine.length()) {
		out[cOut++] = "" + combine + StringFunc.QUOTE_END;
	}
	return Arrays.copyOf( out, cOut);
}

/** Transform array of marked atoms into array with list markers:
 balance lists, remove list SEPs,
 remove outer STX/ ETX pairs from ATOMs. 
 * 
 * @param out
 * @return
 */
public static int lists4MarkedAtoms( String[] out) {
	int iIn = 0;
	int iOut = 0;
	int level = 0;
	for (; iIn < out.length; ++iIn) {
		final String in = out[iIn];
		if (0 >= in.length()) {
			continue;
		}
		switch (in.charAt( 0)) {
		case SHASH_SLITERAL:
			if (0 < in.indexOf( StringFunc.LIST_START)) {
				++level;
			} else if ((0 < in.indexOf( StringFunc.LIST_END)) && (0 < level)) {
				--level;
			}
			out[iOut++] = in.substring( 1);
			continue;
		case SHASH_FLITERAL:
			if (0 < level) {
				// Drop it:
				continue;
			}
			out[iOut++] = in.substring( 1);
			continue;
		case SHASH_TEMP_BITLIST:
		case SHASH_TEMP_NUM_STRING:
			out[iOut++] = in.substring( 1);
			continue;
		default:
			break;
		}
		if (iOut != iIn) {
			out[iOut] = in;
		}
		++iOut;
	}
	final int cOut = iOut;
	iOut = 0;
	for (iIn = 0; iIn < cOut; ++iIn, ++iOut) {
		final String outx = out[iIn];
		if ((StringFunc.QUOTE_START == outx.charAt( 0)) && outx.endsWith( "" + StringFunc.QUOTE_END)) {
			out[iOut] = outx.substring( 1, outx.length() - 1);
			if (0 >= out[iOut].length()) {
				--iOut;
			}
		} else if (iOut != iIn) {
			out[iOut] = out[iIn];
		}
	}
	return iOut;
}

//=====
}
