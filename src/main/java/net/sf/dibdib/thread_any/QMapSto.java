// Copyright (C) 2016,2017,2018,2019,2020  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_any;

import java.util.Arrays;
import net.sf.dibdib.config.Dib2Constants;
import net.sf.dibdib.generic.QResult;

/** Use 'shash' (sortable hash, cmp QStr!) values of QVal objects 
 * for creating data handles in a map. The objects may also be associated
 * with other kinds of keys (e.g. object ID (oid)).
 * Note: shash values are not unique, just like hash values.
 * CPUs should process real words instead of character arrays :-)
 * TODO linked deletion
 */
public final class QMapSto {
//=====

/*
Handle bit values (long!, cmp. QStr):

.......... .   .... .... . (may turn negative!)
shash << 3 f1  sub  inx  f0
       f3 f2           fx

f0: 0 = atomic String, 1 = special (indexed)
f3/f2/f1: if special: type (list/qbag/qmap/qass/...)
sub/inx counting backwards: cache
string index = inx << 1 + f0
shash index = inx << 1
sub: subtable (>0: with hash+x as index, 0: spill list)
are stored in the same way and need to be handled differently outside of this class:
111 = outside reference (to atom/term in 'main')
000 = sequence
001 = list (as basic aggregation of sequence items, e.g. for literals),
010 = q-bunch
011 = q-bag: list of bag/set elements (e.g. for counts or predication/denotation: (ODD 1), (ODD 3), ...)
100 = q-map: list of pairs (2-ary sequence/tuple, e.g. proposition/connotation: (SUCC 3 4) ...),
110 = q-ass: list of triples (3-ary seq., e.g. association/mediation: (SUM 2 3 5) ...).
Note: Bunch = (unpackaged) contents of a set, elementary bunch (= element!) has size 1.
Sequence = (unpackaged) contents of a list, elementary sequence (= item) has size 1.
Normalized list can be used as set/bunch, qmap as map, qass as association.
A list of elements/items may itself be an element of a bunch.

struct index = inx
struct shash = shash of first elements + 2 bits type info f2/f3

value, last bits (see QStr):
0: value == handle2Double()/Str() (lowercase/num)
2: RFU
4: value == handle2Str()/ 1st uppercase
6: value == handle2Str()/ all uppercase

0 = empty string
-2 (-1) = error

 */

/** Atomic items and basic sequences (QTerm): shash + String, or shash + long[]. */
public static final QMapSto main = new QMapSto( 1);

/** Generic aggregations that contain non-atomic items (lists, bags etc). */
public static final QMapSto generic = null; // new QMap( 1 ); // Currently not needed.

/** Dedicated lists etc. */
public static QMapSto[] aggregations = new QMapSto[] { generic, null, null, null };

private static final int BOX_SHIFT = 14;
private static final int BOX_SIZE = 1 << BOX_SHIFT;
private static final int BOX_MASK = (1 << BOX_SHIFT) - 1;
//private static final String MARKER_DELETED = "" + (char) 1;

public static final int COMBINED_BITS__28 = 2 * BOX_SHIFT;
private static final int COMBINED_MASK = (1 << COMBINED_BITS__28) - 1;

public static final long FLAGS_STRUCT_TYPE = 7L << (COMBINED_BITS__28);
public static final short QSEQ_ATOMS = 0; // as sequence in 'main'
public static final short QBUNCH = 1; // ...
public static final short SEQUENCE = 3; // ...
public static final short LIST = 4;
public static final short QBAG = 5;
public static final short QMAP = 6;
public static final short QMAP3 = 7;

/** NIL: empty SEQ/ empty String */
public static final long NIL_HANDLE = 0;
public static final long STRING_ERROR = -2;
public static final long STRUC_ERROR = -4;
public static final long EMPTY_LIST_HANDLE = ((long) LIST << COMBINED_BITS__28) | QStrFunc.FLAG_INDEXED__1;

/** NIL: empty String as basic delimiting literal */
//public static final QVal NIL_DELIM = new QVal( NIL_HANDLE);
/** NIL: empty SEQ */
//public static final QVal[] NIL = new QVal[0];
public static final QVal NIL = QVal.asQVal( NIL_HANDLE);
public static final QVal[] NIL_SEQ = new QVal[] { NIL };
//public static final QVal EMPTY_AGGREG = new QVal( EMPTY_LIST_HANDLE);
public static final QVal NaN = qval4DoubleD4( Double.NaN);
public static final QVal V_0 = qval4DoubleD4( 0.0);
public static final QVal FALSE = V_0;
public static final QVal V_1 = qval4DoubleD4( 1.0 * 1.0e4);
public static final QVal NEG_1 = qval4DoubleD4( -1.0 * 1.0e4);
public static final QVal TRUE = NEG_1;
public static final QVal V_BLANK = QVal.asQVal( QStrFunc.shashBits4PunctFS( " ") & ~QStrFunc.FLAG_INDEXED__1);
/** TODO Special value for indicating delayed processing/ lazy evaluation. */
public static final QVal IN_PROCESS = qval4DoubleD4( Double.NEGATIVE_INFINITY);

/////

private final Trie[] heads;
/** Object is String for shash as single key or String[] for multiple keys (shash + oid/ ...),
 * followed by String (atomic pieces) of literals or long[] for literals (in 'main') and structs. */
private Object[][] values = new Object[][] { new Object[BOX_SIZE] };
/** For internal references: 0=temp, 1=releasable, >= 100 => stop counting */
private byte[][] refCount = new byte[][] { new byte[BOX_SIZE / 2] };
//Skip values[0][0]:
// Initial value pair: shash + string, oid + object, etc. => 2
private short[] sizes = new short[] { 2 };
private int lastClean = -1;

//TODO
//private int hDeletedHead = -1;
//private int hDeletedTail = -1;
//private int hDeletedTemp = -1;

//private static final long[] mCharHandleCache = new long[ 128 ];

//=====

//=====
/** Atomic values and basic sequences (QTerm): Wrapper for debugging ...!
 * Represents the primitive data types plus aggregations (cmp. QStr).
 * May be replaced in source by .*h*.long', except for .*o*.QVal => replace 'QVal.asQVal(' by '.*h*.('.
 */
//=====
public final static class /*o*/ QVal {
//=====

//public static final QVal EMPTY = new QVal( 0 );

/** EMPTY_ATOM in list as Object */
private static final Object EMPTY_ARRAY = NIL;

private final long handle;
private String zString;

private/*o*/ QVal( long handle) {
	this.handle = handle;
}

public static/*o*/QVal asQVal( long handle) {
	return new /*o*/QVal( handle);
}

public static long asQVal( /*o*/QVal val) {
	return val.handle;
}

/** Only for debugging!
*/
@Override
public String toString() {
	if (null == zString) {
		zString = string4Sequence( handle);
	}
	return zString;
}

/** Only for debugging!
*/
@Override
public boolean equals( Object obj) {
	if (obj instanceof QVal) {
		return handle == ((QVal) obj).handle;
	} else if (obj instanceof Long) {
		return handle == (Long) obj;
	}
	return false;
}

@Override
public int hashCode() {
	return 31 + (int) (handle ^ (handle >>> 32));
}

//=====
}

//=====
private static final class Trie {
//=====
private char[] mKeyBits;
/** int[] for refs to values (with trailing 0s), Trie for next key bits */
private Object[] mRefs;
private int mCount;

Trie( int size) {
	mKeyBits = new char[(size / 16 + 1) * 16];
	mRefs = new Object[(size / 16 + 1) * 16];
	mCount = 0;
}

/** Get element or next.
 * @param index Negative for next.
 * @return
 */
Object getElement( int index) {
	if ((0 <= index) && (index < mCount)) {
		return mRefs[index];
	}
	index = (-index - 1);
	if ((0 <= index) && (index < mCount)) {
		return mRefs[index];
	}
	return null;
}

synchronized int search( QResult yResult, char xKeyBits) {
	int iRef = Arrays.binarySearch( mKeyBits, 0, mCount, xKeyBits);
	if (0 > iRef) {
		yResult.object0 = null;
		return iRef;
	}
	yResult.object0 = mRefs[iRef];
	return iRef;
}

int search( char xKeyBits) {
	int iRef = Arrays.binarySearch( mKeyBits, 0, mCount, xKeyBits);
	return iRef;
}

int searchSubTrieIndex( Trie xTrie) {
	Object[] refs = mRefs;
	for (int i0 = 0; i0 < refs.length; ++i0) {
		//		if (refs[ i0 ] instanceof Trie) {
		if (xTrie == refs[i0]) {
			return i0;
		}
	}
	return -1;
}

int nextKeyBits( int xiRef, int comparison) {
	char[] keyBits = mKeyBits;
	xiRef += (0 >= comparison) ? 1 : -1;
	return ((0 > xiRef) || (xiRef >= mCount)) ? -1 : keyBits[xiRef];
}

synchronized int searchNext( QResult yResult, char xKeyBits) {
	int iRef = Arrays.binarySearch( mKeyBits, 0, mCount, xKeyBits);
	iRef = (0 > iRef) ? (-iRef - 1) : (iRef + 1);
	yResult.object0 = (iRef >= mRefs.length) ? null : mRefs[iRef];
	return iRef;
}

synchronized int searchPrevious( QResult yResult, char xKeyBits) {
	int iRef = Arrays.binarySearch( mKeyBits, 0, mCount, xKeyBits);
	iRef = (0 > iRef) ? (-iRef - 2) : (iRef - 1);
	yResult.object0 = (0 > iRef) ? null : mRefs[iRef];
	return iRef;
}

synchronized Trie split( int offs, int iRef, int[] handles, String[] keys) {
	if ((1 >= mCount) && ((256 + offs) >= keys.length)) {
		return this;
	}
	mRefs[iRef] = new Trie( handles.length);
	Trie trie = (Trie) mRefs[iRef];
	int from = 0;
	char keyChar = 0;
	int to = 0;
	for (; to < handles.length; ++to) {
		//		int hx = handles[ to ];
		final String key = keys[to]; // valShash( hx );
		final char kx = (offs >= key.length()) ? 0 : key.charAt( offs);
		if ((kx != keyChar) && (to > from)) {
			trie.mKeyBits[trie.mCount] = keyChar;
			trie.mRefs[trie.mCount++] = Arrays.copyOfRange( handles, from, to);
			from = to;
		}
		keyChar = kx;
	}
	if ((to > from) && (0 < handles[from])) {
		trie.mKeyBits[trie.mCount] = keyChar;
		trie.mRefs[trie.mCount++] = Arrays.copyOfRange( handles, from, to);
	}
	return trie;
}

synchronized int[] adjustHandleArrayNAddKeyChar( int len, int iRef, int keyChar) {
	//if (mKeyBits[ iRef ] > 0)/ ...:
	if ((iRef >= mRefs.length) || (mKeyBits[iRef] != (char) keyChar)) {
		++mCount;
		if (mCount >= mKeyBits.length) {
			mKeyBits = Arrays.copyOf( mKeyBits, mCount * 2);
			mRefs = Arrays.copyOf( mRefs, mCount * 2);
		}
		System.arraycopy( mKeyBits, iRef, mKeyBits, iRef + 1, mCount - iRef - 1);
		System.arraycopy( mRefs, iRef, mRefs, iRef + 1, mCount - iRef - 1);
		mKeyBits[iRef] = (char) keyChar;
		mRefs[iRef] = new int[0];
	}
	int[] handles = (int[]) mRefs[iRef];
	mRefs[iRef] = Arrays.copyOf( handles, len + 1);
	return (int[]) mRefs[iRef];
}

private synchronized boolean removeKey( char keyChar, int index, int len, int handle) {
	int iRef = Arrays.binarySearch( mKeyBits, 0, mCount, keyChar);

	///// Check for race conditions.
	if (0 > iRef) {
		return false;
	}
	final Object next = mRefs[iRef];
	if (!(next instanceof int[])) {
		return false;
	}
	final int[] handles = (int[]) next;
	if ((len < handles.length) && (0 != handles[len])) {
		return false;
	}
	if (handle != handles[index]) {
		return false;
	}

	if (1 >= len) {
		--mCount;
		System.arraycopy( mKeyBits, iRef + 1, mKeyBits, iRef, mCount - iRef);
		System.arraycopy( mRefs, iRef + 1, mRefs, iRef, mCount - iRef);
		mKeyBits[mCount] = 0;
		mRefs[mCount] = null;
		if ((16 < mCount) && (mCount < mKeyBits.length / 3)) {
			mKeyBits = Arrays.copyOf( mKeyBits, mKeyBits.length / 2);
			mRefs = Arrays.copyOf( mRefs, mRefs.length / 2);
		}
	} else {
		System.arraycopy( handles, index + 1, handles, index, len - index - 1);
		handles[len - 1] = 0;
		if ((4 < len) && (len < handles.length / 3)) {
			mRefs[iRef] = Arrays.copyOf( handles, handles.length / 2);
		}
	}
	return true;
}

Trie removeKeyBits( String key, int offs, long xHandle) {
	final int handle = (int) xHandle & COMBINED_MASK;
	char keyChar = (offs >= key.length()) ? 0 : key.charAt( offs);
	// Try again in case of race condition:
	while (true) {
		boolean done = true;
		int iRef = Arrays.binarySearch( mKeyBits, 0, mCount, keyChar);
		if (0 > iRef) {
			return this;
		}
		final Object next = mRefs[iRef];
		if (next instanceof int[]) {
			final int[] handles = (int[]) next;
			int len = handles.length;
			for (int ix = handles.length - 1; ix >= 0; --ix) {
				if (0 == handles[ix]) {
					len = ix;
				} else if (handle == handles[ix]) {
					done = removeKey( keyChar, ix, len, handle);
					break;
				}
			}
			if (done) {
				return null;
			}
		}
		if (done) {
			return (Trie) next;
		}
	}
}
//=====
}

//=====

public QMapSto( int cKeyTypes) {
	///// 0 is empty String or nil
	refCount[0][0] = 100;
	values[0][0] = "";
	values[0][1] = "";
	heads = new Trie[cKeyTypes];
	for (int i0 = cKeyTypes - 1; i0 >= 0; --i0) {
		heads[i0] = new Trie( 64);
	}
}

public boolean isFresh() {
	return (1 == sizes.length) && (2 >= sizes[0]) && (0 == heads[0].mCount);
}

public static boolean isVoid( /*h*/long handle) {
	return (STRING_ERROR == handle) || (STRUC_ERROR == handle) || (-1 == handle);
}

private static boolean isVoidOrEmpty( /*h*/long handle) {
	if (0 != (handle & QStrFunc.FLAG_INDEXED__1)) {
		return false;
		//		handle = handle & ~(FLAGS_STRUCT_TYPE ^ FLAG_STRUCT);
	}
	return (0 == handle) || (EMPTY_LIST_HANDLE == handle) || (STRING_ERROR == handle) || (STRUC_ERROR == handle);
	// old: || (-1 == handle);
}

private boolean isAtomic( /*h*/long handle) {
	if ((0 == (handle & QStrFunc.FLAG_INDEXED__1)) || isVoid( handle)) {
		return true;
	} else if ((this != main) && (QSEQ_ATOMS != (FLAGS_STRUCT_TYPE & handle))) {
		return false;
	}
	Object v0 = main.readValue( handle);
	return (v0 instanceof String);
}

private boolean isCached( long handle) {
	if ((0 == (handle & QStrFunc.FLAG_INDEXED__1)) || isVoid( handle)) {
		return false;
	}
	final int high = (int) (COMBINED_MASK & handle) >>> BOX_SHIFT;
	final int low = (int) (handle & BOX_MASK);
	if (QSEQ_ATOMS == (FLAGS_STRUCT_TYPE & handle)) {
		//		return high >= main.sizes.length;
		return (0 >= main.refCount[high][low / 2]);
	}
	//	return high >= sizes.length;
	return (0 >= refCount[high][low / 2]);
}

public static boolean isCached( int hMap, long handle) {
	return ((0 > hMap) ? main : aggregations[hMap]).isCached( handle);
}

/** Type cast, if needed.
 * @param handles as long[]
 * @return
 */
// Could be public, but: 'QVal' -> 'long' prohibits using 'null' later on.
private static QVal[] qvals4Handles( Object handles) {
	if (QVal.EMPTY_ARRAY instanceof long[]) {
		return (QVal[]) handles;
	}
	long[] in = (long[]) handles;
	QVal[] out = new QVal[in.length];
	for (int i0 = 0; i0 < out.length; ++i0) {
		out[i0] = QVal.asQVal( in[i0]);
	}
	return out;
}

/** Type cast, if needed.
 * @param vals as QVal[]
 * @return
 */
// Could be public ...
private static long[] handles4QVals( Object vals) {
	if (vals instanceof long[]) {
		return (long[]) vals;
	}
	QVal[] in = (QVal[]) vals;
	long[] out = new long[in.length];
	for (int i0 = 0; i0 < out.length; ++i0) {
		out[i0] = QVal.asQVal( in[i0]);
	}
	return out;
}

public static int maxItemsPerTerm( QVal[][] literals) {
	int max = 0;
	for (QVal[] lit : literals) {
		if (null != lit) {
			max = (max >= lit.length) ? max : lit.length;
		}
	}
	return max;
}

public static QVal[] pickItems( int inx, QVal[][] literals) {
	QVal[] out = new QVal[literals.length];
	for (int i0 = 0; i0 < out.length; ++i0) {
		if ((null == literals[i0]) || (0 == literals[i0].length)) {
			out[i0] = NIL;
		} else {
			out[i0] = literals[i0][inx % literals[i0].length];
		}
	}
	return out;
}

private synchronized Object readStoredOrCachedValue( long handle) {
	if ((this != main) && (QSEQ_ATOMS == (FLAGS_STRUCT_TYPE & handle))) {
		return main.readStoredOrCachedValue( handle);
	}
	final int high = (int) (COMBINED_MASK & handle) >>> BOX_SHIFT;
	final int low = (int) (handle & BOX_MASK);
	//	if ((high >= refCount.length) || (0 >= refCount[ high ][ low / 2 ])) {
	if (high >= sizes.length) {
		//		final int inx = COMBINED_MASK ^ (int) (handle & (COMBINED_MASK - 1));
		//		return (cache.length > inx) ? cache[ inx ] : null;
		return null;
	} else if (low >= sizes[high]) {
		return null;
	}
	return values[high][low];
}

/** Read secondary key value of Object (OID etc.)
 * @param handle
 * @return
 */
private String readKey( /*h*/long handle, int iKeyType) {
	if (0 == (QStrFunc.FLAG_INDEXED__1 & handle)) {
		return null;
	}
	final int high = (int) (COMBINED_MASK & handle) >>> BOX_SHIFT;
	final int low = (int) (handle & (BOX_MASK - 1));
	Object keys = null;
	if (high >= sizes.length) {
		//		final int inx = (COMBINED_MASK - 1) ^ (int) (handle & (COMBINED_MASK - 1));
		//		keys = (cache.length > inx) ? cache[ inx ] : null;
		return null;
	} else if (low >= sizes[high]) {
		return null;
	} else {
		keys = values[high][low];
	}
	if ((0 == iKeyType) && (keys instanceof String)) {
		return (String) keys;
	}
	return (keys instanceof String[]) && (iKeyType < ((String[]) keys).length)
		? ((String[]) keys)[iKeyType] : null;
}

public static String readKey( int hMap, long handle, int iKeyType) {
	return aggregations[hMap].readKey( handle, iKeyType);
}

public static String shash4HandleSeqAtoms( /*h*/long handle) {
	if (0 == handle) {
		return "";
	} else if (isVoidOrEmpty( handle)) {
		return "" + QStrFunc.SHASH_FLITERAL;
	} else if (0 == (handle & QStrFunc.FLAG_INDEXED__1)) {
		return QStrFunc.shash4ShashBits( handle);
	} else if (QSEQ_ATOMS != (FLAGS_STRUCT_TYPE & handle)) {
		return null;
	}
	return main.readKey( handle, 0);
}

/**
* @param shash
* @return handle offset, i.e. without index and without FLAG_STRUCT
*/
private static/*h*/long handleOffset4Shash( String shash) { //, String optString ) {
	final int len = shash.length();
	if (0 >= len) {
		return NIL_HANDLE;
	}
	long offs = QStrFunc.shashBits4shash( shash, null) & ~QStrFunc.FLAG_INDEXED__1; //, optString ) & ~1; //(ch0 & 0x1fffL) << (3 + 48);
	//	if (5 >= len) {
	//
	//		int flag = 0;
	//		flag = FLAG_INDEXED__1;
	//      if ...
	//		// Last bit is 0 for simple values:
	//		return offs | flag;
	//	}
	// Keep 28+3 bits for index + flags.
	offs = offs // (offs | ((long) shash.charAt( 2 ) << (3 + 16)))
		& ~(COMBINED_MASK | FLAGS_STRUCT_TYPE);
	return offs | QStrFunc.FLAG_INDEXED__1;
}

private String shash4Handle( /*h*/long handle) {
	String out = shash4HandleSeqAtoms( handle);
	if (null != out) {
		return out;
	}
	return readKey( handle, 0);
}

public static String shash4QVal( QVal val) {
	return main.shash4Handle( QVal.asQVal( val));
}

private String shash4Handles( long... pieces) {
	StringBuilder out = new StringBuilder( Dib2Constants.SHASH_MAX * 2);
	boolean first = true;
	for (long piece : pieces) {
		if (first) {
			first = false;
		} else {
			out.append( "" + (char) 1);
		}
		out.append( shash4Handle( piece));
		if (Dib2Constants.SHASH_MAX <= out.length()) {
			out = out.delete( Dib2Constants.SHASH_MAX - 1, out.length());
			break;
		}
	}
	return (0 >= out.length()) ? ("" + QStrFunc.SHASH_SLITERAL) : out.toString();
}

private synchronized long handle4BoxElement( QResult zTmp, int xHandle) {
	final String shash = getIndexedKeyNValue( zTmp, xHandle, 0);
	if (null == shash) {
		return 0;
	}
	Object o0 = zTmp.object0;
	zTmp.object0 = null;
	return (handleOffset4Shash( shash) & ~(long) COMBINED_MASK)
		| (((o0 instanceof long[]) && (main != this)) ? ((long) LIST << COMBINED_BITS__28) : 0) | xHandle;
}

private synchronized long handle4BoxElement( QResult zTmp, int xiBox, int xiElement) {
	return handle4BoxElement( zTmp, (xiBox << BOX_SHIFT) | xiElement);
}

private long[] findHandles( QResult zTmp, String key, int iKeyType) { //, boolean asLiteral ) {
	int[] handles = findSet4Key( zTmp, key, iKeyType, 0);
	zTmp.object0 = null;
	if (null == handles) {
		return null;
	}
	long[] out = new long[handles.length];
	int count = 0;
	for (int handle : handles) {
		final long hx = handle4BoxElement( zTmp, handle);
		if (0 != hx) {
			out[count++] = hx;
		}
	}
	return out;
}

private synchronized String getIndexedKeyNValue( QResult yResult, int handle, int iKeyType) {
	//	if (DEREF == (FLAGS_STRUCT_TYPE & handle)) { // && (this != main)) {
	//		return main.getIndexedKeyNValue( yResult, handle, iKeyType );
	//	}
	final int high = (COMBINED_MASK & handle) >>> BOX_SHIFT;
	final int low = handle & (BOX_MASK & ~1);
	Object keys = null;
	if (high >= sizes.length) {
		return null;
	} else if (low >= sizes[high]) {
		return null;
	} else {
		keys = values[high][low];
		if (null != yResult) {
			yResult.object0 = values[high][low | 1];
		}
	}
	if (null == keys) {
		return null;
	}
	String out;
	if (keys instanceof String) {
		if (iKeyType != 0) {
			return null;
		}
		out = (String) keys;
	} else {
		if (iKeyType >= ((String[]) keys).length) {
			return null;
		}
		out = ((String[]) keys)[iKeyType];
	}
	//	if (out.startsWith( MARKER_DELETED )) {
	//		return null;
	//	}
	return out;
}

private int traverseTries( QResult yResult, Trie xCurrent, int xIndex, int xDirection, Trie xParent) {
	assert 0 != xDirection;
	//TODO w/o recursion
	Object out = xCurrent.getElement( xIndex);
	if (null == out) {
		if (null == xParent) { // || (0 == xDirection)
			return -1;
		}
		int inx = xParent.searchSubTrieIndex( xCurrent);
		if (0 > inx) {
			return -1;
		}
		inx += xDirection;
		if (0 > inx) {
			return -1;
		}
		return traverseTries( yResult, xParent, inx, xDirection, null);
	} else if (out instanceof int[]) {
		yResult.object0 = xCurrent;
		return xIndex;
	}
	return traverseTries( yResult, (Trie) out, (0 <= xDirection) ? 0 : -1, xDirection, xCurrent);
}

private int traverseTries( QResult yResult, Trie xCurrent, int xiLevel, String xKey, int xiKeyType, int xiStep, Trie xParent) {
	//TODO w/o recursion
	Trie current = (null == xCurrent) ? heads[xiKeyType] : xCurrent;
	final char keyChar = (xiLevel >= xKey.length()) ? 0 : xKey.charAt( xiLevel);
	int iRef = current.search( yResult, keyChar);
	Object out = yResult.object0;
	yResult.object0 = null;
	if (null == out) {
		if (0 == xiStep) {
			return -1;
		}
		iRef = -iRef - 1 - ((0 > xiStep) ? 1 : 0);
		if (0 <= iRef) {
			out = current.getElement( iRef);
		}
		if (null == out) {
			if ((null == xParent) || (0 == xiStep)) {
				return -1;
			}
			iRef = xParent.searchSubTrieIndex( current);
			iRef += xiStep;
			if (0 > iRef) {
				return -1;
			}
			return traverseTries( yResult, xParent, iRef, xiStep, null);
		} else if (out instanceof int[]) {
			yResult.object0 = current;
			return iRef;
		}
	} else if (out instanceof int[]) {
		if (0 == xiStep) {
			yResult.object0 = current;
			return iRef;
		}
		iRef += xiStep;
		//		iStep = -iStep;
		if (0 > iRef) {
			return -1;
		}
		iRef = traverseTries( yResult, current, iRef, xiStep, null);
		current = (Trie) yResult.object0;
		out = null;
	}
	if (null != out) {
		iRef = traverseTries( yResult, (Trie) out, xiLevel + 1, xKey, xiKeyType, xiStep, current);
		current = (Trie) yResult.object0;
	}
	if ((null != current) || (null == xParent)) {
		yResult.object0 = current;
		return iRef;
	}
	iRef = xParent.searchSubTrieIndex( xCurrent);
	iRef += xiStep;
	if (0 > iRef) {
		return -1;
	}
	return traverseTries( yResult, xParent, iRef, xiStep, null);
}

/** Split if handles[] is full and 'large': assuming no trailing 0s.
 * @param iKeyType
 * @param trie
 * @param offs
 * @param iRef
 * @param handles
 */
private synchronized void splitTrie( int iKeyType, Trie trie, int offs, int iRef, int[] handles) {
	String[] keys = new String[handles.length];
	for (int to = handles.length - 1; to >= 0; --to) {
		int hx = handles[to];
		final String key = getIndexedKeyNValue( null, hx, iKeyType);
		keys[to] = key;
	}
	trie.split( offs, iRef, handles, keys);
}

/** Having found the data, do the synchronized process.
 * @param object Data if iKeyType == 0 (shash), otherwise handle for secondary key.
 * @param iKeyType 0 = shash, otherwise secondary key (e.g. 1 = OID).
 * @param key
 * @param offs
 * @param trie
 * @param lenHandles
 * @param iRef
 * @param insert
 * @return
 */
private synchronized int addKeyCharNHandle( QResult zTmp, Object object, int iKeyType, String key,
	int offs, Trie trie, int lenHandles, int iRef, int insert) {
	char keyChar = (offs >= key.length()) ? 0 : key.charAt( offs);

	///// Check for race conditions.
	int ix = trie.search( zTmp, keyChar);
	final Object next = zTmp.object0;
	zTmp.object0 = null;
	ix = (0 > ix) ? (-ix - 1) : ix;
	if (iRef != ix) {
		return 0;
	}
	if (next instanceof int[]) {
		final int[] handles = (int[]) next;
		if (lenHandles > handles.length) {
			return 0;
		} else if ((lenHandles < handles.length) && (0 != handles[lenHandles])) {
			return 0;
		} else if (insert > lenHandles) {
			return 0;
		} else if (insert < lenHandles) {
			int handle = handles[insert];
			final String keyx = getIndexedKeyNValue( null, handle, iKeyType);
			if (null == keyx) {
				return 0;
			}
			final int cmp = key.compareTo( keyx);
			if (0 < cmp) {
				return 0;
			} else if (0 > cmp) {
				if (0 < insert) {
					final String k1 = getIndexedKeyNValue( null, handles[insert - 1], iKeyType);
					if ((null != k1) && (0 > key.compareTo( k1))) {
						return 0;
					}
				}
			}
		}
	}
	int[] handles = trie.adjustHandleArrayNAddKeyChar( lenHandles, iRef, ((offs < key.length()) ? keyChar : 0)); //-1) );
	if (lenHandles > insert) {
		System.arraycopy( handles, insert, handles, insert + 1, lenHandles - insert);
	}
	int handle = 0;
	if (0 == iKeyType) {
		if (sizes[values.length - 1] >= values[values.length - 1].length) {
			final int hx = (lastClean + 1) % sizes.length;
			//TODO Speed up.
			for (int i0 = sizes[hx] - 2; i0 > 0; i0 -= 2) {
				if (null == values[hx][i0]) {
					handle = (hx << BOX_SHIFT) | i0 | 1;
					break;
				}
			}
			if (0 == handle) {
				values = Arrays.copyOf( values, values.length + 1);
				values[values.length - 1] = new Object[BOX_SIZE];
				sizes = Arrays.copyOf( sizes, values.length);
				refCount = Arrays.copyOf( refCount, values.length);
				refCount[refCount.length - 1] = new byte[BOX_SIZE / 2];
			}
		}
		if (0 == handle) {
			handle = ((values.length - 1) << BOX_SHIFT) | sizes[values.length - 1] | QStrFunc.FLAG_INDEXED__1;
			sizes[values.length - 1] += 2;
		}
		//		refCount[ values.length - 1 ][ sizes[ values.length - 1 ] / 2 ] = 0; //1;
		values[handle >>> BOX_SHIFT][handle & (BOX_MASK - 1)] = key;
		values[handle >>> BOX_SHIFT][handle & BOX_MASK] = object;
	} else {
		handle = ((Integer) object) & COMBINED_MASK;
	}
	handles[insert] = handle;
	// Some keys, especially short keys, e.g. for punctuation, may end up in having long lists.
	// This is okay if the lists are sorted and the strings are short. Otherwise, we rather
	// split the search tree:
	if ((offs < key.length()) && (lenHandles >= handles.length) && (16 <= lenHandles) && (0 != key.charAt( offs))) {
		++offs;
		splitTrie( iKeyType, trie, offs, iRef, handles);
	}
	return handle;
}

private int findObject( QResult zTmp, boolean needTrie, int iKeyType, Object object, String key) {
	int iRef = 0;
	int offs = 0;
	int insert = 0;
	int len = 0;
	char keyChar = 0;
	Trie trie = heads[iKeyType];
	for (; offs <= key.length(); ++offs) {
		keyChar = (offs >= key.length()) ? 0 : key.charAt( offs);
		iRef = trie.search( zTmp, keyChar);
		final Object next = zTmp.object0;
		if (0 > iRef) {
			iRef = -iRef - 1;
			break;
		}
		if (next instanceof int[]) {
			final int[] handles = (int[]) next;
			len = handles.length;
			//TODO speed up
			for (insert = 0; insert < len; ++insert) {
				int handle = handles[insert];
				if (0 == handle) {
					// Trailing 0.
					len = insert;
					break;
				}
				final String keyx = getIndexedKeyNValue( null, handle, iKeyType);
				if (null == keyx) {
					continue;
				}
				final int cmp = key.compareTo( keyx);
				if (0 > cmp) {
					break;
				} else if (0 == cmp) {
					if ((object instanceof Integer) && (handle != (Integer) object)) {
						continue;
					} else if ((object instanceof String) &&
						!((String) object).equals( values[BOX_MASK & (handle >>> BOX_SHIFT)][handle & BOX_MASK])) {
						continue;
					} else if (object instanceof long[]) {
						Object vx = values[BOX_MASK & (handle >>> BOX_SHIFT)][handle & BOX_MASK];
						if (!(vx instanceof long[]) || !Arrays.equals( (long[]) object, (long[]) vx)) {
							continue;
						}
					}
					//					takeHandle( handle );
					return handle;
				}
			}
			break;
		}
		trie = (Trie) next;
		iRef = 0;
	}
	zTmp.object0 = null;
	if (needTrie) {
		zTmp.long0 = offs;
		zTmp.object0 = trie;
		zTmp.long1 = len;
		zTmp.long2 = iRef;
		zTmp.long3 = insert;
	}
	return 0;
}

// synchronized on the lower level
private int putObject( QResult zTmp, int iKeyType, Object object, String key) {
	// Try again in case of race condition:
	for (int i0 = 0; i0 < 999; ++i0) {
		int handle = findObject( zTmp, true, iKeyType, object, key);
		if (0 != handle) {
			zTmp.object0 = null;
			return handle;
		}
		if (100 <= i0) {
			// Lost something ...?
			Thread.yield();
		}

		final int offs = (int) zTmp.long0;
		final Trie trie = (Trie) zTmp.object0;
		final int len = (int) zTmp.long1;
		final int iRef = (int) zTmp.long2;
		final int insert = (int) zTmp.long3;
		zTmp.object0 = null;
		handle = addKeyCharNHandle( zTmp, object, iKeyType, key, offs, trie, len, iRef, insert);
		if (0 != handle) {
			return handle;
		}
	}
	return 0;
}

/** Secondary keys, e.g. OID, for stored objects. Does not work for atomic values. */
private synchronized long addKey( QResult zTmp, /*h*/long handle, int iKeyType, String secondaryKey) {
	final int high = (int) (COMBINED_MASK & handle) >>> BOX_SHIFT;
	final int low = (int) (handle & BOX_MASK);
	if ((high >= sizes.length) || (low >= sizes[high])) {
		return STRING_ERROR;
	}
	final Object kx = values[high][low & ~1];
	String[] keys = (kx instanceof String[]) ? (String[]) kx : new String[] { (String) kx };
	//	if (null != keys) { // && !keys[ 0 ].startsWith( MARKER_DELETED )) {
	if ((iKeyType < keys.length) && (secondaryKey.equals( keys[iKeyType]))) {
		return STRING_ERROR;
	} else if (iKeyType >= keys.length) {
		keys = Arrays.copyOf( keys, iKeyType + 1);
	}
	keys[iKeyType] = secondaryKey;
	values[high][low & ~1] = keys;
	if (0 == putObject( zTmp, iKeyType, (int) handle, secondaryKey)) {
		return STRUC_ERROR;
	}
	return handle;
}

private synchronized String removeKeys( int level, /*h*/long handle, String key) {
	final int high = (int) (COMBINED_MASK & handle) >>> BOX_SHIFT;
	final int low = (int) (handle & BOX_MASK);
	if ((high >= sizes.length) || (low >= sizes[high])) {
		return null;
	}
	String out = null;
	String[] keys = null;
	if (0 == level) {
		values[high][low & ~1] = null;
	} else {
		final Object kx = values[high][low & ~1];
		if (kx instanceof String[]) {
			keys = (String[]) kx;
			if ((null != keys) && (level < keys.length)) {
				if (key.equals( keys[level])) {
					values[high][low & ~1] = Arrays.copyOf( keys, level);
					out = key;
				}
			}
		}
	}
	keys = (null != keys) ? keys : new String[] { key };
	for (int iKeyType = level; iKeyType < keys.length; ++iKeyType) {
		int offs = 0;
		for (Trie trie = heads[iKeyType]; offs <= keys[iKeyType].length(); ++offs) { // offs <= key.length(); ++ offs) {
			Trie old = trie;
			trie = trie.removeKeyBits( keys[iKeyType], offs, handle);
			if (null == trie) {
				if (iKeyType == level) {
					out = key;
				}
				break;
			} else if (old == trie) {
				break;
			}
		}
		if (0 != level) {
			break;
		}
	}
	return out;
}

/** For quick shash calculation: does not work for numbers, not for 'sharp S'.
 */
private static long handle4AsciiShort( String str) {
	if (0 >= str.length()) {
		return NIL_HANDLE;
	}
	return QStrFunc.shashBits4Ansi( str, true) & ~QStrFunc.FLAG_INDEXED__1;
}

public static QVal qval4AsciiShort( String str) {
	return QVal.asQVal( handle4AsciiShort( str));
}

/** For cleaning up internally. */
public static synchronized void idle() {
	QMapSto map = main;
	//	fillCharHandleCache();
	for (int h0 = -1; null != map;) {
		//		hDeletedTemp = hDeletedTail;
		//		map.iCacheOld = map.iCache;
		map.lastClean = (map.lastClean + 1) % map.sizes.length;
		int i1 = map.lastClean;
		for (int i0 = 0; i0 < map.sizes[i1]; i0 += 2) {
			if (0 >= map.refCount[i1][i0 / 2]) {
				final Object kx = map.values[i1][i0];
				final String key = (kx instanceof String) ? (String) kx
					: ((kx instanceof String[]) ? ((String[]) kx)[0] : null);
				if (null != key) {
					map.removeKeys( 0, (i1 << BOX_SHIFT) | i0, key);
				}
				map.values[i1][i0 + 1] = null;
			}
		}
		++h0;
		map = (h0 >= aggregations.length) ? null : aggregations[h0];
	}
}

/////

public static boolean isAtomic( int hMap, /*h*/long handle) {
	return ((0 > hMap) ? main : aggregations[hMap]).isAtomic( handle);
}

public static boolean isAtomic( QVal val) {
	return main.isAtomic( QVal.asQVal( val));
}

public static boolean isEmpty( /*h*/long handle) {
	if (0 != (handle & QStrFunc.FLAG_INDEXED__1)) {
		return false;
	}
	return (0 == handle) || (EMPTY_LIST_HANDLE == handle);
}

/** For atomic items. */
private static boolean isNumeric( /*h*/long handle) {
	char shash0;
	if (isVoid( handle)) {
		return false;
	} else if (0 != (handle & QStrFunc.FLAG_INDEXED__1)) {
		final String shash = shash4HandleSeqAtoms( handle);
		if (null == shash) {
			return false;
		}
		shash0 = shash.charAt( 0);
	} else {
		final long inBytes = handle >>> 3;
		shash0 = (char) (0xe000 | (inBytes >>> 48));
	}
	return (QStrFunc.SHASH_NEG <= shash0) && (shash0 < QStrFunc.SHASH_LITERAL);
}

public static boolean isNumeric( QVal val) {
	return isNumeric( QVal.asQVal( val));
}

/** For atomic items. */
private static boolean isDate( /*h*/long handle) {
	char shash0;
	if (isVoid( handle)) {
		return false;
	} else if (0 != (handle & QStrFunc.FLAG_INDEXED__1)) {
		final String shash = shash4HandleSeqAtoms( handle);
		if (null == shash) {
			return false;
		}
		shash0 = shash.charAt( 0);
	} else {
		final long inBytes = handle >>> 3;
		shash0 = (char) (0xe000 | (inBytes >>> 48));
	}
	return (QStrFunc.SHASH_DATE <= shash0) && (shash0 < QStrFunc.SHASH_NEG);
}

public static boolean isDate( QVal val) {
	return isDate( QVal.asQVal( val));
}

//private static/*h*/long handle4Double( double value ) {
//	return handleOffset4Shash( QStr.shash4Double( null, value ), null );
//}

public static QVal qval4DoubleD4( double valueD4) {
	return QVal.asQVal( QStrFunc.shashBits4DoubleD4( null, valueD4) & ~QStrFunc.FLAG_INDEXED__1);
}

public static QVal qval4SlotSecond16( long slotSecond16) {
	return QVal.asQVal( QStrFunc.shashBits4slotSecond16( slotSecond16) & ~QStrFunc.FLAG_INDEXED__1);
}

/** For atomic items. */
private static double doubleD4oHandleNum( /*h*/long handle) {
	if (isVoidOrEmpty( handle)) {
		return Double.NaN;
	} else if (0 != (handle & QStrFunc.FLAG_INDEXED__1)) {
		String shash = main.readKey( handle, 0); //shash4HandleLiteral( handle );
		return ((null == shash) || (0 >= shash.length()) || (QStrFunc.SHASH_LITERAL <= shash.charAt( 0))) ? Double.NaN
			: QStrFunc.doubleD4oShashNum( shash);
	}
	return QStrFunc.doubleD4oShashBits( handle);
}

public static double doubleD4oQVal( QVal val) {
	return doubleD4oHandleNum( QVal.asQVal( val));
}

/** For atomic items. */
private static long slotSecond16oHandle( /*h*/long handle) {
	if (isVoid( handle)) {
		return 0;
	}
	return (handle & ((QStrFunc.SHASH_DATE_0 << 1) - 1)) - QStrFunc.SHASH_DATE_0;
}

public static long slotSecond16oQVal( QVal val) {
	return slotSecond16oHandle( QVal.asQVal( val));
}

private Object readValue( long handle) { //, boolean preferString ) {
	if (0 == handle) {
		return "";
	} else if (isVoid( handle)) {
		return null;
	} else if (0 == (handle & QStrFunc.FLAG_INDEXED__1)) {
		return QStrFunc.string4ShashBits( handle);
	}
	return readStoredOrCachedValue( handle);
}

private int[] findSet4Key( String xKey, int xiKeyType) {
	String key = xKey;
	int offs = 0;
	//	if (key.startsWith( MARKER_DELETED )) {
	//		return null;
	//	}
	for (Trie trie = heads[xiKeyType]; offs <= key.length(); ++offs) {
		final char keyChar = (offs >= key.length()) ? 0 : key.charAt( offs);
		int iRef = trie.search( keyChar);
		if (0 > iRef) {
			return null;
		}
		Object next = trie.getElement( iRef);
		if (next instanceof int[]) {
			int[] handles = (int[]) next;
			int count = 0;
			int[] out = new int[handles.length];
			//TODO speed up
			for (int handle : handles) {
				if (0 == handle) {
					// Trailing 0.
					break;
				}
				final String keyx = getIndexedKeyNValue( null, handle, xiKeyType);
				if (key.equals( keyx)) {
					out[count++] = handle;
				}
			}
			return Arrays.copyOf( out, count);
		}
		trie = (Trie) next;
	}
	return null;
}

private int[] findSet4Key( QResult zTmp, String xKey, int xiKeyType, int comparison) {
	String key = xKey;
	int offs = 0;
	//	if (key.startsWith( MARKER_DELETED )) {
	//		return null;
	//	}
	for (Trie trie = heads[xiKeyType]; offs <= key.length(); ++offs) {
		final char keyChar = (offs >= key.length()) ? 0 : key.charAt( offs);
		int iRef;
		if ((0 < comparison) && (0 == keyChar)) {
			iRef = trie.searchPrevious( zTmp, keyChar);
		} else {
			iRef = trie.search( zTmp, keyChar);
		}
		Object next = zTmp.object0;
		zTmp.object0 = null;
		if (null == next) {
			if (0 < comparison) {
				iRef = trie.searchPrevious( zTmp, keyChar);
			} else if (0 > comparison) {
				iRef = trie.searchNext( zTmp, keyChar);
			}
			next = zTmp.object0;
			zTmp.object0 = null;
			if (null == next) {
				return null;
			}
		}
		if (next instanceof int[]) {
			int[] handles = (int[]) next;
			int count = 0;
			int[] out = new int[handles.length];
			//TODO speed up
			for (int handle : handles) {
				if (0 == handle) {
					// Trailing 0.
					break;
				}
				final String keyx = getIndexedKeyNValue( null, handle, xiKeyType);
				if ((0 == comparison) ? key.equals( keyx)
					: ((comparison < 0) == (key.compareTo( keyx) < 0))) {
					out[count++] = handle;
				}
			}
			if ((0 == count) && (0 != comparison)) {
				int ch = trie.nextKeyBits( iRef, comparison);
				if (0 <= ch) {
					key = key.substring( 0, offs) + (char) ch;
					--offs;
					continue;
				}
			}
			return Arrays.copyOf( out, count);
		}
		trie = (Trie) next;
	}
	return null;
}

//For main
private static boolean equalValue( long handle, String str) {
	if (0 != (FLAGS_STRUCT_TYPE & handle)) {
		return false;
	} else if (0 == (QStrFunc.FLAG_INDEXED__1 & handle)) {
		return QStrFunc.shash( str).equals( shash4HandleSeqAtoms( handle));
	}
	final Object v0 = main.readStoredOrCachedValue( handle);
	if (v0 instanceof String) {
		return QStrFunc.shash( str).equals( main.shash4Handle( handle));
	}
	return false;
}

//For main
private static boolean equalSeqAtoms( long handle0, long handle1) {
	if (handle0 == handle1) {
		return true;
	} else if ((FLAGS_STRUCT_TYPE & handle0) != (FLAGS_STRUCT_TYPE & handle1)) {
		return false;
		//	} else if (0 != (FLAG_STRUCT & handle0)) {
		//		// Outside of main => not a literal.
		//		return false;
	}
	// If they were equal, they would have the same handle ...
	return false;
	///// Therefore, the following check is currently not needed ...
	//	final Object v0 = main.readStoredOrCachedValue( handle0 );
	//	if (v0 instanceof String) {
	//		return ((String) v0).equals( main.readStoredOrCachedValue( handle1 ) ); //main.shash4Handle( handle0 ).equals( main.shash4Handles( handle1 ) );
	//	} else if (!(v0 instanceof long[])) {
	//		return false;
	//	}
	//	final Object v1 = main.readStoredOrCachedValue( handle1 );
	//	if (!(v1 instanceof long[])) {
	//		return false;
	//	}
	//	long[] a0 = (long[]) v0;
	//	long[] a1 = (long[]) v1;
	//	if (a0.length != a1.length) {
	//		return false;
	//	}
	//	for (int i0 = 0; i0 < a0.length; ++ i0) {
	//		if (!main.shash4Handle( handle0 ).equals( main.shash4Handle( handle1 ) )) {
	//			return false;
	//		}
	//	}
	//	return true;
}

private boolean equals( long handle0, long handle1) {
	if (handle0 == handle1) {
		return true;
	} else if ((FLAGS_STRUCT_TYPE & handle0) != (FLAGS_STRUCT_TYPE & handle1)) {
		return false;
	} else if (0 == (FLAGS_STRUCT_TYPE & handle0)) {
		return equalSeqAtoms( handle0, handle1);
	}
	final Object v0 = readValue( handle0);
	final Object v1 = readValue( handle1);
	if (v0 instanceof long[]) {
		if (!(v1 instanceof long[])) {
			return false;
		}
		final long[] a0 = (long[]) v0;
		final long[] a1 = (long[]) v1;
		if (a0.length != a1.length) {
			return false;
		}
		for (int i0 = 0; i0 < a0.length; ++i0) {
			if (!equals( a0[i0], a1[i0])) {
				return false;
			}
		}
		return true;
	}
	return v0.equals( v1);
}

/** For special cases: otherwise just use (handle0 == handle1) !!
 * @param hMap
 * @param handle0
 * @param handle1
 * @return
 */
public static boolean equals( int hMap, long handle0, long handle1) {
	final QMapSto base = (0 > hMap) ? main : aggregations[hMap];
	return base.equals( handle0, handle1);
}

public static boolean equalValues( long handle, String... items) {
	if (1 >= items.length) {
		if (0 == items.length) {
			return NIL_HANDLE == handle;
		} else if (0 != (FLAGS_STRUCT_TYPE & handle)) {
			return false;
		}
		return QStrFunc.shash( items[0]).equals( main.shash4Handle( handle));
	}
	if (0 == (QStrFunc.FLAG_INDEXED__1 & handle)) {
		return false;
	}
	final Object v0 = main.readValue( handle);
	if ((null == v0) || !(v0 instanceof long[])) {
		return false;
	}
	final long[] a0 = (long[]) v0;
	if (a0.length != items.length) {
		return false;
	}
	for (int i0 = 0; i0 < a0.length; ++i0) {
		if (!equalValue( a0[i0], items[i0])) {
			return false;
		}
	}
	return true;
}

private boolean equals( long handle0, long[] ahItems) {
	if (0 == (FLAGS_STRUCT_TYPE & handle0)) {
		return (1 == ahItems.length) && equalSeqAtoms( handle0, ahItems[0]);
	}
	final Object v0 = readValue( handle0);
	if (v0 instanceof long[]) {
		final long[] a0 = (long[]) v0;
		if (a0.length != ahItems.length) {
			return false;
		}
		for (int i0 = 0; i0 < a0.length; ++i0) {
			if (!equals( a0[i0], ahItems[i0])) {
				return false;
			}
		}
		return true;
	}
	return (1 == ahItems.length) && (equals( handle0, ahItems[0]));
}

public static boolean equals( int hMap, long handle0, long[] ahItems) {
	final QMapSto base = (0 > hMap) ? main : aggregations[hMap];
	if ((0 == (FLAGS_STRUCT_TYPE & handle0)) || (null == base)) {
		return (1 == ahItems.length) && equalSeqAtoms( handle0, ahItems[0]);
	}
	return base.equals( handle0, ahItems);
}

private static long handleCached4AtomicLiteral( QResult zTmp, String atomic, String... optionalShash) {
	long offs;
	if (10 >= atomic.length()) {
		if (0 < optionalShash.length) {
			offs = QStrFunc.shashBits4shash( optionalShash[0], atomic) & ~QStrFunc.FLAG_INDEXED__1;
		} else {
			offs = QStrFunc.shashBits4Literal( atomic, true) & ~QStrFunc.FLAG_INDEXED__1;
		}
		//TODO speed up
		if (atomic.equals( QStrFunc.string4ShashBits( offs))) {
			return offs;
		}
	}
	final String shash = (0 >= optionalShash.length) ? QStrFunc.shash4Literal( atomic, 1) : optionalShash[0];
	offs = handleOffset4Shash( shash);
	if (0 == (offs & QStrFunc.FLAG_INDEXED__1)) {
		return offs;
	}
	final int inx = main.putObject( zTmp, 0, atomic, shash);
	return (0 == inx) ? STRING_ERROR : ((offs & ~(long) COMBINED_MASK) | inx | QStrFunc.FLAG_INDEXED__1);
}

public static long cacheQVal( QVal val) {
	// Is already available, nothing actually to be done ...
	return QVal.asQVal( val);
}

private long cacheAggreg( QResult zTmp, short aggregType, long[] xmItems, String optionalShash) {
	final String shash = (null == optionalShash) ? shash4Handles( xmItems) : optionalShash;
	long offs = handleOffset4Shash( shash) | ((long) aggregType << COMBINED_BITS__28);
	if (isVoidOrEmpty( offs)) {
		return offs;
	}
	int[] ajHandles = findSet4Key( shash, 0);
	if (null != ajHandles) {
		for (int jh : ajHandles) {
			if (this.equals( jh, xmItems)) {
				//				get full handle/ literal 
				return ((offs & ~(long) COMBINED_MASK) | jh | QStrFunc.FLAG_INDEXED__1);
			}
		}
	}
	final int inx = putObject( zTmp, 0, xmItems, shash);
	if (0 == inx) {
		return STRUC_ERROR;
	}
	return inx | ((offs & ~(long) COMBINED_MASK) | QStrFunc.FLAG_INDEXED__1);
}

public static QVal cacheSequence( QResult zTmp, QVal... sequence) {
	if (0 == sequence.length) {
		return NIL;
	} else if (1 == sequence.length) {
		return sequence[0];
	}
	return QVal.asQVal( main.cacheAggreg( zTmp, SEQUENCE, handles4QVals( sequence), null));
}

//public static QVal[] cacheSequences( QResult zTmp, QVal[]... sequences) {
//	QVal[] out = new QVal[sequences.length];
//	for (int i0 = 0; i0 < sequences.length; ++i0) {
//		out[i0] = QVal.asQVal( main.cacheAggreg( zTmp, SEQUENCE, handles4QVals( sequences[i0]), null));
//	}
//	return out;
//}

private long storeCachedEntry( QResult zTmp, long handle) {
	if (isVoidOrEmpty( handle) || (0 == (QStrFunc.FLAG_INDEXED__1 & handle))) {
		return handle;
	}
	final int high = (int) (COMBINED_MASK & handle) >>> BOX_SHIFT;
	final int low = (int) (handle & BOX_MASK);
	if ((high >= sizes.length) || (low >= sizes[high])) {
		return STRUC_ERROR;
	}
	Object v0 = readValue( handle);
	if (null == v0) {
		return STRUC_ERROR;
	}
	if (0 < refCount[high][low / 2]) {
		if (100 > refCount[high][low / 2]) {
			++refCount[high][low / 2];
		} else if (100 == refCount[high][low / 2]) {
			++refCount[high][low / 2];
			if (this == main) {
				if (v0 instanceof String) {
					((String) v0).intern();
				}
			}
		}
		return handle;
	}
	refCount[high][low / 2] = 1;
	if (!(v0 instanceof long[])) {
		return handle;
	}
	long[] xmItems = (long[]) v0;
	for (int i0 = 0; i0 < xmItems.length; ++i0) {
		if (0 != (QStrFunc.FLAG_INDEXED__1 & xmItems[i0])) {
			xmItems[i0] = ((QSEQ_ATOMS == (FLAGS_STRUCT_TYPE & xmItems[i0]))
				? main : this).storeCachedEntry( zTmp, xmItems[i0]);
		}
	}
	return handle;
}

private synchronized void release( /*h*/long handle) { //, boolean force ) {
	final int high = (int) (COMBINED_MASK & handle) >>> BOX_SHIFT;
	final int low = (int) (handle & BOX_MASK);
	if ((high >= sizes.length) || (low >= sizes[high])) {
		return;
	}
	final Object v0 = values[high][low];
	final Object kx = values[high][low & ~1];
	if (//keys[ 0 ].startsWith( MARKER_DELETED ) || 
	(null == v0) || (null == kx)) {
		///// Should not happen.
		refCount[high][low / 2] = 0;
		return;
	}
	final String[] keys = (kx instanceof String[]) ? (String[]) kx : new String[] { (String) kx };
	if (1 < refCount[high][low / 2]) {
		// Still needed internally/ only top level entries can be deleted.
		return;
	}
	//	if (force || (1 == refCount[ high ][ low / 2 ])) 
	{
		removeKeys( 0, handle, keys[0]);
		values[high][low] = null;
		values[high][low & ~1] = null;
		if (sizes[high] <= (low + 1)) {
			int sx = low - 1;
			while ((sx >= 0) && (null == values[high][sx])) {
				--sx;
			}
			sizes[high] = (short) (sx + 1);
		}
	}
	refCount[high][low / 2] = 0;
	return;
}

public static void storeQVals( QResult zTmp, QVal... xyCachedQVals) {
	for (int i0 = 0; i0 < xyCachedQVals.length; ++i0) {
		xyCachedQVals[i0] = QVal.asQVal(
			main.storeCachedEntry( zTmp, QVal.asQVal( xyCachedQVals[i0])));
	}
}

public static long cacheAggreg4QVals( QResult zTmp, short aggregType, int hMap, QVal... xmItems) {
	if (0 >= xmItems.length) {
		return EMPTY_LIST_HANDLE;
	} else if ((1 == xmItems.length) && (0 == aggregType)) {
		return cacheQVal( xmItems[0]);
	}
	return aggregations[hMap].cacheAggreg( zTmp, aggregType, handles4QVals( xmItems), null);
}

public static long cacheAggreg( QResult zTmp, short aggregType, int hMap, long... xmItems) {
	if (0 >= xmItems.length) {
		return EMPTY_LIST_HANDLE;
	}
	return aggregations[hMap].cacheAggreg( zTmp, aggregType, handles4QVals( xmItems), null);
}

private long storeAggreg( QResult zTmp, long cachedAggreg) {
	return storeCachedEntry( zTmp, cachedAggreg);
}

public static long storeCachedAggreg( QResult zTmp, int hMap, long cachedAggreg, String... additionalKeys) {
	long out = aggregations[hMap].storeAggreg( zTmp, cachedAggreg);
	for (int i0 = 0; i0 < additionalKeys.length; ++i0) {
		aggregations[hMap].addKey( zTmp, out, i0 + 1, additionalKeys[i0]);
	}
	return out;
}

public static QVal qval4AtomicValue( QResult zTmp, String atom) {
	// Need to decide on its type ...
	String shash = QStrFunc.shash( atom);
	return QVal.asQVal( handleCached4AtomicLiteral( zTmp, atom, shash));
}

public static QVal qval4AtomicLiteral( QResult zTmp, String strAtomic) {
	return QVal.asQVal( handleCached4AtomicLiteral( zTmp, strAtomic));
}

private static final String string4Atoms_fallback = "?";

private static String string4Atoms( long[] handles, String... fillers) {
	StringBuilder out = new StringBuilder( 128 + 16 * handles.length);
	final String filler = (0 < fillers.length) ? fillers[0] : " ";
	boolean punct = true;
	for (long handle : handles) {
		final Object v0 = main.readValue( handle);
		if (v0 instanceof long[]) {
			if (!punct) {
				out.append( (1 < fillers.length) ? fillers[1] : filler);
			}
			punct = false;
			out.append( string4Atoms( (long[]) v0));
			continue;
		}
		//		final String shash = main.shash4Handle( handle );
		final char shash0 = (char) (0xe000 | (handle >>> (48 + 3)));
		String s0 = ((null == v0) || !(v0 instanceof String)) ? string4Atoms_fallback : (String) v0;
		if ((0 == s0.length()) || (QStrFunc.SHASH_NUM > shash0)) {
			//|| QStr.PATTERN_SPACE_NL.matcher( "" + s0.charAt( 0 ) ).matches()) {
			if (!(0 == s0.length()) && (QStrFunc.SHASH_ERR_MAX > shash0)) {
				s0 = (QStrFunc.SHASH_OFFS == shash0) ? "NaN" : "ERROR";
			}
			punct = true;
		} else {
			if (!punct) {
				out.append( filler);
			}
			punct = false;
		}
		out.append( s0);
	}
	return out.toString();
}

/** Create handles for (marked) token pieces of String. */
private static long[] cacheAtoms4String( QResult zTmp, String[] elements, boolean forcedMarkers) {
	long[] out = new long[2 + 2 * elements.length];
	int count = 0;
	//	boolean toSkip = false;
	boolean skipped = false;
	boolean punct = true;
	for (int i0 = 0; i0 < elements.length; ++i0) {
		final String el = elements[i0];
		if (0 >= el.length()) {
			continue;
		}
		if ((el.equals( " ") || el.equals( "" + QStrFunc.SHASH_FLITERAL + ' '))
			//			&& !skipped && (0 < i0)) {
			&& !punct) {
			skipped = true;
			//			toSkip = false;
			punct = true;
			continue;
		}
		final char ch0 = el.charAt( 0);
		long handle = 0;
		String shash = null;
		if (QStrFunc.SHASH_OFFS > ch0) {
			if (forcedMarkers) {
				handle = handleCached4AtomicLiteral( zTmp, el);
			} else {
				// Need to decide on its type ...
				shash = QStrFunc.shash( el);
				//				handle = handleCached4AtomicLiteral( zTmp, el, shash );
			}
		} else if (0xf800 <= ch0) {
			handle = handleCached4AtomicLiteral( zTmp, el);
		} else if (QStrFunc.SHASH_LITERAL <= ch0) {
			handle = handleCached4AtomicLiteral( zTmp, el.substring( 1));
		} else if ((QStrFunc.SHASH_FLITERAL == ch0) || (QStrFunc.SHASH_SLITERAL == ch0)) {
			handle = QStrFunc.shashBits4PunctFS( el.substring( 1)) & ~QStrFunc.FLAG_INDEXED__1;
			if (!el.substring( 1).equals( QStrFunc.string4ShashBits( handle))) {
				shash = QStrFunc.shash4ShashBits( handle);
			}
		} else if ((QStrFunc.SHASH_DATE == ch0)) {
			final long slotSec16 = MiscFunc.slotSecond16oDateApprox( el.substring( 1));
			handle = QStrFunc.shashBits4slotSecond16( slotSec16) & ~QStrFunc.FLAG_INDEXED__1;
			if (!el.substring( 1).equals( QStrFunc.string4ShashBits( handle))) {
				shash = QStrFunc.shash4ShashBits( handle);
			}
		} else if ((QStrFunc.SHASH_TEMP_NUM_STRING == ch0) || (QStrFunc.SHASH_NEG <= ch0)) {
			handle = QStrFunc.shashBits4DoubleD4( el.substring( 1), 0) & ~QStrFunc.FLAG_INDEXED__1;
			if (!el.substring( 1).equals( QStrFunc.string4ShashBits( handle))) {
				shash = QStrFunc.shash4ShashBits( handle);
			}
		} else {
			// Marked.
			//TODO Special treatment for hex, base64, SHASH_NUM etc.
			handle = handleCached4AtomicLiteral( zTmp, el.substring( 1));
		}
		if (null != shash) {
			handle = handleCached4AtomicLiteral( zTmp, el.substring( 1), shash);
		}
		boolean precPunct = punct;
		punct = (((handle >>> (48 + 3)) | 0xe000) < QStrFunc.SHASH_NUM);
		if (!punct) {
			if (!precPunct) {
				out[count++] = NIL_HANDLE;
			}
		} else {
			if (precPunct && skipped) {
				out[count++] = QVal.asQVal( V_BLANK);
			}
		}
		skipped = false;
		out[count++] = handle;
	}
	return Arrays.copyOf( out, count);
}

/**
 * Split string into tokens and return their handles.
 * @param term
 * @return Token handles
 */
private static long[] handlesAtoms4String( QResult zTmp, String term) {
	String[] elements = QStrFunc.markedAtoms4String( term);
	return cacheAtoms4String( zTmp, elements, true);
}

private static long handleSeqAtoms4String( QResult zTmp, String term) {
	long[] atoms = handlesAtoms4String( zTmp, term);
	if (0 >= atoms.length) {
		return 0;
	} else if (1 == atoms.length) {
		return atoms[0];
	}
	return main.cacheAggreg( zTmp, (short) 0, atoms, null);
}

/** Split String into atoms and return the sequence as single QVal (handle).
 * Optional markers: SHASH_STRUC, SHASH_PUNCT, SHASH_TEMP_BITLIST, SHASH_TEMP_NUM_STRING.
 */
//qvalSeq4String
public static QVal qval4String( QResult zTmp, String term) {
	return QVal.asQVal( handleSeqAtoms4String( zTmp, term));
}

/** Split Strings (list of terms) into atoms and return as QVals (handles).
 * Optional markers: SHASH_STRUC, SHASH_PUNCT, SHASH_TEMP_BITLIST, SHASH_TEMP_NUM_STRING.
 */
//qvalSeqs4Strings
public static QVal[] qvals4Strings( QResult zTmp, String[] terms) {
	QVal[] out = new QVal[terms.length];
	for (int i0 = 0; i0 < out.length; ++i0) {
		out[i0] = qval4String( zTmp, terms[i0]);
	}
	return out;
}

/** Split String into atoms and return as QVals (handles).
 * Optional markers: SHASH_STRUC, SHASH_PUNCT, SHASH_TEMP_BITLIST, SHASH_TEMP_NUM_STRING.
 */
public static QVal[] qvalAtoms4String( QResult zTmp, String term) {
	return qvals4Handles( handlesAtoms4String( zTmp, term));
}

/** Cache atoms and full sequence.
 * Optional markers: SHASH_STRUC, SHASH_PUNCT, SHASH_TEMP_BITLIST, SHASH_TEMP_NUM_STRING.
 */
public static QVal[] qvals4AtomicLiterals( QResult zTmp, String[] atoms) {
	QVal[] out = new QVal[atoms.length];
	for (int i0 = 0; i0 < out.length; ++i0) {
		out[i0] = qval4AtomicLiteral( zTmp, atoms[i0]);
	}
	return out; //cacheSequence( zTmp, out);
}

//qval4..
//long[] out = cacheAtoms4String( zTmp, atoms, false);
//return QVal.asQVal( main.cacheAggreg( zTmp, (short) 0, out, null));

private static Object readItemOrFullAggreg( int hMap, long handle, int iItem) {
	if (0 == (QStrFunc.FLAG_INDEXED__1 & handle)) {
		return null;
	}
	Object v0 = aggregations[hMap].readStoredOrCachedValue( handle);
	if (null == v0) {
		return null;
	} else if (v0 instanceof long[]) {
		final long[] handles = (long[]) v0;
		if ((0 <= iItem) && (iItem < handles.length)) {
			if (QSEQ_ATOMS == (FLAGS_STRUCT_TYPE & handles[iItem])) {
				return main.readValue( handles[iItem]);
			}
			return aggregations[hMap].readValue( handles[iItem]);
		}
	} else if (!(v0 instanceof String)) {
		return v0;
	}
	return null;
}

private long[] readAggreg( long handle) {
	if (0 != (QStrFunc.FLAG_INDEXED__1 & handle)) {
		final Object o0 = readValue( handle);
		if (null == o0) {
			return null;
		} else if (o0 instanceof long[]) {
			return (long[]) o0;
		}
	}
	return new long[] { handle };
}

public static QVal[] qvals4Sequence( QVal handle) {
	return qvals4Handles( main.readAggreg( QVal.asQVal( handle)));
}

private static String string4Sequence( long handle, String... fillers) {
	final long[] handles = main.readAggreg( handle);
	return (null == handles) ? null : string4Atoms( handles, fillers);
}

public static String string4QVal( QVal val) {
	return string4Sequence( QVal.asQVal( val));
}

public static String string4QVals( QVal[] items, String... fillers) {
	return string4Atoms( handles4QVals( items), fillers);
}

public static String string4Literals( QVal[] literals, String... fillers) {
	final String innerFiller = (0 < fillers.length) ? fillers[0] : " ";
	final String outerFiller = (1 < fillers.length) ? fillers[1] : innerFiller;
	StringBuilder out = new StringBuilder( 256 + 128 * literals.length);
	boolean first = true;
	for (QVal litereral : literals) {
		if (!first) {
			out.append( outerFiller);
		}
		first = false;
		out.append( string4Sequence( QVal.asQVal( litereral), innerFiller));
	}
	return out.toString();
}

public static long[] readHandles4StoredAggreg( int hMap, long handle) {
	return aggregations[hMap].readAggreg( handle);
}

//public static QVal[] readStoredItems4Aggreg( int hMap, long handle) {
//	final long[] out = readHandles4StoredAggreg( hMap, handle);
//	return (null == out) ? null : qvals4Handles( out);
//}

public static QVal[] qvals4Aggreg( int hMap, long handle) {
	if (isVoidOrEmpty( handle)) {
		return NIL_SEQ;
	} else if (0 == (QStrFunc.FLAG_INDEXED__1 & handle)) {
		return new QVal[] { QVal.asQVal( handle) };
	}
	Object v0 = aggregations[hMap].readStoredOrCachedValue( handle);
	if (null == v0) {
		return NIL_SEQ;
	} else if (v0 instanceof long[]) {
		return qvals4Handles( v0);
	} else if (v0 instanceof String) {
		return new QVal[] { QVal.asQVal( handle) };
	}
	return NIL_SEQ;
}

private static String formatListElement( String str, long handle, boolean bHex) {
	if (isDate( handle)) {
		final String date = (0 < str.length()) ? str : MiscFunc.date4SlotSecond16Approx( slotSecond16oHandle( handle));
		return (!bHex) ? date : StringFunc.hex4Double( MiscFunc.slotSecond16oDateApprox( date) >> 16, null);
	} else if (!isNumeric( handle)) {
		return (!bHex) ? str : StringFunc.hexUtf8( str, true);
	}
	final double vx = doubleD4oHandleNum( handle);
	String num = StringFunc.string4DoubleD4( vx);
	return (!bHex) ? num : ((0 == vx) ? "0/F" : ((-1 * 1.0e4 == vx) ? "-1/T"
		: StringFunc.hex4Double( vx / 1.0e4, "/")));
}

private static String formatListPart( int hMap, long handle, String pre, String sep, String post, boolean bHex) {
	final Object v0 = ((0 > hMap) ? main : aggregations[hMap]).readValue( handle);
	if (null == v0) {
		return "";
	} else if (v0 instanceof long[]) {
		long[] a0 = (long[]) v0;
		if (0 >= a0.length) {
			return pre + post;
		} else {
			StringBuilder sb = new StringBuilder( 128 + 64 * a0.length);
			sb.append( pre);
			sb.append( formatListPart( ((QSEQ_ATOMS == (FLAGS_STRUCT_TYPE & a0[0])) ? -1 : hMap), a0[0], pre, sep, post, bHex));
			for (int i0 = 1; i0 < a0.length; ++i0) {
				sb.append( sep);
				sb.append( formatListPart( ((QSEQ_ATOMS == (FLAGS_STRUCT_TYPE & a0[i0])) ? -1 : hMap), a0[i0], pre, sep, post, bHex));
			}
			sb.append( post);
			return sb.toString();
		}
	} else if (!(v0 instanceof String)) {
		return bHex ? "'..'" : v0.toString();
	}
	return formatListElement( (String) v0, handle, bHex);
}

public static String formatList( int hMap, long[] handles, String pre, String sep, String post, boolean bHex) {
	if (0 >= handles.length) {
		return pre + post;
	}
	StringBuilder sb = new StringBuilder( 128 + (bHex ? 128 : 64) * handles.length);
	boolean hex = false;
	do {
		sb.append( pre);
		if ((0 > hMap) && !hex) {
			sb.append( QMapSto.string4Atoms( handles, sep));
		} else {
			sb.append( formatListPart( ((QSEQ_ATOMS == (FLAGS_STRUCT_TYPE & handles[0])) ? -1 : hMap),
				handles[0], pre, sep, post, hex));
			for (int i0 = 1; i0 < handles.length; ++i0) {
				sb.append( sep);
				sb.append( formatListPart( ((QSEQ_ATOMS == (FLAGS_STRUCT_TYPE & handles[i0])) ? -1 : hMap),
					handles[i0], pre, sep, post, hex));
			}
		}
		hex = !hex;
	} while (hex == bHex);
	sb.append( post);
	return sb.toString();
}

public static String formatList( QVal[] items, String pre, String sep, String post, boolean hex) {
	return formatList( -1, QMapSto.handles4QVals( items), pre, sep, post, hex && (20 >= items.length));
}

//public static String formatList( int hMap, long handle, String pre, String sep, String post, boolean bHex) {
//	final Object v0 = ((0 > hMap) ? main : aggregations[hMap]).readValue( handle);
//	if (null == v0) {
//		return "";
//	} else if (v0 instanceof long[]) {
//		return formatList( hMap, (long[]) v0, pre, sep, post, bHex);
//	} else if (!(v0 instanceof String)) {
//		return pre + v0.toString() + (bHex ? (post + pre + "'..'" + post) : post);
//	}
//	return pre + formatListElement( (String) v0, handle, false)
//		+ ((!bHex) ? "" : (post + pre + formatListElement( (String) v0, handle, true)))
//		+ post;
//}

public static synchronized long[] findStored( QResult zTmp, int hMap, int iKeyType, String key) {
	return aggregations[hMap].findHandles( zTmp, key, iKeyType);
}

public static synchronized void removeStored( int hMap, long handle) {
	aggregations[hMap].release( handle);
}

/**
 * Dump data as STRUCTs, without recognizing literals.
 * @param prevHandle Start with 0, then use long[length-1].
 * @param tag (or 0)
 * @param maxItems
 * @return
 */
public static long[] dump( QResult zTmp, int hMap, /*h*/long prevHandle, int maxItems) {
	//	assert null == dumpStruct_yRead[ 0 ] : "Recursion.";
	final QMapSto map = aggregations[hMap];
	int high = (int) (COMBINED_MASK & prevHandle) >>> BOX_SHIFT;
	int low = (int) (prevHandle & BOX_MASK);
	if ((high >= map.sizes.length) || (low >= map.sizes[high])) {
		return null;
	}
	final long[] out = new long[maxItems];
	low = (low | 1) + 2;
	int count = 0;
	for (; high < map.sizes.length; ++high) {
		for (; (low < map.sizes[high]) && (count < maxItems); low += 2) {
			if ((null == map.values[high][low]) || (0 >= map.refCount[high][low / 2])) {
				continue;
			}
			final long hx = map.handle4BoxElement( zTmp, high, low);
			if (0 != hx) {
				out[count++] = hx;
			}
		}
		low = 1;
	}
	if (count == maxItems) {
		return out;
	}
	return (0 < count) ? Arrays.copyOf( out, count) : null;
}

private long[] traverse4Aggreg( QResult zTmp, String xKey, int xiKeyType, int xiStep) {
	int inx = traverseTries( zTmp, null, 0, xKey, xiKeyType, xiStep, null);
	if (0 > inx) {
		return null;
	}
	Trie trie = (Trie) zTmp.object0;
	zTmp.object0 = null;
	if (null == trie) {
		return null;
	}
	Object el = trie.getElement( inx);
	if (el instanceof int[]) {
		int[] handles = (int[]) el;
		long[] out = new long[handles.length];
		int count = 0;
		for (int handle : handles) {
			if (0 == handle) {
				// Trailing 0.
				break;
			}
			final long hx = handle4BoxElement( zTmp, handle);
			if (0 != hx) {
				out[count++] = hx;
			}
		}
		return out;
	}
	return null;
}

public static long[] traverse( QResult zTmp, int hMap, String xKey, int xiKeyType) {
	return aggregations[hMap].traverse4Aggreg( zTmp, xKey, xiKeyType, 0);
}

//=====
}
