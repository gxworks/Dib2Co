// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.config;

import java.util.Locale;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;

public class Dib2Config implements Dib2Constants {
//=====

///// Fixed values:

public static final long TIME_MIN_2017_01_01 = 1483228800000L;
public static final long TIME_MAX = 2 * TIME_MIN_2017_01_01;

/** First one is default for encoding. */
public static TsvCodecIf[] codecs = new TsvCodecIf[] {
	CodecFunc.instance,
};

public static final Locale locale = null; // null for multilingual texts with CLDR (UCA DUCET) sorting

///// To be initialized:

public static char platformMarker = 0;
public static String moduleShort = "";
public static PlatformFuncIf platform;
public static UiFuncIf ui;

/** Referenced value (to be set in Main): true if processor does fast multiplication, compared to shifting and adding. */
public static boolean[] useFastMultiplication = new boolean[] { false };

/** Option: use linked list instead of array for TfPortBuffer etc. */
public static boolean useLinkedList = false;

public static String dbFileName = null;

/** General initialization here, specific values at caller. */
public static void init( char xPlatformMarker, String xModuleShort, String xDbFileName,
	PlatformFuncIf xPlatform, TsvCodecIf[] xmCsvCodecs) {
	moduleShort = xModuleShort;
	dbFileName = xDbFileName; //qMainContext.init( xPlatform );
	platform = xPlatform;
	codecs = (null == xmCsvCodecs) ? codecs : xmCsvCodecs;
	QMapSto.aggregations[CSVDB_MAP_INDEX] = new QMapSto( Mapping.kcKeyTypes);
	if (0 != platformMarker) {
		return;
	}
	platformMarker = xPlatformMarker;
	switch (platformMarker) {
	case '0': {
		// Assuming i686 or amd64 or similar processor:
		useFastMultiplication = new boolean[] { true };
		break;
	}
	default:
		;
	}
	MiscFunc.timeZoneDone = false;
	TcvCodec.instance.init( platformMarker);
}

/** Enable early logging as much as possible */
public static void log( String... aMsg) {
	//	MiscFunc.keepLog( aMsg[ 0 ], aMsg[ 1 ] );
	if (null != platform) {
		platform.log( aMsg);
	}
}

//=====
}
