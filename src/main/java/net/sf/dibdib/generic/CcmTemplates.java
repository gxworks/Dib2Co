// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

import net.sf.dibdib.thread_any.StringFunc;

public final class CcmTemplates extends QBaton {
//=====

/** 32 bytes each (with 0x00 as default):
 * (0) key, sizeHigh, sizeLow, Family, Style, Variant,
 * (6) Skew degrees, ZFraction percent, Weight tenths, (Width), 0, Decoration, 0, 0, 0, 0,
 * (16) ColorStrokeControl, ColorStrokeRed, ColorStrokeGreen, ColorStrokeBlue, 
 * ColorFillControl, ColorFillRed, ColorFillGreen, ColorFillBlue, 
 * ColorBackgroundControl, ColorBackgroundRed, ColorBackgroundGreen, ColorBackgroundBlue, 0, ...
 */
public static final int BYTES_PER_TEMPLATE = 32;

/////

public static final int FONT_INX_SIZE = 1;
public static final int FONT_INX_FAMILY = 3;
public static final int FONT_INX_STYLE = 4;
public static final int FONT_INX_VARIANT = 5;
public static final int FONT_INX_SKEW = 6;
public static final int FONT_INX_ZF = 7;
public static final int FONT_INX_WEIGHT = 8;
public static final int FONT_INX_DECORATION = 11;
public static final int FONT_INX_COLOR = 16;

public static final byte FAMILY_MONOSPACE = 0x10;
public static final byte FAMILY_SERIF = 0x20;
public static final byte FAMILY_SANS_SERIF = 0x30;
public static final byte FAMILY_CURSIVE = 0x40;
public static final byte FAMILY_FANTASY = 0x50;
public static final byte STYLE_ITALIC = 0x1;
public static final byte STYLE_CONDENSED = (byte) 0x80;
public static final byte VARIANT_SMALL_CAPS = 0x1;
public static final byte SKEW_OBLIQUE = 10;
public static final byte WEIGHT10_NORMAL = 40; // => 400
public static final byte WEIGHT10_BOLD = 70; // => 700

public static final byte LINE_UNDER = 0x1;
public static final byte LINE_OVER = 0x2;
public static final byte LINE_THROUGH = 0x4;
public static final byte DECO_BLINK = 0x1;
public static final byte DECO_SHADOW = 0x2;
public static final byte DECO_OUTLINE = 0x4;

public static final byte COLOR_EXTERNAL_VALUES = 0;
public static final byte COLOR_AS_ARGB = 1;
public static final byte COLOR_OPAQUE = 1;
public static final byte COLOR_TRANSPARENT = (byte) 0xff;
public static final byte COLOR_EXTERNAL_CONTRAST = 0x2;

/////

/** Cmp. sashat.me/2017/01/11/list-of-20-simple-distinct-colors,
 https://www.w3schools.com/colors/colors_xkcd.asp,
 https://simple.wikipedia.org/wiki/List_of_colors.
 */
public static enum Colors {
AMBER( 0xfeb308),
APRICOT( 0xFFB16D),
AZURE( 0x069af3),
BEIGE( 0xE6DAA6),
BLACK( 0x000000),
BLUE( 0x0343DF),

BLUE_COBALT( 0x030aa7),
NAVY20( 0x000075),

BLUEBLUE( 0x2242c7),
BLUE20( 0x4363D8),

BRONZE( 0xa87900),
BROWN( 0x653700),

BROWN86( 0x964B00),
BROWN20( 0x9A6324),

BROWN_DIRT( 0x836539),
CHARTREUSE( 0xc1f80a),
COPPER( 0xb66325),
CYAN( 0x00FFFF),

CYAN_BRIGHT( 0x41fdfe),
CYAN20( 0x42D4F4),

DANDELION( 0xFEDF08),
YELLOW20( 0xFFE119),

GOLD( 0xdbb40c),
GRAY( 0x929591),

GRAYISH( 0xA8A495),
GRAY20( 0xA9A9A9),

GREEN( 0x15b01a),
GREEN20( 0x3CB44B),

INDIGO( 0x380282),
IVORY( 0xffffcb),
LAVENDER( 0xC79FEF),
LEMON( 0xfdff52),

LILAC_PALE( 0xE4CBFF),
LAVENDER20( 0xE6BEFF),

LIME( 0xAAFF32),
LIME20( 0xBFEF45),

MAGENTA( 0xC20078),

MAGENTA_LIGHT( 0xFA5FF7),
MAGENTA20( 0xF032E6),

MAROON( 0x650021),
MINT( 0x9FFEB0),

MINT_LIGHT( 0xB6FFBB),
MINT20( 0xAAFFC3),

NAVY( 0x01153E),
NAVYBLUE( 0x001146),

OLIVE( 0x6E750E),
OLIVE20( 0x808000),

ORANGE( 0xF97306),

ORANGE_DUSTY( 0xF0833A),
ORANGE20( 0xF58231),

PALE( 0xfff9d0),
BEIGE20( 0xFFFAC8),

PEACH_LIGHT( 0xFFD8B1),
APRICOT20( 0xFFD8B1),

PEAR( 0xcbf85f),
PINK( 0xFF81C0),

PINK_PASTEL( 0xFFBACD),
PINK20( 0xFABEBE),

PURPLE( 0x7E1E9C),
PURPLE20( 0x911EB4),

RED( 0xE50000),

RED_DARK( 0x840000),
MAROON20( 0x800000),

RED_PINKISH( 0xF10C45),
RED20( 0xE6194B),

SALMON( 0xff796c),
SILVER( 0xc5c9c7),
TEAL( 0x029386),

TEAL_DUSTY( 0x4C9085),
TEAL20( 0x469990),

VIOLET( 0x9a0eea),
WHITE( 0xFFFFFF),
YELLOW( 0xFFFF14),

;

public final int rgb;
public final int argb;

private Colors( int rgb) {
	this.rgb = rgb;
	argb = rgb | 0xff000000;
}
};

public static final Colors[] kColorValsFg = new Colors[] {
	Colors.BLACK, Colors.BLUEBLUE, Colors.GRAYISH, // 100 %
	//. Colors.BLACK, Colors.BLUE20, Colors.GRAY20,
	Colors.ORANGE_DUSTY, Colors.RED_DARK, Colors.BLUE_COBALT, // >99 %
	//. Colors.ORANGE20, Colors.MAROON20, Colors.NAVY20,
	Colors.RED_PINKISH, Colors.GREEN, Colors.MAGENTA_LIGHT, Colors.TEAL_DUSTY, Colors.BROWN86, // 99 %
	//. Colors.RED20, Colors.GREEN20, Colors.MAGENTA20, Colors.TEAL20, Colors.BROWN20,
	Colors.PURPLE, Colors.OLIVE, // 95 %
	//. Colors.PURPLE20, Colors.OLIVE20,
	Colors.DANDELION, //,
	//. Colors.YELLOW20,
};

public static final Colors[] kColorValsBg = new Colors[] {
	Colors.WHITE, Colors.YELLOW, // 100 %
	//. Colors.WHITE, Colors.YELLOW20,
	Colors.PINK_PASTEL, Colors.LILAC_PALE, // >99 %
	//. Colors.PINK20, Colors.LAVENDER20,
	Colors.CYAN_BRIGHT, Colors.PALE, Colors.MINT_LIGHT, // 99 %
	//. Colors.CYAN20, Colors.BEIGE20, Colors.MINT20,
	Colors.LIME, Colors.PEACH_LIGHT, // 95 %
	//. Colors.LIME20, Colors.APRICOT20,
};

/////

public byte[] templatesFont;
public String[] templateNamesFont;

public CcmTemplates() {
}

public CcmTemplates( byte[] xmTemplatesFont, String[] xmTemplateNamesFont) {
	templateNamesFont = xmTemplateNamesFont;
	templatesFont = xmTemplatesFont;
}

public static final byte[] kDefaultTemplatesFont = {
	// ""
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	// BLUE
	'B', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 0, (byte) 0xff, 1, 0, (byte) 0xff, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	// GREEN
	'G', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, (byte) 0xaa, 0, 1, 0, (byte) 0xff, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	// RED
	'R', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, (byte) 0xee, 0, 0, 1, (byte) 0xff, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
};

public static final String[] kDefaultTemplateNamesFont = { "", "BLUE", "GREEN", "RED" };

public static CcmTemplates Dib2UiP_Templates_Default() {
	return new CcmTemplates( kDefaultTemplatesFont, kDefaultTemplateNamesFont);
}

@Override
public String toString() {
	StringBuilder out = new StringBuilder( 100);
	out.append( '\'');
	for (int i0 = 0; i0 < templatesFont.length; i0 += BYTES_PER_TEMPLATE) {
		out.append( (' ' < templatesFont[i0]) ? ("" + (char) templatesFont[i0]) : StringFunc.hex4Bytes(
			new byte[] { templatesFont[i0] }, false));
	}
	out.append( "':");
	for (int i0 = 0; i0 < templateNamesFont.length; ++i0) {
		out.append( templateNamesFont[i0]);
	}
	return out.toString();
}

//=====
}
