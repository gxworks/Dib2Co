// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

import java.io.File;

public interface PlatformFuncIf {
//=====

void log( String... aMsg);

void invalidate();

File getFilesDir( String... parameters);

String[] getLicense( String pre);

//=====
}
