// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

public interface UiFuncIf {
//=====

int getTextRow( int xYReal, int xRange);

int getTextColumn( int xXReal, String xText);

void setTouched4TextPos();

boolean pushClipboard( String label, String text);

String getClipboard();

void toast( String msg);

//=====
}
