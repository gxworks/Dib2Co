// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

import java.util.HashSet;

public interface TsvCodecIf {
//=====

byte[] init( char platform, Object... parameters);

byte[] compress( byte[] xyData, int xOffset4Reuse, int to);

byte[] decompress( byte[] xData, int length);

/** To take advantage of background processing ...
 * @param phrase Pass phrase.
 * @param headerInfo Null for creating key with xyIvData.
 * @param xyIvData Null for preparing a new key.
 * @return Key bytes.
 */
//byte[] prepareKey( byte[] phrase, Object headerInfo, byte[] xyIvData );

byte[] getKey( byte[] phrase, byte[] accessCode, byte[] salt, int iterations);

String getKeyInfo( HashSet<String> entries);

byte[] encodePhrase( byte[] phrase, byte[] accessCode);

byte[] decodePhrase( byte[] encoded, byte[] accessCode);

/** Header bytes: 0..1 magic bytes, 2 cipher/format, 3 encoder, 4.. counts etc. 
 */
byte[] encode( byte[] compressedData, int from, int to, byte[] key, byte[] iv16, int headerTagsOrKeyInfo, byte[] header,
	byte[] signatureKey)
	throws Exception;

// Header bytes: 0..1 magic bytes, 2 version/format, 3 encoder, 4.. counts etc. */
//byte[] encode4DerivedKey( byte[] compressedData, int from, int to, byte[] pass ) throws Exception;

byte[] decode( byte[] data, int from, int to, byte[] key, byte[] signatureKey) throws Exception;

byte[] getInitialValue( int len);

byte getMethodTag();

//StringBuffer getLog();

//=====
}
