// Copyright (C) 2018,2019 Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

//TODO
public final class CcmTextLines extends QBaton {
//=====

public String[] lines = { "", "Welcome.", "", "ABSOLUTELY NO WARRANTY.", "See license." };

public int count = lines.length;
public int textLineInx = 0; // 2
public int textLineCharInx = -1;

public void posHome() {
	textLineInx = 0;
	textLineCharInx = -1;
}

//=====
}
