// Copyright (C) 2016,2017,2018,2019 Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

import java.util.*;
import net.sf.dibdib.config.Dib2Constants;
//import net.sf.dibdib.generic.Mapping.Fields;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_any.QMapSto.QVal;

/** Conceptual element/ Convenience class for QMap/ ...:
 * Element (record) of categorical database (currently saved as CSV line), also used as Token.
 * Contains:
 * name (atomic) + propositional main category (+ secondary categories)
 * + time stamp + source of information
 * + (propositional) data.
 * (Constraint logic, name as partial ID)
 * Prolog interpretation: (CATEGORY ATOM), (CATEGORY.DATA_LABEL ATOM DATA_ENTRY).
 */
public class Mapping extends QBaton {
//=====

public static final String fieldNames = //,
	"" // OID
		+ "\tNAME" // LABEL
		+ "\tCATS" //,
		+ "\tTIME" //,
		+ "\tCONTRIB" //,
		+ "\tSRCS" //,
		+ "\tRECV" //,
		+ "\tDAT1" //,
		+ "\tDAT2" //,
		+ "\tDAT3" //,
		+ "\tDAT4" //,
		+ "\tDAT5" //,
		+ "\tDAT6" //,
		+ "\tDAT7" //,
		+ "\tDAT8" //,
		+ "\tDAT9";

public static enum Fields {
// OID is key, LABEL is first for shash, then CATS, then DAT:
LABEL, CATS, DAT_0, // DAT is last for CSV line
TIME_STAMP, CONTRIB, SRCS, RECV,
DAT_X, // DAT_1, DAT_2 etc.
}

// shash + OID as keys:
public static final int kcKeyTypes = 2;

public static enum Pack {
NAME, CATS, CONTRIB, SRCS, RECV, DAT, Q_COUNT,
};

public static final QVal qRootDummyAddress = QMapSto.qval4AsciiShort( "0@0");

/** Main OID, to be used for PREF "." and in combination with qRootAddress for main CONTACT.
 * Bootstrapped: generated OID for dummy address, OID for PREF "email_address",
 * OID in combination with qRootAddress from CONTACT. Change cautiously: some Mapping
 * objects have this value for contributor or refer to this with contributor == "".  */
public static QVal qhRootOid = qRootDummyAddress; //QVal.makeAtomic( MiscFunc.createId( qRootDummyAddress ) );

/** To be used in combination with qhRootOid: target value for PREF "." and "email_address". */
public static String qRootAddress = null;

/** Object ID (OID) for import, export, and versioning:
 * locally unique, eventual consistency,
 * (short OIDs for temporary mappings (e.g. 'X', 'Y', ...) ) */
public QVal oid;

///** Informative: msec delta for time zone info. */
//public int timeStampDeltaZone;

/** Required number of data elements for packing. */
public static final int PACK_ADD = 3;

///// -- Immutable - 'as if' (whenever not enforced by Java)

/** 'Name': sememe/ name of (quasi-atomic) element, e.g. 'Charlie Brown' or 'Brown.Charlie' */
public final QVal uLabel;

/** All category names */
public final QVal[] auCategories;

/** Flags for pre-defined categories */
public final long bCategories;

// Nickname not stored here.
// Time stamp below.

/** OID of contributor, "" iff it is the user/ root */
public final QVal uContributor;

public final QVal[] atSources;
public final QVal[] auShareReceivers;
/** Use hex strings for PREF values! */
public final QVal[] atDataElements;

///// -- Mutable (for ease of Baton handling etc.)

// (parent: public String oid, public long timeStamp)
// ...

//=====

/** Pre-defined categories */
public static enum Cats {
//=====

///// Singular
PREF( 1),
VAR( 2),

///// Special
OTHERS( 1L << 8),
TRASH( 1L << 9),
DONE( 1L << 10),

/////
NOTE( 1L << 16),
EVENT( 1L << 17),
CONTACT( 1L << 18),
GROUP( 1L << 19),
MSG( 1L << 20),
//
_MAX( 1L << 21),
//
;

public final long flag;
public final QVal uName;

public static final Cats DEFAULT = NOTE;

private static HashMap<Long, Cats> map = null;

private Cats( long xFlag) {
	flag = xFlag;
	uName = QMapSto.qval4AsciiShort( name());
}

public static Cats valueOf( QVal name) {
	if (null == map) {
		HashMap<Long, Cats> tmp = new HashMap<Long, Cats>();
		for (Cats cat : Cats.values()) {
			tmp.put( QVal.asQVal( cat.uName), cat);
		}
		map = tmp;
	}
	return map.get( QVal.asQVal( name));
}

public static String cats4Flags( long flags) {
	StringBuilder out = new StringBuilder( 30);
	int count = 0;
	for (Cats cat : Cats.values()) {
		if ((cat.flag & flags) != 0) {
			out.append( cat.name());
		}
	}
	if (0 == count) {
		out.append( OTHERS.name());
	}
	return out.toString();
}

public static QVal[] list4Flags( long flags) {
	QVal[] out = new QVal[32];
	int count = 0;
	for (Cats cat : Cats.values()) {
		if ((cat.flag & flags) != 0) {
			out[count++] = QMapSto.qval4AsciiShort( cat.name());
		}
	}
	if (0 == count) {
		out[count++] = QMapSto.qval4AsciiShort( OTHERS.name());
	}
	return Arrays.copyOf( out, count);
}

public static long toFlags( QVal[] cats) {
	long out = 0;
	if (null == cats) {
		return 0;
	}
	if (null == map) {
		// Populate map:
		valueOf( QMapSto.NIL);
	}
	for (QVal nam : cats) {
		Cats cat = Cats.valueOf( nam);
		if (null == cat) {
			out |= OTHERS.flag;
		} else {
			out |= cat.flag;
		}
	}
	return out;
}

////=====
}

//=====

/**
 * iOid -1 for old CSV entries => label,cats,ctrb,time,dat[]
 * otherwise                   => oid,label,cats,time,ctrb,srcs,,dat[]
 */
public Mapping( String[] csvFields, int iOid, int flagsMarkAdjust4Time) {
	final QResult pooled = QResult.get8Pool();
	long time = (6 > csvFields[3].length())
		? MiscFunc.currentTimeMillisLinearized() : MiscFunc.millis4Date( csvFields[3]);
	if (0 != (flagsMarkAdjust4Time & 1)) {
		time = time & ~Dib2Constants.TIME_SHIFTED;
	}
	long min = (0 != (flagsMarkAdjust4Time & 2)) ? 1 : time;
	timeStamp = MiscFunc.alignTime( time, min);

	auCategories = QMapSto.qvalAtoms4String( pooled, csvFields[2 + iOid]); //, -1, cats );
	bCategories = Cats.toFlags( auCategories);
	String label = StringFunc.nameNormalize( csvFields[1 + iOid], 0xffff);
	//	label = (0 == (Cats.MSG.flag & bCategories)) ? label //,
	//		: (label + '*' + MiscFunc.dateShort4Millis( timeStamp ));
	uLabel = QMapSto.qval4AtomicLiteral( pooled, label);
	oid = QMapSto.qval4AtomicLiteral( pooled, ((0 <= iOid) && (10 <= csvFields[iOid].length())) ? csvFields[iOid]
		: MiscFunc.createId( label, timeStamp));
	//bVersioned = ...;

	// As OID:
	String ctrb = csvFields[4 + 2 * iOid];
	if ((null == ctrb) || QMapSto.equalValues( QVal.asQVal( qhRootOid), ctrb)) { //(QMap.main.readStrings( "N.N.", qhRootOid )[ 0 ].equals( ctrb )) {
		ctrb = "";
	} else if (10 > ctrb.length()) {
		ctrb = "";
	}
	uContributor = QMapSto.qval4AtomicLiteral( pooled, ctrb);
	atDataElements = QMapSto.qvals4Strings( pooled, //null, -1, 
		((7 + 3 * iOid) >= csvFields.length) ? new String[0] //,
			: Arrays.copyOfRange( csvFields, 7 + 3 * iOid, csvFields.length));
	String srcs = (5 >= csvFields.length) || (null == csvFields[5]) || (0 >= csvFields[5].length()) ? "" : csvFields[5];
	atSources = //(0 < srcs.length()) ? QMapSto.qvals4Strings( pooled, //null, -1, 
		QMapSto.qvals4AtomicLiterals( pooled, srcs.split( " // "));
	//: QMapSto.NIL;
	auShareReceivers = QMapSto.NIL_SEQ;
}

public Mapping( QVal xOid, QVal[] fields, QVal... optionalData) {
	oid = xOid; //fields[ Fields.OID.ordinal() ].toString();
	//	OID, LABEL, CATS, TIME, CONTRIB, SRCS, RECV, DAT,
	// LABEL, CATS, DAT_0, TIME_MSEC, CONTRIB, SRCS, RECV, DAT_X
	final int len = fields.length;
	uLabel = fields[Fields.LABEL.ordinal()];
	auCategories =
		//QMapSto.qvals4Aggreg( Dib2Constants.CSVDB_MAP_INDEX, QVal.asQVal( fields[Fields.CATS.ordinal()]));
		QMapSto.qvals4Sequence( fields[Fields.CATS.ordinal()]);
	bCategories = Cats.toFlags( auCategories);
	timeStamp = ((long) QMapSto.doubleD4oQVal( fields[Fields.TIME_STAMP.ordinal()])) / 10;//Fields.TIME_MSEC.ordinal()])) / 10;
	uContributor = (6 >= len) ? QMapSto.NIL : fields[Fields.CONTRIB.ordinal()];
//		(fields.length <= Fields.CONTRIB.ordinal()) ? QMapSto.NIL
//		: fields[Fields.CONTRIB.ordinal()];
	atSources = (6 >= len) ? null
//		(fields.length <= Fields.SRCS.ordinal()) ? null
//		: QMapSto.qvals4Sequence( fields[Fields.SRCS.ordinal()]);
		: QMapSto.qvals4Sequence( fields[Fields.SRCS.ordinal()]);
	auShareReceivers = (6 >= len) ? null
//		(fields.length <= Fields.RECV.ordinal()) ? null
//		: QMapSto.qvals4Sequence( fields[Fields.RECV.ordinal()]);
		: QMapSto.qvals4Sequence( fields[Fields.RECV.ordinal()]);
	atDataElements =
		(0 != optionalData.length) ? optionalData
			//		: QMapSto.qvals4Aggreg( Dib2Constants.CSVDB_MAP_INDEX, QVal.asQVal( fields[Fields.DAT.ordinal()]));
			: Arrays.copyOfRange( fields, Fields.DAT_X.ordinal() - 1, len);
	atDataElements[0] = (0 != optionalData.length) ? optionalData[0]
		: fields[Fields.DAT_0.ordinal()];
}

public QVal[] toQVals( boolean partialData) {
	final QResult pooled = QResult.get8Pool();
	// LABEL, CATS, DAT_0, TIME_MSEC, CONTRIB, SRCS, RECV, DATX
	QVal[] out = new QVal[Fields.DAT_X.ordinal() - 1
		+ ((partialData || (0 >= atDataElements.length)) ? 1 : atDataElements.length)];
	out[Fields.LABEL.ordinal()] = uLabel;
	out[Fields.CATS.ordinal()] = QMapSto.cacheSequence( pooled, auCategories);
	out[Fields.RECV.ordinal()] = QMapSto.cacheSequence( pooled, auShareReceivers);
	out[Fields.SRCS.ordinal()] = QMapSto.cacheSequence( pooled, atSources);
	out[Fields.CONTRIB.ordinal()] = uContributor;
	out[Fields.TIME_STAMP.ordinal()] = QMapSto.qval4DoubleD4( timeStamp * 10.0);
	out[Fields.DAT_0.ordinal()] = (0 >= atDataElements.length) ? QMapSto.NIL : atDataElements[0];
	if (!partialData) {
		for (int i0 = 1; i0 < atDataElements.length; ++i0) {
			out[i0 - 1 + Fields.DAT_X.ordinal()] = atDataElements[i0];
		}
	}
	return out;
}

/** Create simple Mapping based on String values.
 * @param label Name or label.
 * @param cat Category (default: DONE).
 * @param ctrbOid "" for main user/ root.
 * @param data Mapped data
 * @return Created Mapping object.
 */
public static Mapping make( String label, Cats cat, String ctrbOid, String data) {
	String oid = MiscFunc.createId( ".".equals( label) ? data : label);
	cat = (null == cat) ? Cats.DONE : cat;
	String time = MiscFunc.date4Millis( false);
	String[] a0 = new String[] { oid, label, cat.name(), time, ctrbOid, "", "", data };
	return new Mapping( a0, 0, 0);
}

public static Mapping make( String label, String cats, String date, int offsetData, String... data) {
	String[] a0 = new String[7 + data.length - offsetData];
	a0[0] = MiscFunc.createId( ".".equals( label) ? data[0] : label);
	a0[1] = label;
	a0[2] = cats;
	a0[3] = (null == date) ? "" : date; //((null == date) || (6 > date.length())) ? MiscFunc.date4Millis() : date;
	a0[4] = "";
	a0[5] = "";
	a0[6] = "";
	for (int i0 = 7; i0 < a0.length; ++i0) {
		a0[i0] = data[i0 - 7 + offsetData];
	}
	return new Mapping( a0, 0, 0);
}

@Override
public String toString() {
	return QMapSto.string4QVal( uLabel) + '@' + MiscFunc.dateShort4Millis( timeStamp) //,
		+ '[' + QMapSto.string4QVals( auCategories) + "]:"
		// "]: " 
		+ ((0 >= atDataElements.length) ? "" : (QMapSto.string4QVal( atDataElements[0])))
		+ ((1 >= atDataElements.length) ? "" : "...");
}

public String toTextLine( int maxLen) {
	StringBuilder out = new StringBuilder( 64 * atDataElements.length);
	if (0 >= maxLen) {
		out.append( MiscFunc.dateShort4Millis( timeStamp).substring( 0, 6));
		out.append( ' ');
	}
	out.append( QMapSto.string4QVal( uLabel));
	String data = QMapSto.string4Literals( atDataElements, " ", "\t");
	out.append( '\t').append( (20 < data.length()) ? (data.substring( 0, 17) + "...") : data);
	if (0 >= maxLen) {
		out.append( "\t[");
		out.append( oid);
		out.append( ']');
		out.append( ' ').append( QMapSto.string4QVals( auCategories));
	}
	out.append( data);
	return ((0 >= maxLen) || (maxLen >= out.length())) ? out.toString() : out.substring( 0, maxLen);
}

public String getContributorOid( String xRootOid) {
	if (QMapSto.NIL == uContributor) {
		return QMapSto.string4QVal( qhRootOid);
	}
	return QMapSto.string4QVal( uContributor);
}

public static String toCsvLine( String oid, String label, long timeStamp, String cat, String dat) {
	StringBuilder out = new StringBuilder( 100 + dat.length());
	out.append( oid);
	String time = MiscFunc.date4Millis( false, timeStamp);
	out.append( '\t').append( (null == label) ? time : label);
	out.append( '\t').append( cat);
	out.append( '\t').append( time);
	out.append( '\t').append( ""); // contributorOid
	out.append( '\t').append( ""); // sources
	out.append( '\t').append( ""); // shareReceivers, " " ) );
	out.append( '\t').append( dat.replace( '\n', '\t'));
	return out.toString();
}

/**
 * @param xUserOid null for root user (qhRootOid).
 * @return
 */
public String toCsvLine( String xUserOid) {
	StringBuilder out = new StringBuilder( 100 + 10 * atDataElements.length);
	out.append( QMapSto.string4QVal( oid));
	String label = QMapSto.string4QVal( uLabel);
	label = (0 < label.indexOf( '*')) ? label.substring( 0, label.indexOf( '*')) : label;
	out.append( '\t').append( label);
	out.append( '\t').append( QMapSto.string4QVals( auCategories));
	out.append( '\t').append( MiscFunc.date4Millis( false, timeStamp));
	out.append( '\t').append( getContributorOid( xUserOid));
	out.append( '\t').append( QMapSto.string4Literals( atSources, " ", " // "));
	out.append( '\t').append( QMapSto.string4QVals( auShareReceivers));
	out.append( '\t').append( QMapSto.string4Literals( atDataElements, " ", "\t").replace( '\n', '\t'));
	return out.toString();
}

public Mapping clone4Cats( QVal cats) {
	QVal[] fields = toQVals( false);
	fields[1] = cats;
	return new Mapping( oid, fields);
}

public Mapping clone4Data( QVal[] dataElements) {
	//	final QResult pooled = QResult.get8Pool();
	QVal[] fields = toQVals( true);
	//fields[Fields.DAT.ordinal()] = 0; //QMap.cacheAggreg4QVals( pooled, QMap.LIST, Dib2Constants.CSVDB_MAP_INDEX, dataElements );
	return new Mapping( oid, fields, dataElements);
}

//=====
}
