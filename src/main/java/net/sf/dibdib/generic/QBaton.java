// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

import net.sf.dibdib.thread_any.MiscFunc;

/** For token flow/ relay model, to be extended (cmp. QDspPrcs and QPlace).
 * Based on Petri net (Arc weight = 1) with typed/ guarded Places for data tokens (QBatons).
 * (Places impose guards on their preceding Transitions).
 * Basic functions also work with other Objects.
 */
public class QBaton {
//=====

/** msec value (0 if trashed), lowest 2 bits may be adjusted according to 'TIME_SHIFTED'. */
public long timeStamp;

/////

public static final QBaton DUMMY = new QBaton();
public static final QBaton EMPTY = new QBaton();
public static final QBaton PENDING = new QBaton();

/////

public void setTimeStamp() {
	timeStamp = MiscFunc.currentTimeMillisLinearized();
}

//=====
}
