// Copyright (C) 2016,2017,2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

public interface ContextIf_OLD extends PlatformFuncIf {
//=====

byte[] get( String prefKey, byte[] defaultValue);

/**
 * @param prefKey
/** @param value (use null for removing)
 */
void set( String prefKey, byte[] value);

/**
 * @param prefKey
/** @param defaultValue (may start with "X'" as marker)
/** @return hex literal (with or without marker, depending on above)
 */
String getHex( String prefKey, String defaultValueHex);

void setHex( String prefKey, String hexLiteral);

String getLiteral( String prefKey, String defaultValue);

void setLiteral( String prefKey, String value);

void remove( String pref);

//public void pref2db();
//
//public void db2pref();

//=====
}
