// Copyright (C) 2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.generic;

public final class QFrame {
//=====

/*
public String delimPre;
public String expression;
public QFrame[] seq;
public QVal val;
public String repr;
*/

public String minScript;
public QFrame[] seq;
public long hTextTemplate;
public long[] ahText;

//=====
}
