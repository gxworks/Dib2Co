// Copyright (C) 2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_wk;

import net.sf.dibdib.generic.*;

final class CcmSource2ExecDsp implements QDspPrcsIf {
//=====

///// Threaded (in/ out/ trigger)

final QPlace wxSource = new QPlace();
private QPlace rCmds;

/////

@Override
public boolean init( QPlace... xrOutgoing) {
	rCmds = xrOutgoing[0];
	return true;
}

@Override
public QBaton peek( long... xbOptFlags) {
	return wxSource.peek();
}

QBaton step() {
	QBaton out = wxSource.pull();
	if (null != out) {
		rCmds.push( out);
	}
	return out;
}

//=====
}
