// Copyright (C) 2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_wk;

import net.sf.dibdib.generic.*;

final class CcmSink4ExternalDsp implements QDspPrcsIf {
//=====

///// Threaded (in/ out/ trigger)

final QPlace wxResult = new QPlace();
private QPlace rGateOut;

/////

@Override
public boolean init( QPlace... xrOutgoing) {
	rGateOut = xrOutgoing[0];
	return true;
}

@Override
public QBaton peek( long... xbOptFlags) {
	return wxResult.peek();
}

QBaton step() {
	QBaton out = wxResult.pull();
	if (null != out) {
		rGateOut.push( out);
	}
	return out; //(null == out) ? QBaton.EMPTY : out;
}

//=====
}
