// Copyright (C) 2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_wk;

import net.sf.dibdib.generic.*;

final class CmdOrResult2SlowOrSinkDsp implements QDspPrcsIf {
//=====

///// Threaded (in/ out/ trigger)

final QPlace wxCommandsNResults = new QPlace();
private QPlace rSlow;
private QPlace rSink;

/////

@Override
public boolean init( QPlace... xr_Command_Result) {
	rSlow = xr_Command_Result[0];
	rSink = xr_Command_Result[1];
	return true;
}

@Override
public QBaton peek( long... xbOptFlags) {
	return wxCommandsNResults.peek();
}

QBaton step() {
	QBaton out = wxCommandsNResults.pull();
	if (null != out) {
		if (out instanceof CommandBaton) {
			rSlow.push( out);
		} else {
			rSink.push( out);
		}
	}
	return out;
}

//=====
}
