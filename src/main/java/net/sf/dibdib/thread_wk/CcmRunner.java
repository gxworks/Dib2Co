// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_wk;

import static net.sf.dibdib.thread_ui.UiDataSto.*;

import java.io.File;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_any.QMapSto.QVal;
import net.sf.dibdib.thread_ui.*;
import net.sf.dibdib.thread_x.ExecSlowPrcs;

/** For platform's worker thread (CCM's QNet). */
public enum CcmRunner implements Runnable {
//=====

INSTANCE;

CcmSource2ExecDsp zSourceDsp = new CcmSource2ExecDsp();
CcmSink4ExternalDsp zSinkDsp = new CcmSink4ExternalDsp();

///// Threaded (in/ out/ trigger)

public final QPlace wxGateIn = zSourceDsp.wxSource; //CcmSource2ExecDsp qGateIn = null;
private QPlace rGateOut; //public CcmSink4ExternalDsp qGateOut = null;

public static volatile boolean wxSave = false;
public static volatile Thread qActive = null;
public static volatile boolean qBusy = false;

/////

private static long zLastSave = 0;
private static long zLastBackup = 0;

ExecSlowPrcs qProcessSlow = null;
ExecuteCmdPrcs qProcessFast = null;
CmdOrResult2SlowOrSinkDsp qDispFast2Slow = null;

public boolean init( QPlace... xrOutgoing) {
	//public void init( boolean createGateways, boolean createNet ) {
	//	if (createGateways) {
	//		qGateIn = new CcmSource2ExecDsp();
	//		qGateOut = new CcmSink4ExternalDsp();
	//	}
	//	if (createNet) {
	rGateOut = xrOutgoing[0];
	qProcessFast = new ExecuteCmdPrcs();
	qProcessSlow = new ExecSlowPrcs();
	qDispFast2Slow = new CmdOrResult2SlowOrSinkDsp();
	//		qDispSlow2Sink = new ExecSlow2Sink();
	zSourceDsp.init( qProcessFast.wxCommands);
	qProcessFast.init( qDispFast2Slow.wxCommandsNResults);
	qDispFast2Slow.init( qProcessSlow.wxCommands, rGateOut);
	qProcessSlow.init( rGateOut);
	return true;
}

public boolean isBusy() {
	return ((null != qActive) && qBusy) || (null != zSourceDsp.peek());
}

private static String getShortDay() {
	return MiscFunc.dateShort4Millis( MiscFunc.currentTimeMillisLinearized()).substring( 0, 6);
}

private static String findLatest( File dir) {
	File file = null;
	String[] names = dir.list();
	if (null != names) {
		String name = null;
		long time = -1;
		for (String x0 : names) {
			if (x0.matches( Dib2Config.dbFileName.replace( ".", ".*"))
				|| (x0.startsWith( Dib2Config.dbFileName)
					&& (x0.endsWith( ".bak") || x0.endsWith( ".old")))) {
				long x1 = new File( dir, x0).lastModified();
				if (time < x1) {
					time = x1;
					name = x0;
				}
			}
		}
		if (0 < time) {
			file = new File( dir, name);
		}
	}
	return (null == file) ? null : file.getAbsolutePath();
}

public static String check4Load() {
	File dir = Dib2Config.platform.getFilesDir( "safe");
	String path = null;
	if (null != dir) {
		File file = new File( dir, Dib2Config.dbFileName);
		path = file.isFile() ? file.getAbsolutePath() : null;
		if (null != path) {
			return path;
		}
		path = CcmRunner.findLatest( dir);
		if (null != path) {
			return path;
		}
	}
	dir = Dib2Config.platform.getFilesDir( "external");
	if ((null != dir) && (dir.exists())) {
		path = CcmRunner.findLatest( dir);
	}
	return path;
}

public static boolean initLoad( boolean force) {
	boolean done = false;
	if (!force && CcmSto.instance.isInitialized()) {
		UiDataSto.pathInitDataFile = null;
		UiDataSto.qSwitches[UiDataSto.SWI_STEP_OR_ACCESS_MINUTE] = 999;
		return true;
	}
	// Check if proper phrase is available:
	final boolean dummy = !TcvCodec.instance.setHexPhrase( "");
	final byte[] pass = TcvCodec.instance.getPassFull();
	//	if ((100 > qSwitches[ UiSwitches.SWI_STEP_OR_ACCESS_MINUTE ]) && !CsvCodec0.setDummyPhrase( false )) {
	if (null != pass) {
		File dir = Dib2Config.platform.getFilesDir( "safe");
		File file = new File( dir, Dib2Config.dbFileName);
		String path = file.isFile() ? file.getAbsolutePath() : null;
		if (null == path) {
			path = CcmRunner.findLatest( dir);
		}
		if (null != path) {
			done = (0 <= CcmSto.instance.load( path));
			if (done || (dummy && new File( path).exists())) {
				// Do not pull old data if password had been set.
			} else if (new File( path + ".bak").isFile()) {
				done = done || (0 <= CcmSto.instance.load( path + ".bak"));
				done = done || (0 <= CcmSto.instance.load( path + ".old"));
			}
		} else {
			dir = Dib2Config.platform.getFilesDir( "external");
			if ((null != dir) && (dir.exists())) {
				path = CcmRunner.findLatest( dir);
				if (null != path) {
					done = done || (0 <= CcmSto.instance.load( new File( path).getAbsolutePath()));
				}
			}
		}
	}
	if (done) {
		UiDataSto.pathInitDataFile = null;
		UiDataSto.qSwitches[UiDataSto.SWI_STEP_OR_ACCESS_MINUTE] = 999;
		if (!dummy) {
			File downloadFolder = Dib2Config.platform.getFilesDir( "external");
			if (null != downloadFolder) {
				String name = CcmRunner.getShortDay().substring( 4, 6);
				name = Dib2Config.dbFileName.replace( ".dm", ".bak.dm");
				zLastBackup = 1 + new File( downloadFolder, name).lastModified();
			}
		}
	} else if (200 > UiDataSto.qSwitches[UiDataSto.SWI_STEP_OR_ACCESS_MINUTE]) {
		UiDataSto.qSwitches[UiDataSto.SWI_STEP_OR_ACCESS_MINUTE] = 200;
	}
	return done;
}

public void addSamples() {
	CcmSto.instance.stackPush( QMapSto.qval4DoubleD4( 3.0 * 1.0e4));
	CcmSto.instance.stackPush( QMapSto.qval4DoubleD4( 2.0 * 1.0e4));
}

public void pushResult( QVal[][] vals, int pos) {
	if ((null == vals) || (0 >= vals.length) || (null == vals[0])) {
		return;
	}
	CcmSto.instance.stackInsert( pos, vals);
}

private synchronized void queueMe() {
	if (null != qActive) {
		Thread.yield();
		for (int i0 = 0; (i0 <= 7) && (null != qActive); ++i0) {
			if (7 == i0) {
				qActive.interrupt();
			}
			try {
				Thread.sleep( 70);
			} catch (InterruptedException e) {
			}
		}
	}
	qActive = Thread.currentThread();
}

public static synchronized void saveAll( long bFlags_toDownloads_asBak) {
	if (0 == bFlags_toDownloads_asBak) {
		TcvCodec.instance.writePhrase();
	}
	File file = new File( Dib2Config.platform.getFilesDir( "safe"), Dib2Config.dbFileName);
	String path = file.getAbsolutePath();
	if (file.exists()) {
		File old = new File( path + ((0 != bFlags_toDownloads_asBak) ? ".old" : ".bak"));
		if (old.exists()) {
			old.delete();
		}
		file.renameTo( old);
	}
	CcmSto.instance.save( path, true);
	zLastSave = MiscFunc.currentTimeMillisLinearized();
	if (0 != (1 & bFlags_toDownloads_asBak)) {
		File downloadFolder = Dib2Config.platform.getFilesDir( "external");
		if (null != downloadFolder) {
			String name = CcmRunner.getShortDay().substring( 4, 6);
			name = Dib2Config.dbFileName.replace( ".dm", "."
				+ ((0 != (2 & bFlags_toDownloads_asBak)) ? "bak" : name)
				+ ".dm");
			file = new File( downloadFolder, name);
			if ((3 != bFlags_toDownloads_asBak)
				|| ((file.lastModified() + 2 * 3600 * 1000) < MiscFunc.currentTimeMillisLinearized())) {
				path = file.getAbsolutePath();
				CcmSto.instance.save( path, true);
				zLastBackup = zLastSave;
			}
		}
	}
}

@Override
public void run() {
	queueMe();
	//	Log.d( "run", "start " + QResult.getThreadIndex() ); // + Dib2UiP.INSTANCE.wPageLines[ 0 ] );
	QResult.getThreadIndex();
	if (1000 > qSwitches[SWI_STEP_OR_ACCESS_MINUTE]) {
		wxGateIn.flush( false);
		if (null != UiDataSto.pathInitDataFile) { // && !CsvDb.instance.isInitialized()) { // (0 > qSwitches[ SWI_STEP_OR_ACCESS_MINUTE ]) {
			try {
				qBusy = true;
				Dib2Config.platform.invalidate();
				if (initLoad( false)) {
					qSwitches[UiDataSto.SWI_STEP_OR_ACCESS_MINUTE] = 990;
				} else if (200 > qSwitches[UiDataSto.SWI_STEP_OR_ACCESS_MINUTE]) {
					qSwitches[UiDataSto.SWI_STEP_OR_ACCESS_MINUTE] = 200;
				}
			} catch (Exception e) {
				Dib2Config.log( "CcmRunner", "exception " + e);
			}
		}
	} else {
		boolean exceptional = false;
		while (!CalcPres.wxExitRequest) {
			try {
				boolean active = false;
				active = (null != zSourceDsp.step()) || active;
				active = (null != qProcessFast.step()) || active;
				active = (null != qDispFast2Slow.step()) || active;
				//TODO Create its own thread ...
				//				active = (null != qProcessSlow.step()) || active;
				active = (null != zSinkDsp.step()) || active;
				if (!active) {
					break;
				}
				qBusy = true;
				// Show progress (qActive != null !):
				Dib2Config.platform.invalidate();
				Thread.yield();
			} catch (Exception e) {
				Dib2Config.log( "CcmRunner", "exception " + e);
				e.printStackTrace();
				if (exceptional) {
					// Consecutive error ...?
					break;
				}
				exceptional = true;
			}
		}
	}
	//	Log.d( "run", "end " + QResult.getThreadIndex() ); // + Dib2UiP.INSTANCE.wPageLines[ 0 ] );
	qActive = null;
	qBusy = false;
	QResult.drop8Pool();
	// Final refresh
	Dib2Config.platform.invalidate();
	if (1000 < qSwitches[UiDataSto.SWI_STEP_OR_ACCESS_MINUTE]) {
		if (wxSave) {
			wxSave = false;
			CcmRunner.saveAll( 0);
		} else if (((zLastBackup + Dib2Constants.MAX_DELTA_ACCESS_CHECK) < MiscFunc.currentTimeMillisLinearized())
			&& (0 < zLastBackup)) {
			if (zLastSave > 1000) {
				CcmRunner.saveAll( 3);
			}
		}
	}
}

//=====
}
