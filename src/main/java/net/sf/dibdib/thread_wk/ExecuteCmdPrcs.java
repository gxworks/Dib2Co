// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_wk;

import java.io.File;
import java.util.Arrays;
import net.sf.dibdib.config.*;
import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_any.QMapSto.QVal;
import net.sf.dibdib.thread_ui.UiDataSto;

final class ExecuteCmdPrcs implements QDspPrcsIf {
//=====

///// Threaded (in/ out/ trigger)

final QPlace wxCommands = new QPlace();
private QPlace rResults;

/////

@Override
public boolean init( QPlace... xrResult) {
	rResults = xrResult[0];
	return true;
}

@Override
public QBaton peek( long... xbOptFlags) {
	return wxCommands.peek();
}

private QVal[] fastExec( QCalc op, QVal[][] args) {
	final QResult pooled = QResult.get8Pool();
	QVal[] result = new QVal[1];
	try {
		Mapping mpg = null;
		String str;
		String str2;
		int len = 0;
		double num;
		double num2;
		switch (op) {
		case MC:
			str = QMapSto.string4QVals( args[0]);
			if ((null != args) && (null != str) && (0 < str.length())) {
				if (1 == str.length()) {
					str = "M" + str.charAt( 0);
				}
				CcmSto.instance.variable_set( str, null);
			}
			break;
		case CLRALL:
			CcmSto.instance.stackClear( true);
			break;
		case CLR1:
		case CLEAR:
			//CsvDb.instance.stackPop( 0 );
			break;
		case CLR2:
			CcmSto.instance.stackPop( 1, null);
			break;
		case CLR3:
			CcmSto.instance.stackPop( 2, null);
			break;
		case DATE:
			long date = MiscFunc.slotSecond16oDateApprox( MiscFunc.date4Millis( true));
			result[0] = QMapSto.qval4SlotSecond16( date);
			break;
		case DUP:
			result = args[0];
			CcmSto.instance.stackPush( args[0]);
			break;
		case LOAD:
			str = QMapSto.string4Literals( args[0]);
			if (1 == str.length()) {
				final char ch = str.charAt( 0);
				switch (ch) {
				case '!':
					str = "T";
					break;
				default:
					str = "M" + ch;
				}
			}
			str = QMapSto.string4QVal( CcmSto.instance.variable_get( str));
			result = QMapSto.qvalAtoms4String( pooled, str);
			break;
		case MMLD:
			len = args[0].length;
			result = (1 == len) ? result : new QVal[args[0].length];
			for (int i0 = 0; i0 < len; ++i0) {
				str = QMapSto.string4QVal( args[0][i0]);
				if (1 == str.length()) {
					final char ch = str.charAt( 0);
					switch (ch) {
					case '!':
						str = "T";
						break;
					default:
						str = "M" + ch;
					}
				}
				result[i0] = CcmSto.instance.variable_get( str);
			}
			break;
		case MMSTO:
			len = args[0].length;
			result = (1 == len) ? result : new QVal[args[0].length];
			for (int i0 = 0; i0 < len; ++i0) {
				str = QMapSto.string4QVal( args[1][i0 % args[1].length]);
				result[i0] = QMapSto.NaN;
				if ((null != args) && (null != str) && (0 < str.length())) {
					if (1 == str.length()) {
						str = "M" + str.charAt( 0);
					}
					CcmSto.instance.variable_set( str, args[0][i0]);
					result = args[0];
				}
			}
			break;
		case NOP:
			break;
		case PRODUCT:
			num2 = 1.0e4;
			for (QVal v0 : args[0]) {
				num = QMapSto.doubleD4oQVal( v0);
				if (!Double.isNaN( num)) {
					num2 = num2 * num / 1.0e8;
				}
			}
			result[0] = QMapSto.qval4DoubleD4( num2);
			break;
		case QCAT:
			len = args[0].length;
			result = (1 == len) ? result : new QVal[args[0].length];
			for (int i0 = 0; i0 < len; ++i0) {
				result[i0] = QMapSto.NaN;
				mpg = CcmSto.search4Oid( args[0][i0]);
				if (null != mpg) {
					if (0 != CcmSto.instance.update( mpg.clone4Cats( args[1][i0]))) {
						result[i0] = args[1][i0];
					}
				}
			}
			break;
		case QDEL:
			len = args[0].length;
			result = (1 == len) ? result : new QVal[args[0].length];
			for (int i0 = 0; i0 < len; ++i0) {
				result[i0] = CcmSto.instance.remove( args[0][i0], true) ? args[0][i0] : QMapSto.NaN;
			}
			break;
		case QLOAD: {
			result[0] = QMapSto.NaN;
			String oid = QMapSto.string4QVals( args[0]);
			mpg = CcmSto.search4Oid( QMapSto.qval4AtomicLiteral( pooled, oid));
		}
			break;
		case QOID: {
			result[0] = QMapSto.NaN;
			String label = QMapSto.string4QVals( args[0]);
			mpg = CcmSto.search4Label( QMapSto.qval4AtomicLiteral( pooled, label), 0);
			if (null != mpg) {
				result[0] = mpg.oid;
				mpg = null;
			}
		}
			break;
		case QNEXT:
			result[0] = QMapSto.NaN;
			mpg = CcmSto.searchNextLabel4Oid( args[0][0]);
			break;
		case QQL: {
			result[0] = QMapSto.NaN;
			long cats = UiDataSto.qSwitches[UiDataSto.SWI_MAPPING_CATS] & ((1L << 32) - 1L);
			String label = QMapSto.string4QVals( args[0]);
			mpg = CcmSto.search4Label( cats, QMapSto.qval4AtomicLiteral( pooled, label));
		}
			break;
		case QS: {
			long cats = UiDataSto.qSwitches[UiDataSto.SWI_MAPPING_CATS] & ((1L << 32) - 1L);
			cats = (0 != cats) ? cats : Mapping.Cats.DEFAULT.flag;
			mpg = Mapping.make( QMapSto.string4QVals( args[0]), Mapping.Cats.cats4Flags( cats), null, 0,
				QMapSto.string4QVals( args[1]));
			if (0 == CcmSto.instance.add( null, mpg)) {
				result[0] = QMapSto.NaN;
			} else {
				result = args[0];
			}
			mpg = null;
		}
			break;
		case QSTORE: {
			mpg = Mapping.make( QMapSto.string4QVal( args[0][0]), QMapSto.string4QVals( args[1]), null, 0,
				QMapSto.string4QVals( args[2]));
			if (0 == CcmSto.instance.add( null, mpg)) {
				result[0] = QMapSto.NaN;
			} else {
				result = args[0];
			}
			mpg = null;
		}
			break;
		case QUP:
			result[0] = QMapSto.NaN;
			String oid = QMapSto.string4QVals( args[0]);
			mpg = CcmSto.search4Oid( QMapSto.qval4AtomicLiteral( pooled, oid));
			if (null != mpg) {
				if (0 != CcmSto.instance.update( mpg.clone4Data( args[1]))) {
					result = args[1];
				}
			}
			break;
		case RANDOM:
			len = (int) (((long) QMapSto.doubleD4oQVal( args[0][0])) / 10000);
			result = new QVal[len];
			for (int i0 = 0; i0 < len; ++i0) {
				result[i0] = QMapSto.qval4DoubleD4( QCalc.getNextRandom() * 1.0e4);
			}
			break;
		case RANGE:
			len = args[0].length;
			num = (1 >= len) ? 0.0 : QMapSto.doubleD4oQVal( args[0][0]);
			num2 = (2 >= len) ? 1.0e4 : QMapSto.doubleD4oQVal( args[0][2]);
			len = (int) ((QMapSto.doubleD4oQVal( args[0][(1 < len) ? 1 : 0]) - num) / num2 + 0.99999);
			len = (0 >= len) ? 1 : len;
			result = new QVal[len];
			for (int i0 = 0; i0 < len; ++i0) {
				result[i0] = QMapSto.qval4DoubleD4( num);
				num += num2;
				num = MiscFunc.roundForIntUsage( num);
			}
			break;
		case STORE:
			str2 = QMapSto.string4Literals( args[0]);
			str = QMapSto.string4Literals( args[1]);
			if (1 == str.length()) {
				str = "M" + str.charAt( 0);
			}
			CcmSto.instance.variable_set( str, QMapSto.qval4AtomicLiteral( pooled, str2));
			result = args[0];
			break;
		case SUM:
			num2 = 0.0;
			for (QVal v0 : args[0]) {
				num = QMapSto.doubleD4oQVal( v0);
				if (!Double.isNaN( num)) {
					num2 += num;
				}
			}
			result[0] = QMapSto.qval4DoubleD4( num2);
			break;
		case SWAP:
			result = args[0];
			CcmSto.instance.stackPush( args[1]);
			break;
		case TICK:
			result[0] = QMapSto.qval4DoubleD4( MiscFunc.currentTimeMillisLinearized() * 10.0);
			break;
		case ZRXG:
		case ZRXN:
		case ZRXT:
			result[0] = QCalc.doRegEx( pooled, QMapSto.string4QVals( args[0]), QMapSto.string4QVals( args[1]),
				(int) (QMapSto.doubleD4oQVal( args[2][0])) / 10000, op);
			break;
		case ZRX:
			result[0] = QCalc.doRegEx( pooled, QMapSto.string4QVals( args[0]), QMapSto.string4QVals( args[1]),
				0.0, op);
			break;
		case ZSPLIT:
			str2 = QMapSto.string4Literals( args[1]);
			str = QMapSto.string4Literals( args[0]);
			result = QMapSto.qvals4Strings( pooled, str.split( str2, -1));
			break;
		default:
			// cmd = null;
			return null;
		}
		if (null != mpg) {
			result[0] = QMapSto.qval4String( pooled, QMapSto.formatList( mpg.atDataElements, "", "\t", "", false));
		}
	} catch (Exception e) {
		result = null;
	}
	return result;
}

private QVal[] MOVE_ME_TO_SLOW__Exec( QCalc funct, QVal[][] args) {
	QVal[] result = new QVal[1];
	if ((null == args) || (0 >= args.length)) {
		return null;
	}
	try {
		String file;
		byte[] dat;
		boolean ok;
		switch (funct) {
		case FDECR:
		case FENCR:
			ok = false;
			//				if ((null != args) && !QMap.isEmpty( QVal.asQVal( args[ 0 ] ) )) {
			file = QMapSto.string4QVals( args[0]);
			if ((null != file) && (0 < file.length())) {
				try {
					if (0 > file.indexOf( '/')) {
						file = new File( Dib2Config.platform.getFilesDir( "external"), file).getAbsolutePath();
					}
					if (QCalc.FDECR == funct) {
						dat = TcvCodec.instance.readPacked( file + ".dib", null, null, null);
						MiscFunc.writeFile( file, dat, 0, dat.length, null);
					} else {
						dat = MiscFunc.readFile( file, 0);
						ok = (0 < TcvCodec.instance.writePacked( dat, 0, dat.length, file + ".dib"));
					}
				} catch (Exception e) {
				}
			}
			result[0] = ok ? QMapSto.TRUE : QMapSto.FALSE;
			break;
		case EXPALL:
		case EXPORT:
			ok = false;
			file = QMapSto.string4QVals( args[0]);
			if ((null != file) && (0 < file.length())) {
				// Do not expose key values etc. unless really requested
				dat = CcmSto.instance.toCsv( null, 0, ~0, (funct == QCalc.EXPALL) ? (~0) : 4);
				try {
					if (0 > file.indexOf( '/')) {
						file = new File( Dib2Config.platform.getFilesDir( "external"), file).getAbsolutePath();
					}
					MiscFunc.writeFile( file, dat, 0, dat.length, null);
					ok = true;
				} catch (Exception e) {
				}
			}
			result[0] = ok ? QMapSto.TRUE : QMapSto.FALSE;
			break;
		case IMPORT:
			ok = false;
			file = QMapSto.string4QVals( args[0]);
			if ((null != file) && (0 < file.length())) {
				try {
					if (0 > file.indexOf( '/')) {
						file = new File( Dib2Config.platform.getFilesDir( "external"), file).getAbsolutePath();
					}
					dat = MiscFunc.readFile( file, 0);
					if (Dib2Constants.MAGIC_BYTES.length < dat.length) {
						if ((byte) Dib2Constants.RFC4880_EXP2 == dat[0]) {
							ok = 0 < CcmSto.instance.importFile( file, false);
						} else if (Arrays.equals( Dib2Constants.MAGIC_BYTES, Arrays.copyOf( dat, Dib2Constants.MAGIC_BYTES.length))) {
							ok = 0 < CcmSto.instance.importCsv( dat, false, 0);
						} else {
							///// Expecting simple entries or a header line.
							boolean found = false;
							for (int i0 = 0; (i0 < 100) && (i0 < dat.length); ++i0) {
								if ('\t' == dat[i0]) {
									found = true;
									break;
								}
							}
							for (int i0 = 0; (i0 < 1000) && (i0 < dat.length); ++i0) {
								if ('\n' == dat[i0]) {
									found = found && true;
									break;
								}
							}
							if (found) {
								ok = 0 < CcmSto.instance.importCsv( dat, false, 0);
							}
						}
					}
				} catch (Exception e) {
				}
			}
			result[0] = ok ? QMapSto.TRUE : QMapSto.FALSE;
			break;
		case SAVTO:
			ok = false;
			file = QMapSto.string4QVals( args[0]);
			if ((null != file) && (0 < file.length())) {
				if (0 > file.indexOf( '/')) {
					file = new File( Dib2Config.platform.getFilesDir( "external"), file).getAbsolutePath();
				}
				ok = 0 < CcmSto.instance.write( file, true, false, true);
			}
			result[0] = ok ? QMapSto.TRUE : QMapSto.FALSE;
			break;
		default:
			return null;
		}
	} catch (Exception e) {
		result = null;
	}
	//	cmd = null;
	return ((null == result) || ((1 <= result.length) && (null == result[0]))) ? null : result;
}

private QBaton push( QBaton baton) {
	rResults.push( baton);
	return baton;
}

//TODO Make use of PENDING/ ExecSlow
QBaton step() {
	final QResult pooled = QResult.get8Pool();
	CommandBaton cmd = (CommandBaton) wxCommands.pull();
	if (null == cmd) {
		return null;
	}

	//	cmd.operator = QCalc.getOperator( cmd.uiCommand );
	if (null == cmd.operator) {
		return push( QBaton.DUMMY);
	}
	int cArgs = cmd.operator.cArgs;
	if (0 > cArgs) {
		CcmSto.instance.stackPush( QMapSto.qval4AtomicLiteral( pooled, "(not implemented yet)"));
		return push( QBaton.EMPTY);
	}
	if ((null != cmd.uiParameter) && (0 < cmd.uiParameter.length())) {
		CcmSto.instance.stackPush( QMapSto.qvalAtoms4String( pooled, cmd.uiParameter));
	}
	cmd.vals = CcmSto.instance.stackPop( cArgs, cmd.operator);
	if ((null == cmd.vals) || (QCalc.NOP == cmd.operator)) {
		return push( QBaton.DUMMY);
	}
	if (cmd.operator == QCalc.STEP) {
		// TODO: for [...] take whole stack
		cmd.operator = QCalc.getOperator( QMapSto.string4Literals( cmd.vals[0]));
		cmd.operator = (null == cmd.operator) ? QCalc.NOP : cmd.operator;
		cArgs = cmd.operator.cArgs;
		cmd.vals = CcmSto.instance.stackPop( cArgs, cmd.operator);
		if (null == cmd.vals) {
			return push( QBaton.DUMMY);
		}
	}

	QVal[] result = null;
	if (!cmd.operator.zipped) { //(1 == cmd.operator.cReturnValues) {
		result = fastExec( cmd.operator, cmd.vals);
		if (null == result) {
			result = MOVE_ME_TO_SLOW__Exec( cmd.operator, cmd.vals);
		}
	}
	if (null == result) {
		if (!cmd.operator.zipped) {
			QVal[] args = (0 == cmd.operator.cArgs) ? (new QVal[0]) : cmd.vals[0];
			result = cmd.operator.calc( args);
			if (null != result) {
				CcmSto.instance.stackInsert( -1, new QVal[][] { result });
			}
		} else {
			///// TODO defer for >100
			final int max = QMapSto.maxItemsPerTerm( cmd.vals);
			final QVal[][] vals = new QVal[cmd.operator.cReturnValues][];
			for (int i0 = 0; i0 < cmd.operator.cReturnValues; ++i0) {
				vals[i0] = new QVal[max];
			}
			for (int i1 = 0; i1 < max; ++i1) {
				QVal[] args = QMapSto.pickItems( i1, cmd.vals);
				if (null == args) {
					result = null;
				} else {
					result = cmd.operator.calc( args);
				}
				//			if ((null == result) || ((1 == result.length) && (null == result[ 0 ]))) {
				for (int i0 = 0; i0 < vals.length; ++i0) {
					vals[i0][i1] = (null == result) ? QMapSto.NaN : result[i0];
				}
			}
			CcmSto.instance.stackInsert( -1, vals);
		}
	} else {
		if ((0 < result.length) && (null != result[0])) {
			CcmSto.instance.stackPush( result);
		}
	}
	return push( QBaton.EMPTY);
}

//=====
}
