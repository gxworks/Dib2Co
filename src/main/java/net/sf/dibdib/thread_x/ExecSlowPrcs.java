// Copyright (C) 2018,2019  Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_x;

import net.sf.dibdib.generic.*;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_any.QMapSto.QVal;

/** Long-running process. */
public final class ExecSlowPrcs implements QDspPrcsIf { //QThreadUserIf {
//=====

///// Threaded (in/ out/ trigger)

public final QPlace wxCommands = new QPlace();
private QPlace rResults;

/////

private CommandBaton mIn;
private CommandBaton mOut = null;
private double[] mSlowIntermediate;
private long[] mSlowParameters;

@Override
public boolean init( QPlace... xrResult) {
	rResults = xrResult[0];
	return true;
}

@Override
public QBaton peek( long... xbOptFlags) {
	return wxCommands.peek();
}

//TODO
private boolean start() {
	if (null == mIn) {
		return false;
	}
	mOut = null;
	CommandBaton out = null;
	try {
//		boolean delay = true; //(0 >= msecLimit);
//		///// For the fun of it ...
//		double factArg = ((QCalc.FACT == mIn.operator) && (null != mIn.vals) //,
//			&& QMapSto.isNumeric( mIn.vals[0][0])) ? ...QMapSto.doubleD4oQVal( mIn.vals[0][0]) : 0.0;
//		delay = delay || ((100.0 < factArg) && (factArg < 170.0)); // && (200 > msecLimit));
//		if (delay) {
//			mSlowParameters = new long[2];
//			mSlowIntermediate = new double[1];
//			mSlowParameters[0] = 1;
//			mSlowParameters[1] = (long) factArg;
//			mSlowParameters[1] = (170 < mSlowParameters[1]) ? 170 : mSlowParameters[1];
//			mSlowIntermediate[0] = 1.0;
//			return true;
//		} else {
		final QVal[] result = mIn.operator.calc( mIn.vals[0]);
		out = new CommandBaton();
		out.vals = new QVal[][] { result };
//		}
	} catch (Exception e) {
		return false;
	}

	mOut = out;
	return true;
}

QBaton step() {
	CommandBaton out = mOut;
	mOut = null;
	if (null == out) {
		if (null == mIn) {
			mIn = (CommandBaton) wxCommands.pull();
			start();
			if (null == mIn) {
				return mOut;
			}
		}
		if (QCalc.FACT == mIn.operator) {
			// for testing
			try {
				Thread.sleep( 200);
			} catch (InterruptedException e) {
			}
			for (long to = mSlowParameters[0] + 10; (mSlowParameters[0] < to) && (mSlowParameters[0] <= mSlowParameters[1]);
				++mSlowParameters[0]) {
				mSlowIntermediate[0] *= mSlowParameters[0];
			}
			if (mSlowParameters[0] < mSlowParameters[1]) {
				return QBaton.PENDING;
			}
			out = new CommandBaton();
			out.vals = new QVal[][] { new QVal[] { QMapSto.qval4DoubleD4( mSlowIntermediate[0]) } };
		}
	}
	// TODO push
	return out;
}

//=====
}
