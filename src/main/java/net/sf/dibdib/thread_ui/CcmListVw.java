// Copyright (C) 2018,2019 Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_ui;

import net.sf.dibdib.generic.Mapping;
import net.sf.dibdib.thread_any.*;
import net.sf.dibdib.thread_any.QMapSto.QVal;

public final class CcmListVw extends CcmVwIf {
//=====

String[] out = new String[100];

@Override
String[] getTextLines() {
	return out;
}

@Override
int prepareTextLines( boolean is4Console, String... param) {
	long xbHexMemory = 3;
	int xOffset = 1;
	boolean partial = false;
	long cats = UiDataSto.qSwitches[UiDataSto.SWI_MAPPING_CATS] & ((1L << 32) - 1L);
	cats = (0 == cats) ? Mapping.Cats.NOTE.flag : cats;
	Mapping[] list = CcmSto.instance.toList( cats, 1000);
	if (!partial) {
		out = ((null != out) && (out.length > (list.length + 5 + xOffset))) ? out
			: new String[list.length + 5 + xOffset];
	} else {
		out = (null == out) ? new String[30] : out;
	}
	QVal[] aCats = Mapping.Cats.list4Flags( cats);
	out[xOffset] = "Filter\t" + QMapSto.string4QVals( aCats, " "); // ((0 >= aCats.length) ? "" : Mapping.Cats.list4Flags( cats )[ 0 ].name());
	String[] stack = new String[5]; // includes potential '...' at end.
	CcmSto.instance.stackRead( xbHexMemory & ~2, stack, 0, true);
	out[xOffset + 1] = (null == stack[0]) ? "" : stack[0];
	out[xOffset + 2] = (null == stack[1]) ? "" : stack[1];
	out[xOffset + 3] = (null == stack[2]) ? "" : stack[2];
	int i1 = xOffset + 4;
	for (int i0 = 0; (i0 < list.length) && (i1 < out.length); ++i0, ++i1) {
		out[i1] = list[i0].toTextLine( -1);
		out[i1] = StringFunc.makePrintable( out[i1]);
	}
	out[0] = "";
	int len = list.length + 4 + xOffset;
	for (int i0 = out.length - 1; i0 >= len; --i0) {
		out[i0] = null;
	}
	return len;
}

//=====
}
