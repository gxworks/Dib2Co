// Copyright (C) 2020 Roland Horsch <gx work s{at}mai l.de>.
// License: GPLv3-or-later - see LICENSE file (github.com/gxworks/dibdib),
// plus the compatible full texts for further details.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sf.dibdib.thread_ui;

// Could be done as real 'interface' instead of 'abstract class', but Java's interfaces
// do not support implementing methods without explicit 'public' marker. This would get
// confusing if we want to mark only (or mainly) those methods as 'public' that are thread-safe.
abstract class CcmVwIf {
//=====

int prepareTextLines( boolean is4Console, String... param) {
	return -1;
}

String[] getTextLines() {
	return null;
}

//=====
}
