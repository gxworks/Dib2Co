Dib2Co (aka Dibdib Console)
======

Simple reader (plus calculator) for Dib2 encoded data (runs on a PC,
e.g. for backups created by the Dib2Qm app).

Cmp. https://dib2x.github.io

Compiled JAR files:
https://gitlab.com/gxworks/Dib2Co/pipelines


Copyright (C) 2016-2021  Roland Horsch <gx work
s{at}ma il.de >.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version (or, but only for the 'net.sf.dibdib'
    parts of the source code, the matching LGPL variant of GNU).

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License and the LICENSE file for more details.

See LICENSE file (= GPLv3-or-later: https://www.gnu.org/licenses/gpl.html)
and further details under 'LICENSE_all' or 'assets' or 'resources'
(e.g. https://gitlab.com/dibdib/dib2j/blob/master/LICENSE_all)

(Impressum: IMPRESSUM.md @ github.com/dib2x/dib2x.github.io =
https://github.com/dib2x/dib2x.github.io/blob/main/IMPRESSUM.md)
